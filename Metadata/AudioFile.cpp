/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

std::list<File> AudioFile::artExtractedList = std::list<File>();

AudioFile::TagKeyMap AudioFile::tagKeyMap(
{
      { Tag::TagTitle             , { "TITLE"               , "Title"                } }
    , { Tag::TagTitleSort         , { "TITLESORT"           , "Artist"               } }
    , { Tag::TagAlbum             , { "ALBUM"               , "Album"                } }
    , { Tag::TagAlbumSort         , { "ALBUMSORT"           , "Album Sort"           } }
    , { Tag::TagArtist            , { "ARTIST"              , "Artist"               } }
    , { Tag::TagArtistSort        , { "ARTISTSORT"          , "Artist Sort"          } }
    , { Tag::TagAlbumArtist       , { "ALBUMARTIST"         , "Album Artist"         } }
    , { Tag::TagTrackNumber       , { "TRACKNUMBER"         , "Track Number"         } }
    , { Tag::TagDiscNumber        , { "DISCNUMBER"          , "Disc Number"          } }
    , { Tag::TagCompilation       , { "COMPILATION"         , "Compilation"          } }
    , { Tag::TagContentGroup      , { "CONTENTGROUP"        , "Content Group"        } }
    , { Tag::TagEncodedBy         , { "ENCODEDBY"           , "Encoded By"           } }
    , { Tag::TagDate              , { "DATE"                , "Year"                 } }
    , { Tag::TagGenre             , { "GENRE"               , "Genre"                } }
    , { Tag::TagComment           , { "COMMENT"             , "Comment"              } }
    , { Tag::TagComposer          , { "COMPOSER"            , "Composer"             } }
    , { Tag::TagRemixer           , { "REMIXER"             , "Remixer"              } }
    , { Tag::TagLabel             , { "LABEL"               , "Label"                } }
    , { Tag::TagGrouping          , { "GROUPING"            , "Grouping"             } }
    , { Tag::TagLength            , { "LENGTH"              , "Length"               } }
    , { Tag::TagBPM               , { "BPM"                 , "BPM"                  } }
    , { Tag::TagInitialKey        , { "INITIALKEY"          , "Initial Key"          } }
    , { Tag::TagEnergyLevel       , { "ENERGYLEVEL"         , "Energy Level"         } }
    , { Tag::TagEncoding          , { "ENCODING"            , "Encoding"             } }
    , { Tag::TagOriginalDate      , { "ORIGINALDATE"        , "Original Date"        } }
    , { Tag::TagReleaseDate       , { "RELEASEDATE"         , "Release Date"         } }
    , { Tag::TagLyrics            , { "LYRICS"              , "Lyrics"               } }
    , { Tag::TagISRC              , { "ISRC"                , "ISRC"                 } }
    , { Tag::TagURL               , { "URL"                 , "URL"                  } }
    , { Tag::TagMusicBrainzTrackID, { "MUSICBRAINZ TRACK ID", "MusicBrainz Track ID" } }
});

namespace
{
    TagLib::RIFF::AIFF::File  *GetAIFF(TagLib::File *file) { return dynamic_cast<TagLib::RIFF::AIFF::File*>(file); }
    TagLib::FLAC::File        *GetFLAC(TagLib::File *file) { return dynamic_cast<TagLib::FLAC::File*>(file); }
    TagLib::MPEG::File        *GetMP3 (TagLib::File *file) { return dynamic_cast<TagLib::MPEG::File*>(file); }
    TagLib::RIFF::WAV::File   *GetWAV (TagLib::File *file) { return dynamic_cast<TagLib::RIFF::WAV::File*>(file); }
}

AudioFile::AudioFile()
: file          ("")
, fileRef       (TagLib::FileRef(""))
{}

AudioFile::AudioFile (const String &absolutePath)
: file          (absolutePath)
, fileRef       (TagLib::FileRef(absolutePath.toRawUTF8()))
{}

AudioFile::AudioFile (const File &file)
: file          (file)
, fileRef       (TagLib::FileRef(file.getFullPathName().toRawUTF8()))
{}

AudioFile::AudioFile (const AudioFile &other)
: file             (other.file)
, fileRef          (other.fileRef)
{}

AudioFile& AudioFile::operator= (const String &newAbsolutePath)
{
    file    = newAbsolutePath;
    fileRef = TagLib::FileRef(file.getFullPathName().toRawUTF8());

    return *this;
}

AudioFile& AudioFile::operator= (const File &newFile)
{
    file    = newFile;
    fileRef = TagLib::FileRef(file.getFullPathName().toRawUTF8());

    return *this;
}

AudioFile& AudioFile::operator= (const AudioFile &other)
{
    file    = other.file;
    fileRef = other.fileRef;

    return *this;
}

AudioFile::AudioFile (AudioFile &&other)
: file             (other.file)
, fileRef          (other.fileRef)
{}

AudioFile& AudioFile::operator= (AudioFile &&other)
{
    file    = other.file;
    fileRef = other.fileRef;

    return *this;
}

AudioFile::Format AudioFile::GetFormat() const
{
    String fileExt = file.getFileExtension();

    if      (String(".mp3")  == fileExt)  return Format::MP3;
    else if (String(".m4a")  == fileExt
         || (String(".aac")  == fileExt)) return Format::MP4;
    else if (String(".flac") == fileExt)  return Format::FLAC;
    else if (String(".wav")  == fileExt)  return Format::WAV;
    else if (fileExt.startsWith(".aif"))  return Format::AIFF;
    else                                  return Format::Unknown;
}

bool AudioFile::IsKnownFormat() const
{
    return GetFormat() != Format::Unknown;
}

String AudioFile::GetFormatString(Format format)
{
    switch (format)
    {
        case Format::AIFF:    return "AIFF";
        case Format::FLAC:    return "FLAC";
        case Format::MP3:     return "MP3";
        case Format::MP4:     return "MP4";
        case Format::WAV:     return "WAV";
        case Format::Unknown: return "Unsupported";

        default:
            jassertfalse;
            return "";
    }
}

std::list<AudioFile::Tag> AudioFile::GetTagList()
{
    std::list<AudioFile::Tag> list;

    for (auto pair : tagKeyMap)
    {
        list.push_back(pair.first);
    }

    return list;
}

String AudioFile::GetTagDisplayString(Tag tag)
{
    return tagKeyMap[tag].second;
}

bool AudioFile::ContainsTag(Tag tag) const
{
    return ContainsTag(tagKeyMap[tag].first);
}

String AudioFile::GetTag(Tag tag) const
{
    const char * key = tagKeyMap[tag].first;

    return ContainsTag(key) ? GetPropertyMap().find(key)->second[0].to8Bit()
                            : String();
}

void AudioFile::SetTag(Tag tag, String tagValue)
{
    if (auto tagFile = fileRef.file())
    {
        const char * key = tagKeyMap[tag].first;
        auto properties = tagFile->properties();

        properties.replace(key, TagLib::StringList(tagValue.toStdString()));

        fileRef.file()->setProperties(properties);
        fileRef.save();
    }
}

bool AudioFile::HasID3Tag() const
{
    return HasID3v1Tag() || HasID3v2Tag();
}

bool AudioFile::HasID3v1Tag() const
{
    return GetID3v1Tag();
}

bool AudioFile::HasID3v2Tag() const
{
    return GetID3v2Tag();
}

bool AudioFile::HasApeTag() const
{
    return GetAPETag();
}

bool AudioFile::IsValid() const
{
    return Exists() && IsKnownFormat();
}

bool AudioFile::Exists() const
{
    return file.existsAsFile();
}

Image AudioFile::GetArtwork() const
{
    if (std::find(artExtractedList.begin()
                  , artExtractedList.end()
                  , file) == artExtractedList.end())
    {
        ImageCache::addImageToCache(ExtrackArtwork(), file.hashCode64());
        artExtractedList.push_back(file);
    }

    return ImageCache::getFromHashCode(file.hashCode64());
}

int AudioFile::GetBitrate() const
{
    auto properties = GetAudioProperties();
    return properties ? properties->bitrate() : -1;
}

int AudioFile::GetSampleRate() const
{
    auto properties = GetAudioProperties();
    return properties ? properties->sampleRate() : -1;
}

int AudioFile::GetNumChannels() const
{
    auto properties = GetAudioProperties();
    return properties ? properties->channels() : -1;
}

int AudioFile::GetLengthSeconds() const
{
    auto properties = GetAudioProperties();
    return properties ? properties->lengthInSeconds() : -1;
}

int AudioFile::GetLengthMs() const
{
    auto properties = GetAudioProperties();
    return properties ? properties->lengthInMilliseconds() : -1;
}

void AudioFile::DumpIntoConsole()
{
    DBG("AudioFile ============================================");
    DBG("Path: " <<file.getFullPathName());
    DBG("");

    for (auto pair : GetPropertyMap())
    {
        DBG(pair.first.to8Bit());

        for (auto value : pair.second)
        {
            DBG("   -" << value.to8Bit());
        }
    }

    DBG("");
    DBG("Bitrate      : " << GetBitrate());
    DBG("Sample rate  : " << GetSampleRate());
    DBG("Channels     : " << GetNumChannels());
    DBG("Length (Secs): " << GetLengthSeconds());
    DBG("Length (Ms)  : " << GetLengthMs());
    DBG("");
    DBG("Contains Artwork: " << (GetArtwork().isValid() ? "Yes" : "No"));
    DBG("AudioFile ============================================");
}

TagLib::PropertyMap AudioFile::GetPropertyMap() const
{
    auto file = fileRef.file();
    return file ? file->properties() : TagLib::PropertyMap();
}

TagLib::AudioProperties *AudioFile::GetAudioProperties() const
{
    return fileRef.audioProperties();
}

TagLib::ID3v1::Tag *AudioFile::GetID3v1Tag() const
{
    switch (GetFormat())
    {
        case Format::AIFF:    return nullptr;
        case Format::FLAC:    return GetFLAC(fileRef.file())->ID3v1Tag();
        case Format::MP3:     return GetMP3 (fileRef.file())->ID3v1Tag();
        case Format::WAV:     return nullptr;
        case Format::MP4:     return nullptr;

        case Format::Unknown: return nullptr;
        default:
            // You need to handle all cases in the Format enum
            jassertfalse;
            return nullptr;
    }
}

TagLib::ID3v2::Tag *AudioFile::GetID3v2Tag() const
{
    switch (GetFormat())
    {
        case Format::AIFF:    return GetAIFF(fileRef.file())->tag();
        case Format::FLAC:    return GetFLAC(fileRef.file())->ID3v2Tag();
        case Format::MP3:     return GetMP3 (fileRef.file())->ID3v2Tag();
        case Format::WAV:     return GetWAV (fileRef.file())->tag();
        case Format::MP4:     return nullptr;

        case Format::Unknown: return nullptr;
        default:
            // You need to handle all cases in the Format enum
            jassertfalse;
            return nullptr;
    }
}

TagLib::APE::Tag *AudioFile::GetAPETag() const
{
    switch (GetFormat())
    {
        case Format::AIFF:    return nullptr;
        case Format::FLAC:    return nullptr;
        case Format::MP3:     return GetMP3(fileRef.file())->APETag();
        case Format::WAV:     return nullptr;
        case Format::MP4:     return nullptr;

        case Format::Unknown: return nullptr;
        default:
            // You need to handle all cases in the Format enum
            jassertfalse;
            return nullptr;
    }
}

Image AudioFile::ExtrackArtwork() const
{
    if (file.existsAsFile())
    {
        switch (GetFormat())
        {
            case Format::AIFF:
            case Format::FLAC:
            case Format::MP3:
            case Format::WAV:
            {
                if(auto id3v2Tag = GetID3v2Tag())
                {
                    auto frameList = id3v2Tag->frameListMap();
                    
                    if (auto frame = frameList["APIC"].front())
                    {
                        auto pictureFrame = static_cast<TagLib::ID3v2::AttachedPictureFrame *> (frame);

                        return ImageFileFormat::loadFrom (pictureFrame->picture().data()
                                                          , size_t(pictureFrame->picture().size()));
                    }
                }
                break;
            }
            case Format::MP4:
            {
                TagLib::MP4::File         mp4File(file.getFullPathName().toRawUTF8());
                TagLib::MP4::Tag         *tag          = mp4File.tag();
                TagLib::MP4::ItemListMap  itemsListMap = tag->itemListMap();
                TagLib::MP4::Item         coverItem    = itemsListMap["covr"];
                TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();

                if (!coverArtList.isEmpty())
                {
                    TagLib::MP4::CoverArt coverArt = coverArtList.front();
                    return ImageFileFormat::loadFrom(  coverArt.data().data()
                                                     , size_t(coverArt.data().size()));
                }
                break;
            }
            case Format::Unknown: /*Intentional fallthrough*/
            default:
                break;
        }
    }

    return Image();
}

bool AudioFile::ContainsTag(const char *tag) const
{
    return GetPropertyMap().contains(tag);
}

void AudioFile::SetPropertyMap(const TagLib::PropertyMap &replacementMap)
{
    if (auto file = fileRef.file())
    {
        file->setProperties(replacementMap);
        file->save();
    }
}
