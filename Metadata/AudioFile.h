/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    Holds onto a juce::File, and is able to perform metadata operations on the
    file via use of the TagLib library.
    You can get/set metadata values, and probe for the tag type.
    Theres a method for extracting artwork, also.
    TODO: Does it need to take an AudioFormatManager?

    @code
    AudioFile audioFile(formatManager, "Path/To/File");

    for (auto tag : AudioFile::GetTagList())
    {
        if (audioFile.ContainsTag(tag))
        {
            switch (tag)
            {
                case AudioFile::TagArtist:
                {
                    const String artist = audioFile.GetTag(tag);
                    // Do something

                    break;
                }
                case AudioFile::TagTitle:
                {
                    const String title = audioFile.GetTag(tag);
                    // Do something

                    break;
                }
                case AudioFile::TagBPM:
                {
                    float bpm = audioFile.GetTag(tag).getFloatValue();
                    // Do something

                    break;
                }
                default:
                    break;
            }
        }
    }
    @endcode
    */
class AudioFile
{
public:
    //==============================================================================
    /** Creates an (invalid) file object.
        You can use its operator= method to point it at a proper file.
     */
    AudioFile();

    /** Creates an AudioFile object from an absolute path.

        If the path supplied is a relative path, it is taken to be relative
        to the current working directory (see File::getCurrentWorkingDirectory()),
        but this isn't a recommended way of creating a file, because you
        never know what the CWD is going to be.

        On the Mac/Linux, the path can include "~" notation for referring to
        user home directories.
     */
    AudioFile (const String &absolutePath);

    /** Creates an AudioFile from a File object.
     */
    AudioFile (const File &file);

    /** Creates a copy of another AudioFile object. */
    AudioFile (const AudioFile&);

    /** Destructor. */
    ~AudioFile() = default;

    /** Sets the AudioFile based on an absolute pathname.

        If the path supplied is a relative path, it is taken to be relative
        to the current working directory (see File::getCurrentWorkingDirectory()),
        but this isn't a recommended way of creating a file, because you
        never know what the CWD is going to be.

        On the Mac/Linux, the path can include "~" notation for referring to
        user home directories.
     */
    AudioFile &operator= (const String &newAbsolutePath);

    /** Sets the AudioFile object based on a File object.
     */
    AudioFile &operator= (const File &newFile);

    /** Copies from another AudioFile object. */
    AudioFile &operator= (const AudioFile &other);

    /** Move constructor. */
    AudioFile (AudioFile&&);

    /** Move assignment operator. */
    AudioFile &operator= (AudioFile&&);

    //==============================================================================
    /** @return juce::File
     */
    const File &GetFile() const { return file; }

    //==============================================================================
    /** Supported File formats. Theres no reason why this can't be longer;
        would just need the relevant TagLib implementations.
        TODO: Add more.
     */
    enum Format
    {
          /** .aif, .aiff*/
          AIFF
          /** .flac */
        , FLAC
          /** .mp3 */
        , MP3
          /** .m4a, .aac */
        , MP4
          /** .wav */
        , WAV

          /** Unsupported format */
        , Unknown
    };

    /**
     @return The file format of the audio file.
     */
    Format GetFormat() const;

    /** @return true if the Format type is not Format::Unknown
     */
    bool IsKnownFormat() const;

    /** Returns a display string for the Format.

        @param format The format to get.
        @return the display string.
     */
    static String GetFormatString(Format format);

    //==============================================================================
    /** An enum of metadata values that can be accessed.
        Useful if you want to produce a TableListBox with columns matching with
        the metadata tags.

        @see GetTagList, TableListBox
     */
    enum Tag
    {
          TagTitle              = 1 << 0
        , TagTitleSort          = 1 << 1
        , TagAlbum              = 1 << 2
        , TagAlbumSort          = 1 << 3
        , TagArtist             = 1 << 4
        , TagArtistSort         = 1 << 5
        , TagAlbumArtist        = 1 << 6
        , TagTrackNumber        = 1 << 7
        , TagDiscNumber         = 1 << 8
        , TagCompilation        = 1 << 9
        , TagContentGroup       = 1 << 10
        , TagEncodedBy          = 1 << 11
        , TagDate               = 1 << 12
        , TagGenre              = 1 << 13
        , TagComment            = 1 << 14
        , TagComposer           = 1 << 15
        , TagRemixer            = 1 << 16
        , TagLabel              = 1 << 17
        , TagGrouping           = 1 << 18
        , TagLength             = 1 << 19
        , TagBPM                = 1 << 20
        , TagInitialKey         = 1 << 21
        , TagEnergyLevel        = 1 << 22
        , TagEncoding           = 1 << 23
        , TagOriginalDate       = 1 << 24
        , TagReleaseDate        = 1 << 25
        , TagLyrics             = 1 << 26
        , TagISRC               = 1 << 27
        , TagURL                = 1 << 28
        , TagMusicBrainzTrackID = 1 << 29
    };

    /** @return a std::list of the Tags enum.
     */
    static std::list<Tag> GetTagList();

    /** @param tag the Tag value to get
        @return a user facing display string
     */
    static String GetTagDisplayString(Tag tag);

    /** @param tag the Tag to check.
        @return true if this file has that metadata tag.
     */
    bool ContainsTag(Tag tag) const;

    /** Will return the value of a tag embedded in the metadata, if the file has
        metadata and if the tag is present.

        @param tag The metadata value to find.
        @return the found value, or an empty String if not found.
     */
    String GetTag(Tag tag) const;

    /** Will set the chosen metadata tag with the value. This will save the change
        to the file.

        @param tag the metadata value to find.
        @param tagValue the value to set.
     */
    void SetTag(Tag tag, String tagValue);

    //==============================================================================
    /** @return true if has an ID3v1 or a ID3v2 tag.
     */
    bool HasID3Tag() const;

    /** @return true if has an ID3v1 tag.
     */
    bool HasID3v1Tag() const;

    /** @return true if has an ID3v2 tag.
     */
    bool HasID3v2Tag() const;

    /** @return true if has an APE tag.
     */
    bool HasApeTag() const;

    /** @return true if the file exists and is an audio file.
     */
    bool IsValid() const;

    /** @return true if audio file exists.
     */
    bool Exists() const;

    //==============================================================================
    /** This will attempt to extract any embedded artwork from the file and return
        a juce::Image. The first time you call GetArtwork, it performs the extraction
        so this operation may take a little time. Subsequent calls to this will
        return a cached image.

        This method can legitimately return an invalid Image if there is
        no embedded artwork.

        @return juce::Image of the embedded artwork.
     */
    Image GetArtwork() const;

    /** @return bitrate if file is valid, else -1.
     */
    int GetBitrate() const;

    /** @return sample rate if file is valid, else -1.
     */
    int GetSampleRate() const;

    /** @return num channels if file is valid, else -1.
     */
    int GetNumChannels() const;

    /** @return length in seconds (rounded to nearest) if file is valid, else -1.
     */
    int GetLengthSeconds() const;

    /** @return length in milliseconds if file is valid, else -1.
     */
    int GetLengthMs() const;

    //==============================================================================

    /** Prints debug data into the console.
     */
    void DumpIntoConsole();
    
 private:
    //==============================================================================
    TagLib::PropertyMap      GetPropertyMap()             const;
    TagLib::AudioProperties *GetAudioProperties()         const;
    TagLib::ID3v1::Tag      *GetID3v1Tag()                const;
    TagLib::ID3v2::Tag      *GetID3v2Tag()                const;
    TagLib::APE::Tag        *GetAPETag()                  const;
    Image                    ExtrackArtwork()             const;
    bool                     ContainsTag(const char *tag) const;

    void                     SetPropertyMap(const TagLib::PropertyMap &replacementMap);

    using TagKeyMap = std::map<Tag, std::pair<const char *, const char *>>;

    File                   file;
    TagLib::FileRef        fileRef;
    
    static std::list<File> artExtractedList;
    static TagKeyMap       tagKeyMap;
};
