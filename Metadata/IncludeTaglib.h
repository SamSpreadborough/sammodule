/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnon-virtual-dtor"
#endif

#include <taglib/tpropertymap.h>
#include <taglib/audioproperties.h>
#include <taglib/id3v1tag.h>
#include <taglib/id3v2tag.h>
#include <taglib/apetag.h>
#include <taglib/fileref.h>
#include <taglib/aifffile.h>
#include <taglib/flacfile.h>
#include <taglib/mpegfile.h>
#include <taglib/wavfile.h>
#include <taglib/mp4file.h>
#include <taglib/attachedpictureframe.h>

#ifdef __clang__
#pragma clang diagnostic pop
#endif
