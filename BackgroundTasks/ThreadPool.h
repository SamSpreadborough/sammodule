/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    A very basic thread pool.
    You probably want to use the JUCE ThreadPool as this was written as a
    learning exercise.
 */
class ThreadPool
{
public:
    //==============================================================================
    /** Constructs a new ThreadPool object. Specify the number of threads
        to create.
    */
    ThreadPool(size_t numberOfThreads);

    /** Destructor
     */
    ~ThreadPool();
    
    /** Enqueue some work to be done on the ThreadPool.
        The work will be completed when a thread becomes available.

        @param job a lambda job to complete
     */
    void Enqueue(std::function<void()> job);

    /** @return the number of jobs in the queue
     */
    size_t QueueSize() const;

    /** Removes all jobs from the queue. Any jobs currently running will
        complete.
     */
    void Clear();

    /** Signal to stop all threads. Any jobs currently running will be
        completed before the threads exit.
     */
    void SignalThreadShouldExit();

    /** @return true if the threads are running.
    */
    bool ThreadsRunning() const;
private:
    //==============================================================================
    void Run();

    //==============================================================================
    std::mutex                        mutex;
    std::condition_variable           condition;

    std::queue<std::function<void()>> jobs;
    std::vector<std::thread>          threads;

    bool threadShouldExit;
};
