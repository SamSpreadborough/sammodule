/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

ThreadPool::ThreadPool(size_t numberOfThreads)
    : threadShouldExit(false)
{
    assert(numberOfThreads);

    for (size_t i = 0; i < numberOfThreads; ++i)
    {
        threads.emplace_back([=]{ Run(); });
    }
}

ThreadPool::~ThreadPool()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        threadShouldExit = true;
    }

    condition.notify_all();

    for(auto &thread : threads)
    {
        thread.join();
    }
}

size_t ThreadPool::QueueSize() const
{
    std::unique_lock<std::mutex> lock(mutex);

    return jobs.size();
}

void ThreadPool::Clear()
{
    std::unique_lock<std::mutex> lock(mutex);

    while (!jobs.empty())
    {
        jobs.pop();
    }
}

void ThreadPool::SignalThreadShouldExit()
{
    threadShouldExit = true;
}

bool ThreadPool::ThreadsRunning() const
{
    return !threadShouldExit;
}

void ThreadPool::Enqueue(std::function<void()> job)
{
    std::unique_lock<std::mutex> lock(mutex);

    if (!threadShouldExit)
    {
        jobs.emplace(job);
    }

    condition.notify_one();
}

void ThreadPool::Run()
{
    while(!threadShouldExit)
    {
        std::function<void()> runJob;

        {
            std::unique_lock<std::mutex> lock(mutex);

            condition.wait(lock, [=]{ return threadShouldExit || !jobs.empty(); });

            if (!jobs.empty())
            {
                runJob = std::move(jobs.front());
                jobs.pop();
            }
        }

        if (runJob)
            runJob();
    }

    Clear();
}
