/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#include "Tests/TestTrackable.cpp"
#include "Tests/TestTrackableArray.cpp"
#include "Tests/TestConstSignal.cpp"
#include "Tests/TestConstProperty.cpp"
#include "Tests/TestDatabase.cpp"

class RunTests
{
public:
    RunTests()
    {
        UnitTestRunner testRunner;
        
        TestTrackable      testTrackable;
        TestTrackableArray testTrackableArray;
        TestConstSignal    testConstSignal;
        TestConstProperty  testConstProperty;
        TestDatabase       testDatabase;
        
        testRunner.runAllTests();
    }
};

static RunTests tests;
