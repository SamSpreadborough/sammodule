/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class TestTrackableArray
: public UnitTest
{
public:
    TestTrackableArray()
    : UnitTest("TestTrackableArray")
    {}

    // UnitTest
    void runTest() override
    {
        Construction();
        AddElement();
    }

    void Construction()
    {
        class Object : public Trackable {};

        beginTest("Construction");

        TrackableArray<Object> array;

        expect(array.Size() == 0);
        expect(array.IsEmpty());
        expect(!array.IsNotEmpty());
    }

    void AddElement()
    {
       class Object
        : public Trackable
        {
        public:
            virtual ~Object() = default;
        };

        beginTest("Add element");

        TrackableArray<Object> array;

        std::unique_ptr<Object> element(new Object);

        array.Add(element.get());

        expect(array.Size() == 1);
        expect(!array.IsEmpty());
        expect(array.IsNotEmpty());

        expect(array.Contains(element.get()));
        expect(array.IndexOf(element.get()) == 0);
        expect(array[0] == element.get());
        expect(array.Get(0) == element.get());

        beginTest("When object is deleted, it automagically removes it from array");
        element.reset();

        expect(array.Size() == 0);
    }
};
