/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class TestConstProperty
    : public UnitTest
    , public Trackable
{
public:
    TestConstProperty()
    : UnitTest("TestConstProperty")
    {}

    void runTest() override
    {
        Construction();
        AssignValue();
        TestComparators();
        AddCallback();
        AddCallbackAndCall();
        AddCallbackOldAndNew();
        AddCallbackOldNewAndCall();
        AddFilter();
        Chain();
        ChainWithConversation();
    }

    void Construction()
    {
        beginTest("Construction");

        ConstProperty<TestConstProperty, int> property;
        expect(property.Size() == 0);
        expect(property == 0);
    }

    void AssignValue()
    {
        beginTest("Assign value");

        ConstProperty<TestConstProperty, int> property;

        property = 7;

        expect(property.Get() == 7);
    }

    void TestComparators()
    {
        beginTest("Test comparators");

        ConstProperty<TestConstProperty, int> intProperty;

        intProperty = 7;
        expect(intProperty == 7);
        expect(intProperty != 10);
        expect(intProperty >  0);
        expect(intProperty <  10);
        expect(intProperty >= 7);
        expect(intProperty <= 10);

        ConstProperty<TestConstProperty, int> boolProperty;

        boolProperty = true;
        expect(boolProperty);
        expect(! (!boolProperty));
    }

    void AddCallback()
    {
        beginTest("Add callback");

        Property<unsigned> property;

        unsigned callbackValue = 0;
        property.AddListener(this, [&](unsigned value)
        {
            callbackValue = value;
        });

        property = 9;

        expect(callbackValue == 9);
    }

    void AddCallbackAndCall()
    {
        beginTest("Add callback and call");

        Property<unsigned> property(10);

        unsigned callbackValue = 0;
        property.AddListenerAndCall(this, [&](unsigned value)
        {
            callbackValue = value;
        });

        expect(callbackValue == 10);
    }

    void AddCallbackOldAndNew()
    {
        beginTest("Add callback old and new");

        Property<unsigned> property(10);

        unsigned oldCallbackValue = 0;
        unsigned newCallbackValue = 0;
        property.AddListenerOldNew(this, [&](unsigned oldValue, unsigned newValue)
        {
            oldCallbackValue = oldValue;
            newCallbackValue = newValue;
        });

        property = 7;

        expect(oldCallbackValue == 10);
        expect(newCallbackValue == 7);
    }

    void AddCallbackOldNewAndCall()
    {
        beginTest("Add callback old new and call");

        Property<unsigned> property;

        unsigned oldCallbackValue = 0;
        unsigned newCallbackValue = 0;

        property = 5;
        property = 10;

        property.AddListenerOldNewAndCall(this, [&](unsigned oldValue, unsigned newValue)
        {
            oldCallbackValue = oldValue;
            newCallbackValue = newValue;
        });

        expect(oldCallbackValue == 5);
        expect(newCallbackValue == 10);

        property = 7;

        expect(oldCallbackValue == 10);
        expect(newCallbackValue == 7);
    }

    void AddFilter()
    {
        beginTest("Add filter");

        auto filter = [=] (float value)->float
        {
            return jlimit(0.f, 1.f, value);
        };

        Property<float> property(0.f, filter);

        property = 0.5f;
        expect(property == 0.5f);

        property = -0.5f;
        expect(property == 0.f);

        property = 2.f;
        expect(property == 1.f);
    }

    void Chain()
    {
        beginTest("Chain Propertys of same type");

        Property<int> propertyA(10);
        Property<int> propertyB(5);

        ::Chain(propertyA, propertyB);

        expect(propertyA == 10);
        expect(propertyB == 10);

        propertyA = 1;

        expect(propertyB == 1);
    }

    void ChainWithConversation()
    {
        beginTest("Chain Propertys of different type");

        Property<double> propertyA(10.0);
        Property<float>  propertyB(5.f);

        ::ChainWithConversion(propertyA, propertyB);

        expect(propertyA == 10.0);
        expect(propertyB == 10.f);

        propertyA = 1.0;

        expect(propertyB == 1.f);
    }
};
