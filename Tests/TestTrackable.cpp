/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class TestTrackable
: public UnitTest
, public Tracker
{
public:
    class TrackableObject : public Trackable {};

    TestTrackable()
    : UnitTest("TestTrackable")
    {}

    // UnitTest
    void runTest() override
    {
        StartTracking();
        StopTracking();
        IsTracking();
    }

    void StartTracking()
    {
        beginTest("Start tracking");

        const int numTests = 10;

        std::unique_ptr<TrackableObject> trackable[numTests];

        for (int i = 0; i < numTests; ++i)
        {
            trackable[i].reset(new TrackableObject);
            trackable[i]->AddTracker(this);

            expect(trackable[i]->Size() == 1);
        }

        Trackable *compare[numTests];

        for (int i = 0; i < numTests; ++i)
        {
            compare[i] = trackable[i].get();
        }

        for (int i = 0; i < numTests; ++i)
        {
            calledBack       = false;
            trackableDeleted = nullptr;

            trackable[i].reset();

            expect(calledBack == true);
            expect(trackableDeleted != nullptr);
            expect(compare[i] == trackableDeleted);
        }
    }

    void StopTracking()
    {
        beginTest("Stop tracking");

        std::unique_ptr<TrackableObject> trackable(new TrackableObject);

        expect(trackable->Size() == 0);
        trackable->AddTracker(this);
        expect(trackable->Size() == 1);

        trackable->RemoveTracker(this);
        expect(trackable->Size() == 0);
    }

    void IsTracking()
    {
        beginTest("Is tracking");

        std::unique_ptr<TrackableObject> trackable(new TrackableObject);
        trackable->AddTracker(this);

        class TrackerClass
        : public Tracker
        {
        public:
            // Tracker
            void TrackableWillBeDeleted(Trackable *trackable) override {}
        };

        std::unique_ptr<TrackerClass> nonTrackingClass(new TrackerClass);

        expect(trackable->IsTracking(this));
        expect(!trackable->IsTracking(nullptr));
        expect(!trackable->IsTracking(nonTrackingClass.get()));
    }

    // Tracker
    void TrackableWillBeDeleted(Trackable *trackable) override
    {
        calledBack = true;
        trackableDeleted = trackable;
    }

    bool calledBack;
    Trackable *trackableDeleted;
};
