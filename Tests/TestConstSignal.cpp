/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class TestConstSignal
    : public UnitTest
    , public Trackable
{
public:
    TestConstSignal()
    : UnitTest("TestConstSignal")
    {}

    void runTest() override
    {
        Construction();
        AddSingleListener();
        AddCallback();
        BlockCallback();
        AutomaticListenerRemoval();
    }

    void Construction()
    {
        beginTest("Construction");

        ConstSignal<TestConstSignal, int> signal;
        expect(signal.Size() == 0);
    }

    void AddSingleListener()
    {
        beginTest("Add single listener");
        ConstSignal<TestConstSignal, int> signal;

        Blockable::Ptr connection = signal.AddListener(this, [=]{ });

        expect(signal.Size() == 1);
        expect(!connection.expired());
    }

    void AddCallback()
    {
        beginTest("Add callback");
        ConstSignal<TestConstSignal, int> signal;

        bool callbackOccured = false;
        signal.AddListener(this, [&]
        {
            callbackOccured = true;
        });

        expect(signal.IsListening(this));

        signal.Notify(0);

        expect(callbackOccured == true);
    }

    void BlockCallback()
    {
        beginTest("Block callback");
        ConstSignal<TestConstSignal, String> signal;

        bool callbackOccured = false;
        auto connection = signal.AddListener(this, [&]
        {
            callbackOccured = true;
        });

        signal.Notify("");

        expect(callbackOccured == true);

        callbackOccured = false;

        ConnectionBlocker blocker(connection);

        signal.Notify("");

        expect(!callbackOccured);
    }

    void RemoveListener()
    {
        beginTest("Remove listener with Trackable");
        Signal<double> signal;

        signal.AddListener(this, [=]{});

        expect(signal.Size() == 1);

        signal.RemoveListener(this);

        expect(signal.Size() == 0);


        beginTest("Remove listener with Trackable");

        Blockable::Ptr connection = signal.AddListener(this, [=]{});

        expect(signal.Size() == 1);

        signal.RemoveListener(connection);

        expect(signal.Size() == 0);
    }

    void AutomaticListenerRemoval()
    {
        beginTest("Remove listener when it is deleted");

        class ListenerClass
        : public Trackable
        {
        };

        std::unique_ptr<ListenerClass> listenerClass(new ListenerClass);

        Property<bool> property;

        property.AddListener(listenerClass.get(), [=]{ });

        expect(property.Size() == 1);

        listenerClass.reset();

        expect(property.Size() == 0);
    }
};
