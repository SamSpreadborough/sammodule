/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class TestDatabase
: public UnitTest
{
public:
    TestDatabase()
    : UnitTest("TestDatabase")
    , random(juce::Time::getCurrentTime().currentTimeMillis())
    {
        
    }
    
    std::string RandomString(int length = 0)
    {
        if (length < 1)
            length = random.nextInt(Range<int>(0, 128));
        
        const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const int max_index = (sizeof(charset) - 1);
        
        std::string str;
        
        for (int i = 0; i < length; ++i)
        {
            str += charset[random.nextInt(max_index)];
        }
        
        return str;
    }
    
    Track CreateTrack()
    {
        Track track;
        
        track.filePath      = RandomString();
        track.dateAdded     = RandomString();
        track.title         = RandomString();
        track.artist        = RandomString();
        track.album         = RandomString();
        track.albumArtist   = RandomString();
        track.genre         = RandomString();
        track.comment       = RandomString();
        track.composer      = RandomString();
        track.remixer       = RandomString();
        track.label         = RandomString();
        track.trackNumber   = random.nextInt();
        track.year          = random.nextInt();
        track.bitRate       = random.nextInt();
        track.numChannels   = random.nextInt();
        track.key           = random.nextInt();
        track.sampleRate    = random.nextInt();
        track.lengthSeconds = random.nextDouble();
        track.beatGridStart = random.nextDouble();
        track.bpm           = random.nextDouble();
        track.gain          = random.nextDouble();
        
        return track;
    }
    
    // UnitTest
    void runTest() override
    {
        Construction();
        CreateDatabase();
        OpenDatabase();
        InsertTrack();
        Sort();
        CreatePlaylist();
        AddTrackToPlaylist();
        AddPlaylistToPlaylist();
        DeletePlaylist();
        TestTrackConnector();
        
        CloseDatabase();
    }
    
    void Construction()
    {
        beginTest("Construction");
        
        expect(database.IsOpen() == false);
        expect(database.CountTracks() == 0);
        expect(database.GetDateCreated() == Time());
        expect(database.CreatePlaylist() == false);
        expect(database.GetAllTracks().empty());
    }
    
    void CreateDatabase()
    {
        beginTest("Create a new database");
        
        file = Database::CreateNewDatabase(File("~/Desktop"));
        
        expect(file.existsAsFile());
        expect(Database::CanOpen(file));
    }
    
    void OpenDatabase()
    {
        beginTest("Open database");
        
        file = Database::CreateNewDatabase(File("~/Desktop"));
        
        expect(database.Open(file));
        expect(database.IsOpen());
        
        expect(database.GetDateCreated().getDayOfMonth() == Time::getCurrentTime().getDayOfMonth());
        expect(database.GetDateCreated().getMonth()      == Time::getCurrentTime().getMonth());
        expect(database.GetDateCreated().getYear()       == Time::getCurrentTime().getYear());
    }
    
    void InsertTrack()
    {
        beginTest("Insert track");
        
        Track track = CreateTrack();
        auto id = database.InsertTrack(track);
        expect(id != 0);
        expect(id == track.id);
        expect(database.CountTracks() == 1);
        
        beginTest("Get track");
        
        expect(database.ContainsTrack(id));
        auto returnTrack = database.GetTrack(id);
        expect(returnTrack != nullptr);
        expect(*returnTrack == track);
        auto vec = database.GetAllTracks();
        expect(!vec.empty());
        expect(vec.size() == database.CountTracks());
        expect(vec[0] == track);
        
        beginTest("Update track");
        
        track.title = RandomString();
        database.UpdateTrack(track);
        returnTrack = database.GetTrack(id);
        expect(returnTrack != nullptr);
        expect(*returnTrack == track);
        
        beginTest("Remove track");
        
        database.RemoveTrack(track);
        expect(database.CountTracks() == 0);
        expect(database.GetTrack(track.id) == nullptr);
    }
    
    void Sort()
    {
        beginTest("Get sorted");
        
        Playlist playlist = database.CreatePlaylist();
        
        for (size_t i = 0; i < 100; ++i)
        {
            Track track;
            track.title = RandomString(1);
            
            database.InsertTrack(track);
            
            playlist.InsertTrack(track.id);
        }
        
        expect(database.CountTracks() == 100);
        
        Track::Vec trackVec = database.GetAllTracks();
        std::vector<track_t> idVec;
        std::vector<Playlist::Item> playlistTracks = playlist.GetAllTracks();
        
        for (auto &it : trackVec)
        {
            idVec.push_back(it.id);
        }
        
        database.GetSorted(idVec         , &Track::title, true);
        database.GetSorted(trackVec      , &Track::title, true);
        database.GetSorted(playlistTracks, &Track::title, true);
        
        expect(idVec.size()          == trackVec.size());
        expect(playlistTracks.size() == trackVec.size());
        
        for (size_t i = 1; i < trackVec.size(); ++i)
        {
            auto prev = String(trackVec[i - 1].title);
            auto next = String(trackVec[i].title);
            
            expect(prev.compareNatural(next) <= 0);
            
            prev = database.GetTrack(idVec[i - 1])->title;
            next = database.GetTrack(idVec[i])    ->title;
            
            expect(prev.compareNatural(next) <= 0);
            
            prev = database.GetTrack(playlistTracks[i - 1].trackId)->title;
            next = database.GetTrack(playlistTracks[i]    .trackId)->title;
            
            expect(prev.compareNatural(next) <= 0);
        }
    }
    
    void CreatePlaylist()
    {
        beginTest("Create playlist");
        
        Playlist playlist = database.CreatePlaylist();
        
        expect(playlist);
        expect(playlist.IsValid());
        expect(playlist.GetName() == "New Playlist");
        expect(playlist.GetParentPlaylist().IsValid() == false);
        expect(playlist.ParentIsRoot() == true);
        
        playlist.SetName("New name");
        expect(playlist.GetName() == "New name");
        
        beginTest("Get playlist");
        
        std::vector<Playlist> check;
        for (size_t i = 0; i < 5; ++i)
        {
            auto p = database.CreatePlaylist();
            
            check.push_back(p);
            playlist.InsertPlaylist(p);
        }
        
        for (auto &p : check)
        {            
            expect(p == database.GetPlaylist(p.GetUid()));
        }
    }
    
    void AddTrackToPlaylist()
    {
        beginTest("Create playlist");
        
        Track track = CreateTrack();
        auto trackId = database.InsertTrack(track);
        
        Playlist playlist = database.CreatePlaylist();
        playlist.InsertTrack(trackId);
        
        expect(playlist.NumTracks() == 1);
        
        auto playlistTrack = playlist.GetTrack(0);
        expect(playlistTrack.dateAdded.getDayOfMonth() == Time::getCurrentTime().getDayOfMonth());
        expect(playlistTrack.dateAdded.getMonth()      == Time::getCurrentTime().getMonth());
        expect(playlistTrack.dateAdded.getYear()       == Time::getCurrentTime().getYear());
        expect(playlistTrack.trackId == track.id);
        
        beginTest("Remove track from playlist");
        playlist.RemoveTrack(playlistTrack);
        expect(playlist.NumTracks() == 0);
    }
    
    void AddPlaylistToPlaylist()
    {
        beginTest("Add a playlist to a playlist");
        
        Playlist parent = database.CreatePlaylist();
        Playlist child  = database.CreatePlaylist();
        
        parent.InsertPlaylist(child);
        
        expect(parent.NumPlaylists() == 1);
        expect(parent.GetPlaylist(0) == child);
        expect(child.GetParentPlaylist() == parent);
        
        beginTest("Move child to new playlist");
        
        Playlist newParent = database.CreatePlaylist();
        newParent.InsertPlaylist(child);
        
        expect(parent.NumPlaylists() == 0);
        expect(newParent.NumPlaylists() == 1);
        expect(newParent.GetPlaylist(0) == child);
        expect(child.GetParentPlaylist() == newParent);
        
        beginTest("Remove playlist child");
        
        newParent.RemovePlaylist(child);
        
        expect(child.ParentIsRoot());
    }
    
    void DeletePlaylist()
    {
        beginTest("Delete playlist");
        
        Playlist playlist = database.CreatePlaylist();
        expect(playlist.IsValid() == true);
        
        database.DeletePlaylist(playlist);
        expect(playlist.IsValid() == false);
    }
    
    void TestTrackConnector()
    {
        beginTest("Construct TrackConnector");
        
        TrackConnector connector(&database);
        expect(connector.GetTrackId() == 0);
        expect(connector.IsConnected() == false);
        
        expect(connector.filePath       == std::string(""));
        expect(connector.dateAdded      == std::string(""));
        expect(connector.title          == std::string(""));
        expect(connector.artist         == std::string(""));
        expect(connector.album          == std::string(""));
        expect(connector.albumArtist    == std::string(""));
        expect(connector.genre          == std::string(""));
        expect(connector.comment        == std::string(""));
        expect(connector.composer       == std::string(""));
        expect(connector.remixer        == std::string(""));
        expect(connector.label          == std::string(""));
        expect(connector.trackNumber    == 0);
        expect(connector.year           == 0);
        expect(connector.bitRate        == 0);
        expect(connector.numChannels    == 0);
        expect(connector.key            == 0);
        expect(connector.sampleRate     == 0);
        expect(connector.lengthSeconds  == 0.0);
        expect(connector.beatGridStart  == 0.0);
        expect(connector.bpm            == 0.0);
        expect(connector.gain           == 0.0);
        expect(connector.smallArtwork   == std::vector<char>());
        expect(connector.largeArtwork   == std::vector<char>());
        
        Track track = CreateTrack();
        database.InsertTrack(track);
        
        beginTest("TrackConnector - ReferTo");
        connector.ReferTo(track.id);

        expect(connector.filePath       == track.filePath);
        expect(connector.dateAdded      == track.dateAdded);
        expect(connector.title          == track.title);
        expect(connector.artist         == track.artist);
        expect(connector.album          == track.album);
        expect(connector.albumArtist    == track.albumArtist);
        expect(connector.genre          == track.genre);
        expect(connector.comment        == track.comment);
        expect(connector.composer       == track.composer);
        expect(connector.remixer        == track.remixer);
        expect(connector.label          == track.label);
        expect(connector.trackNumber    == track.trackNumber);
        expect(connector.year           == track.year);
        expect(connector.bitRate        == track.bitRate);
        expect(connector.numChannels    == track.numChannels);
        expect(connector.key            == track.key);
        expect(connector.sampleRate     == track.sampleRate);
        expect(connector.lengthSeconds  == track.lengthSeconds);
        expect(connector.beatGridStart  == track.beatGridStart);
        expect(connector.bpm            == track.bpm);
        expect(connector.gain           == track.gain);
        expect(connector.smallArtwork   == track.smallArtwork);
        expect(connector.largeArtwork   == track.largeArtwork);
        
        beginTest("TrackConnector - Updated when database changes");
        track.filePath = RandomString();
        database.UpdateTrack(track);
        
        expect(connector.filePath == track.filePath);
        
        beginTest("TrackConnector - Can set database when connector Property changes");
        connector.albumArtist = RandomString();
        track = *database.GetTrack(track.id);
        
        expect(connector.albumArtist == track.albumArtist);
    }
    
    void CloseDatabase()
    {
        beginTest("Close database");
        
        expect(database.IsOpen());
        
        auto playlist = database.CreatePlaylist();
        
        database.Close();
        expect(!database.IsOpen());
        
        expect(playlist.IsValid() == false);
    }
    
    File file;
    Database database;
    Random random;
};
