/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

AudioAnalysisJob::AudioAnalysisJob(  const String      &jobName
                                   , AudioFormatReader *reader
                                   , bool               deleteWhenFinished)
: ThreadPoolJob(jobName)
, blockSize(4096)
, progress(0)
, reader(reader, deleteWhenFinished)
{
    if (reader)
    {
        const int sampleRate  = int(reader->sampleRate);
        const int numChannels = reader->numChannels;

        bpmDetect.reset (new soundtouch::BPMDetect(numChannels, sampleRate));

        replayGain.reset(new ReplayGain);
        replayGain->initialise(sampleRate, numChannels);

        audioData.reset(new KeyFinder::AudioData);
        audioData->setFrameRate(sampleRate);
        audioData->setChannels(numChannels);
    }
}

juce::ThreadPoolJob::JobStatus AudioAnalysisJob::runJob()
{
    if (reader)
    {
        while (progress < reader->lengthInSamples)
        {
            int numSamples = GetBlockSize();

            AudioSampleBuffer buffer;
            
            ReadBlock(buffer, numSamples);
            ProcessBlock(buffer);
            UpdateProgress(numSamples);
        }
    }

    Complete();
    
    return jobHasFinished;
}

int AudioAnalysisJob::GetBlockSize() const
{
    return (progress + blockSize) < reader->lengthInSamples
    ? blockSize
    : int(reader->lengthInSamples - progress);
}

void AudioAnalysisJob::ReadBlock(AudioSampleBuffer &buffer, int numSamples)
{
    buffer.setSize(reader->numChannels, numSamples);

    reader->read(  &buffer
                 , 0
                 , numSamples
                 , progress
                 , true, true);
}

void AudioAnalysisJob::ProcessBlock(AudioSampleBuffer &buffer)
{
    const int numSamples  = buffer.getNumSamples();
    const int numChannels = buffer.getNumChannels();

    AudioSampleBuffer interleavedBuffer (1, numChannels * numSamples);
    float* interleaved = interleavedBuffer.getWritePointer (0);
    juce::AudioDataConverters::interleaveSamples (  buffer.getArrayOfReadPointers()
                                                  , interleaved
                                                  , numSamples
                                                  , numChannels);

    // Process samples
    bpmDetect->inputSamples (interleaved, numSamples);

    replayGain->process(  buffer.getReadPointer(0)
                        , buffer.getReadPointer(numChannels > 1 ? 1 : 0)
                        , buffer.getNumSamples());

    for (int i = 0; i < numSamples; ++i)
    {
        audioData->addToSampleCount(1);
        audioData->setSample(progress + i, interleaved[i]);
    }
}

void AudioAnalysisJob::UpdateProgress(int numSamples)
{
    progress += numSamples;

    if (onProgressChanged)
    {
        MessageManager::getInstance ()->callAsync ([=]
        {
            onProgressChanged(float(progress) / float(reader->lengthInSamples));
        });
    }
}

float AudioAnalysisJob::GetFirstBeatPos()
{
    const int arraySize = 64;

    std::array<float, arraySize> posArray, dbValueArray;
    bpmDetect->getBeats(posArray.data(), dbValueArray.data(), arraySize);

    float avgValue = 0.f;

    for (int i = 0; i < arraySize; ++i)
    {
        avgValue += dbValueArray[i];
    }

    avgValue = avgValue / arraySize;

    for (int i = 0; i < arraySize; ++i)
    {
        if (dbValueArray[i] > avgValue)
            return posArray[i];
    }

    return posArray[0];
}

void AudioAnalysisJob::Complete()
{
    if (onCompleted)
    {
        Results results;
        results.success = reader;

        if (results.success)
        {
            KeyFinder::KeyFinder keyFinder;

            results.tempo        = bpmDetect->getBpm ();
            results.gain         = replayGain->end ();
            results.firstBeatPos = GetFirstBeatPos();
            results.key          = keyFinder.keyOfAudio (*audioData);
        }

        MessageManager::getInstance ()->callAsync ([=]
        {
            onCompleted(results);
        });
    }
}
