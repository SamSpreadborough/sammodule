/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    A ThreadPoolJob that will analyse audio, getting tempo, key, gain, and
    first beat position. Construct one with an AudioFormatReader.
    Requires the SoundTouch, ReplayGain and KeyFinder libraries.
 */
class AudioAnalysisJob
: public ThreadPoolJob
{
public:
    //==============================================================================
    AudioAnalysisJob(  const String      &jobName
                     , AudioFormatReader *reader
                     , bool               deleteWhenFinished);

    //==============================================================================
    /** The data that will be calculated
     */
    struct Results
    {
        bool             success      = false;
        float            firstBeatPos = -1.f;
        float            tempo        = -1.f;
        float            gain         = -1.f;
        KeyFinder::key_t key          = KeyFinder::key_t::SILENCE;
    };

    /** Will update the progress every block that is analysed.
     */
    std::function<void(float)> onProgressChanged;

    /** Called when the ThreadPoolJob has finished, and has Results.
     */
    std::function<void(Results)> onCompleted;

    // ThreadPoolJob
    juce::ThreadPoolJob::JobStatus runJob() override;
private:
    //==============================================================================
    int   GetBlockSize() const;
    void  ReadBlock(AudioSampleBuffer &buffer, int numSamples);
    void  ProcessBlock(AudioSampleBuffer &buffer);
    void  UpdateProgress(int numSamples);
    float GetFirstBeatPos();
    void  Complete();

    //==============================================================================
    const int blockSize;
    int       progress;

    OptionalScopedPointer<AudioFormatReader> reader;

    std::unique_ptr<soundtouch::BPMDetect> bpmDetect;
    std::unique_ptr<ReplayGain>            replayGain;
    std::unique_ptr<KeyFinder::AudioData>  audioData;
};
