/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

/**
 */
namespace CrossFader
{
    /**
     */
    enum Assign
    {
          AssignA
        , AssignB
        , AssignThru
    };
    
    /**
     */
    enum Curve
    {
          CurveLinear
        , CurvePower
        , CurveReverse
        , CurveDisable
    };
    
    inline
    float ProcessSample(Curve curve, float crossFaderValue, float sample)
    {
        return sample;
    }
};

inline
String ToString(CrossFader::Assign assign)
{
    switch (assign)
    {
        case CrossFader::AssignA:    return "A";
        case CrossFader::AssignB:    return "B";
        case CrossFader::AssignThru: return "Thru";
            
        default:
            jassertfalse;
            return "";
    }
}

inline
String ToString(CrossFader::Curve curve)
{
    switch (curve)
    {
        case CrossFader::CurveLinear:  return "Linear";
        case CrossFader::CurvePower:   return "power";
        case CrossFader::CurveReverse: return "Reverse";
        case CrossFader::CurveDisable: return "Disable";
            
        default:
            jassertfalse;
            return "";
    }
}
