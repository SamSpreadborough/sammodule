/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#include "../Audio/AudioUtilities.h"

#include "../Audio/Equaliser.h"
#include "../Audio/CrossFaderCurve.h"
#include "../Audio/DJMixerChannel.h"
#include "../Audio/DJAudioSource.h"

#if ENABLE_XWAX
#include "../Audio/DVS/TimecodeDefinitions.h"
#include "../Audio/DVS/TimecodeProcessor.h"
#endif

#if USE_SOUNDTOUCH \
&& USE_REPLAYGAIN \
&& ENABLE_FFTW3 \
&& USE_KEY_FINDER
#include "../Audio/AudioAnalysisJob.h"
#endif
