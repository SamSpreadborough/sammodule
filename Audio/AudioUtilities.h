/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

inline
float ToDecibels(float value)
{
    return 20.0f * log10 (value);
}

inline
float FromDecibels(float decibels)
{
    return pow (10, (decibels * 0.05f));
}

inline
double SecondsToMins(second_t seconds)
{
    return seconds * (1.0 / 60.0);
}

inline
sample_t SecondsToSamples(second_t seconds, sample_t sampleRate)
{
    return sample_t(seconds * sampleRate);
}

inline
float SecondsToDegrees(int revsPerMin, second_t seconds)
{
    const double perRotation   = 60.0 / double(revsPerMin);
    const double dNumRotations = double(seconds) / perRotation;
    const int    iNumRotations = int(dNumRotations);
    const double fraction      = dNumRotations - iNumRotations;
    const float  degrees       = fraction * 360.f;

    return degrees;
}

inline
float SecondsToRadians(int revsPerMin, second_t seconds)
{
    return degreesToRadians(SecondsToDegrees(revsPerMin, seconds));
}

inline
double SamplesToMs(sample_t samples, sample_t sampleRate)
{
    return (1000 * (samples / sampleRate));
}

inline
second_t SamplesToSeconds(sample_t samples, sample_t sampleRate)
{
    return (samples / sampleRate);
}

inline
second_t PixelsToSeconds(Rectangle<int> bounds, second_t audioRange, int x)
{
    return x * (audioRange / double(bounds.getWidth()));
}

inline
second_t SecondsInABeat(double bpm)
{
    return 60.0 / bpm;
}

inline
sample_t SamplesInABeat(double bpm, sample_t sampleRate)
{
    return SecondsToSamples(SecondsInABeat(bpm), sampleRate);
}

inline
String ToHighResTimeStamp(second_t seconds)
{
    bool   isNegativeValue = seconds < 0.0;

    seconds = fabs(seconds);

    int min   = seconds / 60;
    int secs  = seconds - (60 * min);
    int milli = (seconds - (min * 60.0) - (secs)) * 100;

    String timeStamp = String::formatted("%.2d", abs(min))
    + String(":")
    + String::formatted("%.2d", abs(secs));

    if (isNegativeValue)
        timeStamp = String("-") + timeStamp;

    return timeStamp + String(".") + String::formatted("%.2d", abs(milli));
}

inline
String ToLowResTimeStamp(second_t seconds)
{
    return ToHighResTimeStamp(seconds).dropLastCharacters(3);
}
