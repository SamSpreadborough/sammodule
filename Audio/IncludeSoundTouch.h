/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#include "../Audio/soundtouch/include/FIFOSampleBuffer.h"
#include "../Audio/soundtouch/include/SoundTouch.h"
#include "../Audio/soundtouch/include/BPMDetect.h"
#include "../Audio/soundtouch/include/STTypes.h"
