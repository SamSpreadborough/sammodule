/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#include "../Audio/libKeyFinder/audiodata.cpp"
#include "../Audio/libKeyFinder/chromagram.cpp"
#include "../Audio/libKeyFinder/chromatransform.cpp"
#include "../Audio/libKeyFinder/chromatransformfactory.cpp"
#include "../Audio/libKeyFinder/constants.cpp"
#include "../Audio/libKeyFinder/fftadapter.cpp"
#include "../Audio/libKeyFinder/keyclassifier.cpp"
#include "../Audio/libKeyFinder/keyfinder.cpp"
#include "../Audio/libKeyFinder/lowpassfilter.cpp"
#include "../Audio/libKeyFinder/lowpassfilterfactory.cpp"
#include "../Audio/libKeyFinder/spectrumanalyser.cpp"
#include "../Audio/libKeyFinder/temporalwindowfactory.cpp"
#include "../Audio/libKeyFinder/toneprofiles.cpp"
#include "../Audio/libKeyFinder/windowfunctions.cpp"
#include "../Audio/libKeyFinder/workspace.cpp"
