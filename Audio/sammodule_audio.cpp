/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#if USE_REPLAYGAIN
#include "../Audio/replaygain/replaygain.cpp"
#endif

#if USE_SOUNDTOUCH
#include "../Audio/IncludeSoundTouch.cpp"
#endif

#if ENABLE_FFTW3 && USE_KEY_FINDER
#include "../Audio/IncludeKeyFinder.cpp"
#endif

#include "../Audio/DJAudioSource.cpp"

#if USE_SOUNDTOUCH \
&& USE_REPLAYGAIN \
&& ENABLE_FFTW3 \
&& USE_KEY_FINDER
#include "../Audio/AudioAnalysisJob.cpp"
#endif

#if ENABLE_XWAX
namespace xwax
{
    extern "C"
    {
#include "timecoder.c"
#include "lut.c"
    }
}

#include "../Audio/DVS/TimecodeProcessor.cpp"
#endif

