/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wshadow"
#pragma clang diagnostic ignored "-Wmacro-redefined"
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wunused"
#endif

#ifdef JUCE_MSVC
#pragma warning (push)
#pragma warning (disable: 4005 4189 4189 4267 4702 4458 4100)
#endif

#include "../Audio/soundtouch/source/SoundTouch/BPMDetect.cpp"
#include "../Audio/soundtouch/source/SoundTouch/PeakFinder.cpp"
#include "../Audio/soundtouch/source/SoundTouch/FIFOSampleBuffer.cpp"
#include "../Audio/soundtouch/source/SoundTouch/AAFilter.cpp"
#include "../Audio/soundtouch/source/SoundTouch/cpu_detect_x86.cpp"
#include "../Audio/soundtouch/source/SoundTouch/FIRFilter.cpp"
#include "../Audio/soundtouch/source/SoundTouch/InterpolateCubic.cpp"
#include "../Audio/soundtouch/source/SoundTouch/InterpolateLinear.cpp"
#include "../Audio/soundtouch/source/SoundTouch/InterpolateShannon.cpp"
#include "../Audio/soundtouch/source/SoundTouch/mmx_optimized.cpp"
#include "../Audio/soundtouch/source/SoundTouch/RateTransposer.cpp"
#include "../Audio/soundtouch/source/SoundTouch/SoundTouch.cpp"
#include "../Audio/soundtouch/source/SoundTouch/sse_optimized.cpp"
#include "../Audio/soundtouch/source/SoundTouch/TDStretch.cpp"

#ifdef JUCE_MSVC
#pragma warning (pop)
#endif

#ifdef __clang__
#pragma clang diagnostic pop
#endif
