/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

/// Return the string used by xwax to find the correct timecode definition
const char* GetXwaxDefinition(Timecode::Definition definition)
{
    switch (definition)
    {
        case Timecode::DefSerato2A:      return "serato_2a";
        case Timecode::DefSerato2B:      return "serato_2b";
        case Timecode::DefSeratoCD:      return "serato_cd";
        case Timecode::DefTraktorA:      return "traktor_a";
        case Timecode::DefTraktorB:      return "traktor_b";
        case Timecode::DefMixVibesV2:    return "mixvibes_v2";
        case Timecode::DefMixVibes7Inch: return "mixvibes_7inch";

        default:
            jassert(false);
            return "";
    }
}

TimecodeProcessor::TimecodeProcessor(StereoPair inputCh)
: sampleRate (0.0)
, inputCh    (inputCh)
{
    Definition    .AddListener(this, [=]{ this->UpdateTimecoder(); });
    SoftwarePreamp.AddListener(this, [=]{ this->UpdateTimecoder(); });

    UpdateTimecoder();
}

StereoPair TimecodeProcessor::GetInputChannels() const
{
    return inputCh;
}

void TimecodeProcessor::SetInputChannels(StereoPair newInputCh)
{
    bool state = Enabled;
    
    Enabled = false;
    inputCh = newInputCh;
    Enabled = state;
}

void TimecodeProcessor::audioDeviceIOCallback(const float **inputChannelData
                                              , int numInputChannels
                                              , float **outputChannelData
                                              , int numOutputChannels
                                              , int numSamples)
{
    const ScopedLock sl (readLock);
    
    if (!InputChannelsAreValid(numInputChannels))
    {
        Enabled = false;
    }
    
    if (Enabled)
    {
        const float *inL = inputChannelData[inputCh.first];
        const float *inR = inputChannelData[inputCh.second];
        
        while (--numSamples)
        {
            short pcm[2];

            // Create PCM audio and apply DVS gain
            pcm[0] = short(*inL * (float)(1<<14) * Gain[0]);
            pcm[1] = short(*inR * (float)(1<<14) * Gain[1]);

            timecoder_submit(&timecoder, pcm, 1);
            
            ProcessPitch   (xwax::timecoder_get_pitch   (&timecoder));
            ProcessPosition(xwax::timecoder_get_position(&timecoder, NULL));
            
            ++inL;
            ++inR;
        }
    }
}

void TimecodeProcessor::audioDeviceAboutToStart(juce::AudioIODevice *device)
{
    this->sampleRate = device->getCurrentSampleRate();
    UpdateTimecoder();
}

void TimecodeProcessor::audioDeviceStopped()
{
    
}

void TimecodeProcessor::UpdateTimecoder()
{
    definition = xwax::timecoder_find_definition(GetXwaxDefinition(Definition));

    xwax::timecoder_init(  &timecoder
                         , definition
                         , 1.0
                         , unsigned(sampleRate)
                         , SoftwarePreamp);
}

void TimecodeProcessor::ProcessPitch(double pitch)
{
    if (!std::isnan(pitch))
        Pitch = pitch_t (pitch);
}

void TimecodeProcessor::ProcessPosition(unsigned position)
{
    if (position != -1)
    {
        Error = false;
        Position = second_t(position / 1000);
    }
    else
    {
        Error = true;
    }
}

bool TimecodeProcessor::InputChannelsAreValid(unsigned numChannels)
{
    return inputCh.first  < numChannels
        && inputCh.second < numChannels;
}
