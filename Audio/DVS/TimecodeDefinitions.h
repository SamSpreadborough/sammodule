/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace Timecode
{
    enum Definition
    {
          DefSerato2A
        , DefSerato2B
        , DefSeratoCD
        , DefTraktorA
        , DefTraktorB
        , DefMixVibesV2
        , DefMixVibes7Inch
    };

    enum PlaybackMode
    {
        /** Ignores position data */
          ModeRelative
        /** Applies position data */
        , ModeAbsolute
        /** Ignores DVS, and sends audio input to output */
        , ModeThrough
    };
}

inline
String ToString(Timecode::Definition definition)
{
    switch (definition)
    {
        case Timecode::DefSerato2A:      return "Serato Vinyl Side A";
        case Timecode::DefSerato2B:      return "Serato Vinyl Side B";
        case Timecode::DefSeratoCD:      return "Serato CD";
        case Timecode::DefTraktorA:      return "Traktor Vinyl Side A";
        case Timecode::DefTraktorB:      return "Traktor Vinyl Side B";
        case Timecode::DefMixVibesV2:    return "MixVibes Vinyl V2";
        case Timecode::DefMixVibes7Inch: return "MixVibes Vinyl 7Inch";

        default:
            jassert(false);
            return "";
    }
}

inline
String ToString(Timecode::PlaybackMode mode, bool shortened = false)
{
    switch (mode)
    {
        case Timecode::ModeRelative: return shortened ? "Rel"  : "Relative";
        case Timecode::ModeAbsolute: return shortened ? "Abs"  : "Absolute";
        case Timecode::ModeThrough:  return shortened ? "Thru" : "Through";

        default:
            jassert(false);
            return "";
    }
}
