/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class TimecodeProcessor;

//==============================================================================
/**
    Model for TimecodeProcessor
 */
struct TimecodeSettings
{
    TimecodeSettings()
    : Enabled        (true)
    , Pitch          (0.0)
    , Position       (0.0)
    , Error          (false)
    , Gain           {1.f, 1.f}
    , SoftwarePreamp (false)
    , Definition     (Timecode::Definition::DefSeratoCD)
    , PlaybackMode   (Timecode::PlaybackMode::ModeRelative)
    {}
    
    /** Sets whether the TimecodeProcessor will process audio or not */
    Property<bool> Enabled;

    /** The decoded pitch */
    ConstProperty<TimecodeProcessor, pitch_t> Pitch;

    /** The decoded position in seconds */
    ConstProperty<TimecodeProcessor, second_t> Position;

    /** Will notify if there is a DVS read error */
    ConstProperty<TimecodeProcessor, bool> Error;

    /** The gain for left and right input channel. Use this to tweak the scope */
    Property<float> Gain[2];

    /** If enabled, will overgain and apply filtering to simulate a phono input */
    Property<bool> SoftwarePreamp;

    /** The timecode definition being used */
    Property<Timecode::Definition> Definition;

    /** The current playback moode. */
    Property<Timecode::PlaybackMode> PlaybackMode;
};

//==============================================================================
/**
    Processes DVS audio using the TimecodeModel settings using the xwax library.
    When a new TimecodeDefintion is set, it will reset the xwax timecoder.
 */
class TimecodeProcessor
: public  TimecodeSettings
, public  AudioIODeviceCallback
, private Trackable
{
public:
    //==============================================================================
    TimecodeProcessor(StereoPair inputCh);

    StereoPair GetInputChannels() const;
    void       SetInputChannels(StereoPair newInputCh);

    // AudioIODeviceCallback
    void audioDeviceIOCallback(const float **inputChannelData
                               , int numInputChannels
                               , float **outputChannelData
                               , int numOutputChannels
                               , int numSamples) override;
    void audioDeviceAboutToStart(juce::AudioIODevice *device) override;
    void audioDeviceStopped() override;
private:
    //==============================================================================
    void UpdateTimecoder();
    void ProcessPitch(double pitch);
    void ProcessPosition(unsigned position);
    
    bool InputChannelsAreValid(unsigned numChannels);

    //==============================================================================
    sample_t sampleRate;

    CriticalSection readLock;
    xwax::timecoder     timecoder;
    xwax::timecode_def *definition;
    StereoPair          inputCh;
};
