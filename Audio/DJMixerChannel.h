/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

/** Data model for a DJ mixer channel.
 */
struct ChannelProperties
{
    ChannelProperties(Property<EQ::Curve> &EQCurve)
    : Trim             (EQCurve)
    , LowEQ            (EQCurve)
    , MidEQ            (EQCurve)
    , HighEQ           (EQCurve)
    , Filter           (EQCurve)
    , ChannelVolume    (EQCurve)
    , CueBusEnabled    (false)
    , CrossFaderAssign (CrossFader::Assign::AssignThru)
    {}
    
    VolumeControl                Trim;
    VolumeControl                LowEQ;
    VolumeControl                MidEQ;
    VolumeControl                HighEQ;
    VolumeControl                Filter;
    VolumeControl                ChannelVolume;
    Property<bool>               CueBusEnabled;
    Property<CrossFader::Assign> CrossFaderAssign;
};

/**
 */
struct ChannelFilters
{
    void PrepareFilters(double sampleRate)
    {
        
    }
    
    IIRFilter lowBandEq;
    IIRFilter midBandEq;
    IIRFilter highBandEq;
    
    IIRFilter lowPassFilter;
    IIRFilter highPassFilter;
};

/**
 */
class ChannelProcessData
: public ChannelProperties
, public ChannelFilters
{
public:
    ChannelProcessData(Property<EQ::Curve> &EQCurve
                       , AudioSource *audioSource)
    : ChannelProperties (EQCurve)
    , source            (audioSource)
    {}
    
    AudioSource *source;
};
