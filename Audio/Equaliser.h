/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace EQ
{
    enum Curve
    {
          CurveIsolator
        , CurveEQ
    };
    
    inline
    NormalisableRange<float> GetDBRange(Curve curve)
    {
        switch (curve)
        {
                /* Use 0.0001f to prevent inf */
            case CurveIsolator: return NormalisableRange<float>(-0.001f, 6.0f);
            case CurveEQ:       return NormalisableRange<float>(-26.f  , 6.0f);
                
            default:
                jassertfalse;
                return GetDBRange(Curve::CurveEQ);
        }
    }
}

inline
String ToString(EQ::Curve curve)
{
    switch (curve)
    {
        case EQ::CurveIsolator: return "Isolator";
        case EQ::CurveEQ:       return "EQ";
            
        default:
            jassertfalse;
            return "";
    }
}

//==============================================================================
/**
    Manages Properties that update when set by either a normalised value,
    a decible value, or a float gain value. Set one and the others will update.
 
    Listens to the current EQ curve and constrains the volume values by the
    EQ curve.
 */
class VolumeControl
: public Trackable
{
public:
    //==============================================================================
    VolumeControl(Property<EQ::Curve> &EQCurve)
    : Normalised(0.f)
    , Db        (0.f)
    , Gain      (0.f)
    {
        EQCurve.AddListenerAndCall(this, [=](EQ::Curve curve)
        {
            dbRange = GetDBRange(curve);
            
            SetDb(Db);
        });
        
        SetNormalised(0.f);
    }
    
    /** Value between 0.f-1.f. Will map to the EQ curve.
     */
    ConstProperty<VolumeControl, float> Normalised;
    
    /** Value in decibels. Is constrained by the EQ curve
     */
    ConstProperty<VolumeControl, float> Db;
    
    /** Value as float converted from decibels. Constrained by EQ curve.
    */
    ConstProperty<VolumeControl, float> Gain;
    
    /** Set a normalised value.
     
        @param value new value to set
     */
    void SetNormalised(float value)
    {
        value      = ConstrainNormalised(value);
        
        Normalised = value;
        Db         = ToDecibels(dbRange.convertFrom0to1(value));
        Gain       = FromDecibels(Db);
    }
    
    /** Set a decibel value

        @param value new value to set
     */
    void SetDb(float value)
    {
        value      = ConstrainDb(value);
        
        Db         = value;
        Gain       = FromDecibels(Db);
        Normalised = dbRange.convertTo0to1(Db);
    }
    
    /** Set a float gain value

        @param value new value to set
     */
    void SetGain(float value)
    {
        value      = ConstrainGain(value);
        
        Gain       = value;
        Db         = ToDecibels(Gain);
        Normalised = dbRange.convertTo0to1(value);
    }
private:
    //==============================================================================
    float ConstrainNormalised(float value)
    {
        return jlimit(0.f, 1.f, value);
    }
    
    float ConstrainDb(float value)
    {
        return jlimit(dbRange.start, dbRange.end, value);
    }
    
    float ConstrainGain(float value)
    {
        return jlimit(FromDecibels(dbRange.start), FromDecibels(dbRange.end), value);
    }
    
    NormalisableRange<float> dbRange;
};
