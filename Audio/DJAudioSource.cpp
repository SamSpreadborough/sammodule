/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

DJAudioSource::DJAudioSource(StereoPair outCh)
: playing  (true)
, playhead (0.0)
, pitch    (1.0)
, gain     (1.f)
, outputCh (outCh)
{}

void DJAudioSource::SetSource(AudioFormatReader *formatReader, bool deleteWhenFinished)
{
    const ScopedLock lock(criticalSection);

    Reset();
    reader.set(formatReader, deleteWhenFinished);
}

AudioFormatReader *DJAudioSource::GetSource() const
{
    return reader;
}

void DJAudioSource::SetPlaying(bool newPlaying)
{
    const ScopedLock lock(criticalSection);

    playing = newPlaying;
}

void DJAudioSource::SetPitch(pitch_t newPitch)
{
    const ScopedLock lock(criticalSection);

    pitch = newPitch;
}

void DJAudioSource::SetPlayheadSample(sample_t newPlayheadSample)
{
    const ScopedLock lock(criticalSection);

    playhead = newPlayheadSample;
}

void DJAudioSource::SetPlayheadSeconds(second_t newPlayheadSecond)
{
    sample_t samplePos = reader ? newPlayheadSecond * reader->sampleRate
                                : 0.0;

    SetPlayheadSample(samplePos);
}

void DJAudioSource::SetGain(float newGain)
{
    const ScopedLock lock(criticalSection);

    gain = newGain;
}

void DJAudioSource::SetOutputChannels(StereoPair newOutputChannels)
{
    const ScopedLock lock(criticalSection);
    
    outputCh = newOutputChannels;
}

float    DJAudioSource::GetGain()     const { return gain;     }
bool     DJAudioSource::IsPlaying()   const { return playing;  }
pitch_t  DJAudioSource::GetPitch()    const { return pitch;    }
sample_t DJAudioSource::GetPlayhead() const { return playhead; }
second_t DJAudioSource::GetSeconds()  const { return reader ? SamplesToSeconds(playhead, reader->sampleRate) : 0.0; }

StereoPair DJAudioSource::GetOutputChannels() const { return outputCh; }

second_t DJAudioSource::GetLengthSeconds() const { return reader ? SamplesToSeconds(reader->lengthInSamples, reader->sampleRate) : 0.0; }
sample_t DJAudioSource::GetLengthSamples() const { return reader ? sample_t(reader->lengthInSamples) : 0.0; }

void DJAudioSource::releaseResources()
{
    Reset();
}

void DJAudioSource::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    if (PlayheadIsValid() && OutputIsValid(bufferToFill.buffer->getNumChannels()) && IsPlaying())
    {
        const bool reverse        = pitch < 0.f;
        const int  tempSampleSize = bufferToFill.buffer->getNumSamples() * (2 * fabs(pitch));
        const bool fileIsStereo   = reader->numChannels == 2;
        
        AudioSampleBuffer temp(  reader->numChannels
                               , tempSampleSize);

        reader->read(  &temp
                     , 0
                     , tempSampleSize
                     , int64(playhead) - (reverse ? tempSampleSize : 0)
                     , true, true);

        temp.applyGain(gain);

        if (reverse)
            temp.reverse(0, tempSampleSize);

        interpolator[0].process(  fabs(pitch)
                                , temp.getReadPointer(0)
                                , bufferToFill.buffer->getWritePointer(outputCh.first)
                                , bufferToFill.numSamples);
        
        interpolator[1].process(  fabs(pitch)
                                , temp.getReadPointer(fileIsStereo ? 1 : 0)
                                , bufferToFill.buffer->getWritePointer(outputCh.second)
                                , bufferToFill.numSamples);
    }
    else
    {
        bufferToFill.clearActiveBufferRegion();
    }

    IncrementPlayhead(bufferToFill.numSamples);
}

void DJAudioSource::IncrementPlayhead(int numSamples)
{
    // Increment playhead even if playhead isn't valid
    if (reader && playing)
    {
        const ScopedLock lock(criticalSection);
        
        if (customPlayheadProcessing)
        {
            playhead = customPlayheadProcessing(  playhead
                                                , pitch
                                                , numSamples
                                                , PlayheadIsValid());
        }
        else
        {
            playhead += numSamples * pitch;
        }
    }
}

void DJAudioSource::Reset()
{
    const ScopedLock lock(criticalSection);

    playhead = 0.0;
    reader.reset();
    interpolator[0].reset();
    interpolator[1].reset();
}

bool DJAudioSource::PlayheadIsValid()
{
    return reader && playhead >= 0.0 && playhead < reader->lengthInSamples;
}

bool DJAudioSource::OutputIsValid(unsigned numOutputChannels)
{
    return outputCh.first  < numOutputChannels
        && outputCh.second < numOutputChannels;
}
