/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/// Implement a DJ deck. Handles the AudioIO for playing a File and decoding
/// DVS timecode.
/// You can have multiple Decks in a system, and all will handle audio I/O.
class Deck
    : private Timer
{
public:
    //==============================================================================
    Deck(  AudioFormatManager &formatManager
         , AudioDeviceManager &deviceManager
         , size_t              number);
    
    /** Returns the deck number for this object.

        @return The deck number.
     */
    size_t GetDeckNumber();
    
    /** The audio channels that this deck will output to.

        @return The output StereoPair
     */
    StereoPair GetOutputRoute();
    
    /** The audio channels that this deck will input from.
     
        @return The input StereoPair
     */
    StereoPair GetInputRoute();
    
    /// The currently loaded Track
    ConstProperty<Deck, Track> CurrentTrack;
    
    /// The currently loaded audio file.
    ConstProperty<Deck, File> CurrentFile;
    
    /// The number of seconds elapsed. Calls on UI thread.
    ConstProperty<Deck, double> SecondsElapsed;
    
    /// The number of seconds remaining. Calls on UI thread.
    ConstProperty<Deck, double> SecondsRemaining;
    
    /// The current pitch that the player will play at. Calls on UI thread.
    ConstProperty<Deck, double> Pitch;
    
    /// The normalised player progress. Calls on UI thread.
    ConstProperty<Deck, double> Progress;
    
    /** Set this deck with a new audio file to play.

        @param file The audio file to play.
     */
    void LoadDeck(const Track &track);
    
    /** Unloads the currently loaded File.
     */
    void Unload();
    
    /**  @return The struct of DVS properties
     */
    TimecodeSettings &GetTimecodeSettings();
private:
    //==============================================================================
    class Player
    : public AudioSourcePlayer
    {
    public:
        Player(  AudioDeviceManager &deviceManager
               , StereoPair          inputPair
               , StereoPair          outputPair);
        
        TimecodeProcessor &GetTimecodeProcessor();
        DJAudioSource     &GetDJAudioSource();
        
        //==============================================================================
        /** Implementation of the AudioIODeviceCallback method. */
        void audioDeviceIOCallback (const float** inputChannelData,
                                    int totalNumInputChannels,
                                    float** outputChannelData,
                                    int totalNumOutputChannels,
                                    int numSamples) override;
        
        /** Implementation of the AudioIODeviceCallback method. */
        void audioDeviceAboutToStart (AudioIODevice* device) override;
        
        /** Implementation of the AudioIODeviceCallback method. */
        void audioDeviceStopped() override;
    private:
        TimecodeProcessor timecodeProcessor;
        DJAudioSource     audioSource;
    };
    //==============================================================================
    // Timer
    void timerCallback() override;
    
    AudioFormatManager &formatManager;
    
    const size_t number;
    Player player;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Deck)
};
