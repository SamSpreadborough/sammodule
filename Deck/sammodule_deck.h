/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#include "../Deck/BeatGrid/BeatGrid.h"
#include "../Deck/Database/MusicalKey.h"
#include "../Deck/Database/iTunes/Itunes.h"

#if ENABLE_XWAX
#include "../Deck/Deck.h"
#endif
