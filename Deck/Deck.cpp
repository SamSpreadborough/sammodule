/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

Deck::Deck(  AudioFormatManager &formatManager
           , AudioDeviceManager &deviceManager
           , size_t              number)
: CurrentTrack     (Track())
, CurrentFile      (File())
, SecondsElapsed   (0.0)
, SecondsRemaining (0.0)
, Pitch            (0.0)
, formatManager    (formatManager)
, number           (number)
, player           (  deviceManager
                    , std::make_pair(number * 2, (number * 2) + 1)
                    , std::make_pair(number * 2, (number * 2) + 1))
{
    
}

StereoPair Deck::GetOutputRoute()
{
    return player.GetDJAudioSource().GetOutputChannels();
}

StereoPair Deck::GetInputRoute()
{
    return player.GetTimecodeProcessor().GetInputChannels();
}

void Deck::LoadDeck(const Track &track)
{
    Unload();
    
    const File file(track.filePath);
    auto reader = formatManager.createReaderFor(file);
    
    if (reader)
    {
        CurrentFile  = file;
        CurrentTrack = track;
        
        startTimerHz(60);
        
        player.GetDJAudioSource().SetSource(reader);
    }
}

void Deck::Unload()
{
    stopTimer();
    
    player.GetDJAudioSource().SetSource(nullptr);
    
    SecondsElapsed   = 0.0;
    SecondsRemaining = 0.0;
    Pitch            = 0.0;
    CurrentFile      = File();
}

TimecodeSettings &Deck::GetTimecodeSettings()
{
    return player.GetTimecodeProcessor();
}

Deck::Player::Player(  AudioDeviceManager &deviceManager
                     , StereoPair          inputPair
                     , StereoPair          outputPair)
: timecodeProcessor (inputPair)
, audioSource       (outputPair)
{
    deviceManager.addAudioCallback(this);
    setSource(&audioSource);
}

TimecodeProcessor &Deck::Player::GetTimecodeProcessor()
{
    return timecodeProcessor;
}

DJAudioSource &Deck::Player::GetDJAudioSource()
{
    return audioSource;
}

void Deck::Player::audioDeviceIOCallback (const float** inputChannelData,
                                          int totalNumInputChannels,
                                          float** outputChannelData,
                                          int totalNumOutputChannels,
                                          int numSamples)
{
    if (timecodeProcessor.PlaybackMode == Timecode::PlaybackMode::ModeThrough)
    {
        auto inPair  = timecodeProcessor.GetInputChannels();
        auto outPair = audioSource.GetOutputChannels();
        
        if (   inPair.first   < totalNumInputChannels
            && inPair.second  < totalNumInputChannels
            && outPair.first  < totalNumOutputChannels
            && outPair.second < totalNumOutputChannels)
        {
            std::vector<const float*> in;
            std::vector<float*>       out;
            
            for (int ch = 0; ch < totalNumInputChannels; ++ch)
            {
                in.push_back(inputChannelData[ch]);
            }
            
            for (int ch = 0; ch < totalNumOutputChannels; ++ch)
            {
                out.push_back(outputChannelData[ch]);
            }
            
            while (numSamples--)
            {
                *out[outPair.first]  = *in[inPair.first];
                *out[outPair.second] = *in[inPair.first];
                
                for (auto &ch : in)
                {
                    ++ch;
                }
                
                for (auto &ch : out)
                {
                    ++ch;
                }
            }
        }
    }
    else
    {
        timecodeProcessor.audioDeviceIOCallback(  inputChannelData
                                                , totalNumInputChannels
                                                , outputChannelData
                                                , totalNumOutputChannels
                                                , numSamples);
        
        audioSource.SetPitch(1.0);
        
        AudioSourcePlayer::audioDeviceIOCallback(  inputChannelData
                                                 , totalNumInputChannels
                                                 , outputChannelData
                                                 , totalNumOutputChannels
                                                 , numSamples);
    }
}

void Deck::Player::audioDeviceAboutToStart (AudioIODevice* device)
{
    timecodeProcessor.audioDeviceAboutToStart(device);
    AudioSourcePlayer::audioDeviceAboutToStart(device);
}

void Deck::Player::audioDeviceStopped()
{
    timecodeProcessor.audioDeviceStopped();
    AudioSourcePlayer::audioDeviceStopped();
}

void Deck::timerCallback()
{
    SecondsElapsed   = player.GetDJAudioSource().GetSeconds();
    SecondsRemaining = player.GetDJAudioSource().GetLengthSeconds() - player.GetDJAudioSource().GetSeconds();
    Pitch            = player.GetDJAudioSource().GetPitch();
    Progress         = jlimit(0.0
                              , 1.0
                              , player.GetDJAudioSource().GetSeconds() / player.GetDJAudioSource().GetLengthSeconds());
}
