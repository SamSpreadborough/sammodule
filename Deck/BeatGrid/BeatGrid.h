/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/** Data representing a beat grid marker
 */
struct BeatMarkerData
{
    const sample_t sample;
    const second_t seconds;
    const int      index;
    const bool     downBeat;
};

//==============================================================================
/**
    Creates an infinite beat grid for a track using the BPM, sample rate
    and the beat grid start.

    Find the next, previous and closest beat grid markers for a given sample
    position, and get a range of beat markers given a range of sample positions.

    BeatMarkerData holds the sample position, the number of seconds it represents,
    the index of the marker, and whether or not that marker is a downbeat
    (assuming that all music is in 4/4... which it is!)

    TODO: Support multiple time signatures
 */
class BeatGrid
{
public:
    //==============================================================================
    BeatGrid(float bpm, sample_t sampleRate, sample_t firstBeatSample);
    BeatGrid(BeatGrid &other);
    BeatGrid(BeatGrid &&other);
    
    /** @return the set bpm
     */
    float GetBpm() const;

    /** @return the set sample rate
     */
    sample_t GetSampleRate() const;

    /** @return the sample of the first beat
     */
    sample_t GetFirstBeatSample() const;

    /** @return length in samples
     */
    sample_t GetBeatLength() const;

    /** @param sample the sample to check
        @return the percentage progress between previous beat marker and the
        next one. Value is between 0.0 and 1.0
     */
    double GetBeatFraction(sample_t sample) const;

    /**
        Given a sample, will return the next sample position that adheres to the
        grid.

        @param sample the sample to find the next grid marker from
        @return the found BeatMarkerData
     */
    BeatMarkerData FindNextBeat(sample_t sample) const;

    /**
        Given a sample, will return the previous sample position that adheres to the
        grid.

        @param sample the sample to find the previous grid marker from
        @return the found BeatMarkerData
     */
    BeatMarkerData FindPrevBeat (sample_t sample) const;

    /**
        Given a sample, will find the closest beat marker to it

        @param sample the sample to find the closest grid marker from
        @return the found BeatMarkerData
     */
    BeatMarkerData FindClosestBeat(sample_t sample) const;

    /**
        Will return a std::vector of BeatMarkerData representing the grid
        given a start and end position.

        @param startSample the sample to start the range from
        @param stopSample the sample to end the range at
        @return a container of BeatMarkerData representing the given beat grid range
     */
    std::vector<BeatMarkerData> FindBeats (sample_t startSample, sample_t stopSample) const;
    
private:
    //==============================================================================
    float    bpm;
    sample_t sampleRate;
    sample_t firstBeatSample;
};
