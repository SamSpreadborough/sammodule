/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//namespace iTunes
//{
//    enum Type
//    {
//          TypeTrack
//        , TypePlaylist
//    };
//
//    // ===============================================================================
//    /** Data model for a track as described
//        in the iTunes XML file.
//     */
//    struct Track
//    {
//        int                         trackID;
//
//        std::optional<int>          size;
//        std::optional<int>          totalTime;
//        std::optional<int>          discNumber;
//        std::optional<int>          discCount;
//        std::optional<int>          trackNumber;
//        std::optional<int>          trackCount;
//        std::optional<int>          year;
//        std::optional<juce::String> dateModified;
//        std::optional<juce::String> dateAdded;
//        std::optional<int>          bitRate;
//        std::optional<int>          sampleRate;
//        std::optional<int>          playCount;
//        std::optional<int>          playDate;
//        std::optional<juce::String> playDateUTC;
//        std::optional<juce::String> releaseDate;
//        std::optional<int>          artworkCount;
//        std::optional<juce::String> persitentID;
//        std::optional<juce::String> trackType;
//        std::optional<bool>         appleMusic;
//        std::optional<juce::String> name;
//        std::optional<juce::String> artist;
//        std::optional<juce::String> albumArtist;
//        std::optional<juce::String> composer;
//        std::optional<juce::String> album;
//        std::optional<juce::String> genre;
//        std::optional<juce::String> kind;
//        std::optional<juce::String> sortName;
//        std::optional<juce::String> sortAlbum;
//        std::optional<juce::String> sortArtist;
//        std::optional<juce::File>   location;
//    };
//
//    // ===============================================================================
//    /** Data model for a playlist as described
//        in the iTunes XML file.
//        The iTunes XML holds an array of items held in the playlist, so this
//        is parsed and added to the vector.
//        As playlists can contain playlists, items are stored as a pair
//        with the Type and the unique ID.
//        If the playlist is a playlist folder, then Folder will not be a nullopt.
//     */
//    struct Playlist
//    {
//        int                         playlistID;
//
//        std::optional<bool>         master;
//        std::optional<juce::String> persistentID;
//        std::optional<juce::String> parentPersistentID;
//        std::optional<bool>         folder;
//        std::optional<juce::String> name;
//
//        std::vector< std::pair<Type, unsigned> > items;
//    };
//
//    // ===============================================================================
//    /** Holds a map of Tracks and Playlists, mapping the ID to the itunes item.
//     */
//    struct Data
//    {
//        std::map <unsigned, Playlist> playlistMap;
//        std::map <unsigned, Track>    trackMap;
//    };
//
//    /** Loads an iTunes file and parses it into
//        Tracks and Playlists.
//        Omits any tracks that aren't local, and/or are Films, Podcast etc.
//
//        @return a nullopt if the file was not able to be loaded, else
//        a parsed Data object.
//     */
//    std::optional<Data> ParseItunesXmlFile(const File &file);
//}
