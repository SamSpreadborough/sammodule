/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//namespace
//{
//    inline
//    bool IsAudioFile(juce::XmlElement *currentElement)
//    {
//        bool isAudioFile = false;
//
//        forEachXmlChildElement(*currentElement, child)
//        {
//            const juce::String elementKey (child->getAllSubText());
//
//            if (elementKey == "Kind")
//            {
//                if (    child->getNextElement()->getAllSubText().contains ("audio file")
//                    && !child->getNextElement()->getAllSubText().contains ("Apple Music"))
//                {
//                    isAudioFile = true;
//                }
//            }
//            if (elementKey == "Podcast")
//            {
//                return false;
//            }
//            else if (elementKey == "Track Type")
//            {
//                // this is not a local or readable file
//                if (   child->getNextElement()->getAllSubText().contains ("Remote")
//                    || child->getNextElement()->getAllSubText().contains ("URL")
//                    || child->getNextElement()->getAllSubText().contains ("Apple Music"))
//                {
//                    return false;
//                }
//            }
//        }
//
//        return isAudioFile;
//    }
//
//    inline
//    bool IsPlaylist(XmlElement *currentElement)
//    {
//        bool isPlaylist = false;
//
//        forEachXmlChildElement(*currentElement, child)
//        {
//            const String elementKey (child->getAllSubText());
//
//            if (elementKey == "Playlist ID")
//            {
//                isPlaylist = true;
//            }
//            else if (elementKey == "Distinguished Kind")
//            {
//                return false;
//            }
//        }
//
//        return isPlaylist;
//    }
//
//    inline
//    void ParsePlaylistItems(juce::XmlElement *attribute, iTunes::Data &data, iTunes::Playlist &playlist)
//    {
//        forEachXmlChildElement(*attribute->getNextElement(), item)
//        {
//            auto key   = item->getFirstChildElement();
//            auto value = key->getNextElement();
//
//            if (key->getAllSubText() == "Track ID")
//            {
//                int id = value->getAllSubText().getIntValue();
//
//                // Only add track if it is in the data track map. Some items are
//                // rejected (e.g Podcasts, Films etc)
//                if (data.trackMap.find(id) != data.trackMap.end())
//                {
//                    playlist.items.emplace_back(std::make_pair(iTunes::Type::TypeTrack
//                                                               , id));
//                }
//            }
//            else if (key->getAllSubText() == "Playlist ID")
//            {
//                int id = value->getAllSubText().getIntValue();
//
//                if (data.playlistMap.find(id) != data.playlistMap.end())
//                {
//                    playlist.items.emplace_back(std::make_pair(iTunes::Type::TypePlaylist
//                                                               , id));
//                }
//            }
//        }
//    }
//
//    inline
//    iTunes::Playlist ParsePlaylist(XmlElement *currentElement, iTunes::Data &data)
//    {
//        iTunes::Playlist playlist;
//
//        forEachXmlChildElement(*currentElement, attribute)
//        {
//            const String elementKey = attribute->getAllSubText();
//
//            if (attribute->getNextElement())
//            {
//                const String elementValue = attribute->getNextElement()->getAllSubText();
//
//                if      (elementKey == "Playlist ID")            playlist.playlistID         = elementValue.getIntValue();
//                else if (elementKey == "Master")                 playlist.master             = elementValue.getIntValue();
//                else if (elementKey == "Name")                   playlist.name               = elementValue;
//                else if (elementKey == "Parent Persistent ID")   playlist.parentPersistentID = elementValue;
//                else if (elementKey == "Playlist Persistent ID") playlist.persistentID       = elementValue;
//                else if (elementKey == "Folder")                 playlist.folder             = elementValue.getIntValue();
//
//                else if (elementKey == "Playlist Items")         ParsePlaylistItems(attribute, data, playlist);
//            }
//        }
//
//        return playlist;
//    }
//
//    inline
//    void ParsePlaylists(juce::XmlElement *currentElement, iTunes::Data &data)
//    {
//        forEachXmlChildElement(*currentElement, child)
//        {
//            if (IsPlaylist(child))
//            {
//                auto playlist = ParsePlaylist(child, data);
//
//                // If a playlist has a parent, add this playlist to it's parent's items vector
//                if (playlist.parentPersistentID)
//                {
//                    for (auto &[ID, i] : data.playlistMap)
//                    {
//                        if (*i.persistentID == *playlist.parentPersistentID)
//                        {
//                            i.items.emplace_back(std::make_pair(iTunes::Type::TypePlaylist
//                                                                , playlist.playlistID));
//                        }
//                    }
//                }
//
//                data.playlistMap[playlist.playlistID] = playlist;
//            }
//        }
//    }
//
//    inline
//    String stripFileProtocolForLocal (const String& pathToStrip)
//    {
//        if (pathToStrip.startsWith ("file://"))
//        {
//#if JUCE_WINDOWS
//            String temp (pathToStrip.substring (pathToStrip.indexOf (7, "/") + 1));
//#else
//            String temp (pathToStrip.substring (pathToStrip.indexOf (7, "/")));
//#endif
//            return URL::removeEscapeChars (temp);
//        }
//
//        return String();
//    }
//
//    inline
//    iTunes::Track ParseTrack(XmlElement *currentElement)
//    {
//        iTunes::Track track;
//
//        forEachXmlChildElement(*currentElement, child)
//        {
//            const String elementKey(child->getAllSubText());
//
//            if (child->getNextElement())
//            {
//                const String elementValue(child->getNextElement()->getAllSubText());
//
//                if      (elementKey == "Track ID")      track.trackID      = elementValue.getIntValue();
//                else if (elementKey == "Size")          track.size         = elementValue.getIntValue();
//                else if (elementKey == "Total Time")    track.totalTime    = elementValue.getIntValue();
//                else if (elementKey == "Disc Number")   track.discNumber   = elementValue.getIntValue();
//                else if (elementKey == "Disc Count")    track.discCount    = elementValue.getIntValue();
//                else if (elementKey == "Track Number")  track.trackNumber  = elementValue.getIntValue();
//                else if (elementKey == "Track Count")   track.trackCount   = elementValue.getIntValue();
//                else if (elementKey == "Year")          track.year         = elementValue.getIntValue();
//                else if (elementKey == "Date Modified") track.dateModified = elementValue;
//                else if (elementKey == "Date Added")    track.dateAdded    = elementValue;
//                else if (elementKey == "Bit Rate")      track.bitRate      = elementValue.getIntValue();
//                else if (elementKey == "Sample Rate")   track.sampleRate   = elementValue.getIntValue();
//                else if (elementKey == "Play Count")    track.playCount    = elementValue.getIntValue();
//                else if (elementKey == "Play Date")     track.playDate     = elementValue.getIntValue();
//                else if (elementKey == "Play Date UTC") track.playDateUTC  = elementValue;
//                else if (elementKey == "Release Date")  track.releaseDate  = elementValue;
//                else if (elementKey == "Artwork Count") track.artworkCount = elementValue.getIntValue();
//                else if (elementKey == "Persistent ID") track.persitentID  = elementValue;
//                else if (elementKey == "Track Type")    track.trackType    = elementValue;
//                else if (elementKey == "Apple Music")   track.appleMusic   = elementValue.getIntValue();
//                else if (elementKey == "Name")          track.name         = elementValue;
//                else if (elementKey == "Artist")        track.artist       = elementValue;
//                else if (elementKey == "Album Artist")  track.albumArtist  = elementValue;
//                else if (elementKey == "Composer")      track.composer     = elementValue;
//                else if (elementKey == "Album")         track.album        = elementValue;
//                else if (elementKey == "Genre")         track.genre        = elementValue;
//                else if (elementKey == "Kind")          track.kind         = elementValue;
//                else if (elementKey == "Sort Name")     track.sortName     = elementValue;
//                else if (elementKey == "Sort Album")    track.sortAlbum    = elementValue;
//                else if (elementKey == "Sort Artist")   track.sortArtist   = elementValue;
//                else if (elementKey == "Location")      track.location     = stripFileProtocolForLocal(elementValue);
//            }
//        }
//
//        return track;
//    }
//
//    inline
//    void ParseTracks(juce::XmlElement *&currentElement, iTunes::Data &data)
//    {
//        while (currentElement)
//        {
//            currentElement = currentElement->getNextElement(); // move on to the <dict>
//
//            if (!currentElement)
//                break; // finished
//
//            if (currentElement->getTagName() == "dict")
//            {
//                if (IsAudioFile(currentElement))
//                {
//                    auto track = ParseTrack(currentElement);
//
//                    data.trackMap[track.trackID] = track;
//                }
//            }
//
//            currentElement = currentElement->getNextElement(); // move to next track
//        }
//    }
//}
//
////===============================================================================
//
//std::optional<iTunes::Data> iTunes::ParseItunesXmlFile(const File &file)
//{
//    if (file.existsAsFile())
//    {
//        std::unique_ptr<juce::XmlElement> itunesXml;
//        itunesXml.reset(XmlDocument::parse(file));
//
//        if (itunesXml)
//        {
//            iTunes::Data data;
//
//            if (   itunesXml->hasTagName ("plist")
//                && itunesXml->getStringAttribute ("version") == "1.0")
//            {
//                // Move to track playlist
//                XmlElement *dict           = itunesXml->getFirstChildElement()->getChildByName("dict");
//                XmlElement *currentElement = dict     ->getFirstChildElement();
//
//                ParseTracks(currentElement, data);
//
//                currentElement = dict++; // Move to playlist element
//                currentElement = currentElement->getNextElementWithTagName("array");
//
//                ParsePlaylists(currentElement, data);
//
//                return data;
//            }
//        }
//    }
//
//    return std::nullopt;
//}
