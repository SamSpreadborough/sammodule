/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

enum MusicalKey
{
      A_Major
    , A_Minor
    , B_FlatMajor
    , B_FlatMinor
    , B_Major
    , B_Minor
    , C_Major
    , C_Minor
    , D_FlatMajor
    , D_FlatMinor
    , D_Major
    , D_Minor
    , E_FlatMajor
    , E_FlatMinor
    , E_Major
    , E_Minor
    , F_Major
    , F_Minor
    , G_FlatMajor
    , G_FlatMinor
    , G_Major
    , G_Minor
    , A_FlatMajor
    , A_FlatMinor

    , NoKey
};

enum KeyNotationType
{
      Musical
    , Camelot
    , OpenKey
};

inline
String ToString(KeyNotationType type)
{
    switch (type)
    {
        case Musical: return "Musical";
        case Camelot: return "Camelot";
        case OpenKey: return "Open Key";

        default:
            jassert(false);
            return "";
    }
};

inline
String ToString(MusicalKey key, KeyNotationType type)
{
    switch (type)
    {
        case Musical:
        {
            switch (key)
            {
                case A_Major:     return "A";
                case A_Minor:     return "Am";
                case B_FlatMajor: return "Bb";
                case B_FlatMinor: return "Bbm";
                case B_Major:     return "B";
                case B_Minor:     return "Bm";
                case C_Major:     return "C";
                case C_Minor:     return "Cm";
                case D_FlatMajor: return "Db";
                case D_FlatMinor: return "Dbm";
                case D_Major:     return "D";
                case D_Minor:     return "Dm";
                case E_FlatMajor: return "Eb";
                case E_FlatMinor: return "Ebm";
                case E_Major:     return  "E";
                case E_Minor:     return "Em";
                case F_Major:     return  "F";
                case F_Minor:     return "Fm";
                case G_FlatMajor: return "Gb";
                case G_FlatMinor: return "Gbm";
                case G_Major:     return "G";
                case G_Minor:     return "Gm";
                case A_FlatMajor: return "Ab";
                case A_FlatMinor: return "Abm";

                case NoKey: /*Intentional fallthrough*/
                default:
                    return "No Key";
            }
        }
        case Camelot:
        {
            switch (key)
            {
                case A_Major:      return "11B";
                case A_Minor:      return "8A";
                case B_FlatMajor:  return "6B";
                case B_FlatMinor:  return "3A";
                case B_Major:      return "1B";
                case B_Minor:      return "10A";
                case C_Major:      return "8B";
                case C_Minor:      return "5A";
                case D_FlatMajor:  return "3B";
                case D_FlatMinor:  return "12A";
                case D_Major:      return "10B";
                case D_Minor:      return "7A";
                case E_FlatMajor:  return "5B";
                case E_FlatMinor:  return "2A";
                case E_Major:      return "12B";
                case E_Minor:      return "9A";
                case F_Major:      return "7B";
                case F_Minor:      return "4A";
                case G_FlatMajor:  return "2B";
                case G_FlatMinor:  return "11A";
                case G_Major:      return "9B";
                case G_Minor:      return "6A";
                case A_FlatMajor:  return "4B";
                case A_FlatMinor:  return "1A";

                case NoKey: /*Intentional fallthrough*/
                default:
                    return "No Key";
            };
        }
        case OpenKey:
        {
            switch (key)
            {
                case A_Major:      return "4d";
                case A_Minor:      return "1m";
                case B_FlatMajor:  return "11d";
                case B_FlatMinor:  return "8m";
                case B_Major:      return "6d";
                case B_Minor:      return "3m";
                case C_Major:      return "1d";
                case C_Minor:      return "10m";
                case D_FlatMajor:  return "8d";
                case D_FlatMinor:  return "5m";
                case D_Major:      return "3d";
                case D_Minor:      return "12m";
                case E_FlatMajor:  return "10d";
                case E_FlatMinor:  return "7m";
                case E_Major:      return "5d";
                case E_Minor:      return "2m";
                case F_Major:      return "12d";
                case F_Minor:      return "9m";
                case G_FlatMajor:  return "7d";
                case G_FlatMinor:  return "4m";
                case G_Major:      return "2d";
                case G_Minor:      return "11m";
                case A_FlatMajor:  return "9d";
                case A_FlatMinor:  return "6m";

                case NoKey: /*Intentional fallthrough*/
                default:
                    return "No Key";
            };
        }
        default:
            jassert(false);
            return "";
    }
};
