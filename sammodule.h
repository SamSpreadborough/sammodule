/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

/*******************************************************************************
 The block below describes the properties of this module, and is read by
 the Projucer to automatically generate project code that uses it.
 For details about the syntax and how to create or use a module, see the
 JUCE Module Format.txt file.

 #if 0
 BEGIN_JUCE_MODULE_DECLARATION

 ID:                 sammodule
 vendor:             Sam Spreadborough
 name:               SamModule various useful JUCE classes and utilities
 version:            5.3.2
 description:        Extensions to the JUCE library
 website:            https://samspreadborough.com
 license:            MIT
 minimumCppStandard: 17
 searchpaths:        submodules/link/include submodules/link/modules/asio-standalone/asio/include submodules/sqlite_orm/include Audio/DVS/xwax Network/curl submodules/cereal/include
 
 dependencies:   juce_audio_basics, juce_audio_devices, juce_audio_formats, juce_audio_utils, juce_core, juce_data_structures, juce_events, juce_graphics, juce_gui_basics, juce_osc

 END_JUCE_MODULE_DECLARATION

 #endif

 *******************************************************************************/

#pragma once

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
#pragma clang diagnostic ignored "-Wexceptions"
#endif

#include <JuceHeader.h>
#include <variant>
#include <optional>
#include <list>
#include <map>

//=============================================================================
/** Config: USE_TAGLIB
 Enables the TagLib library.
 */
#ifndef USE_TAGLIB
    #define USE_TAGLIB 0
#endif

#if USE_TAGLIB
#include "Metadata/IncludeTaglib.h"
#endif

//=============================================================================
/** Config: USE_ABLETON_LINK
 Enables the Ableton Link library.
 */
#ifndef USE_ABLETON_LINK
#define USE_ABLETON_LINK 0
#endif

#if USE_ABLETON_LINK
#include "Network/IncludeAbletonLink.h"
#endif

//=============================================================================
/** Config: USE_SQLITE_ORM
 Enables the SQLite ORM library
 */
#ifndef USE_SQLITE_ORM
#define USE_SQLITE_ORM 0
#endif

#if USE_SQLITE_ORM
#include <sqlite_orm/sqlite_orm.h>
#endif

//=============================================================================
/** Config: USE_SOUNDTOUCH
 Enables the SoundTouch library
 */
#ifndef USE_SOUNDTOUCH
#define USE_SOUNDTOUCH 0
#endif

#if USE_SOUNDTOUCH
#undef max
#undef PI
#include "Audio/IncludeSoundTouch.h"
#endif

//=============================================================================
/** Config: USE_REPLAYGAIN
 Enables the Replay Gain library
 */
#ifndef USE_REPLAYGAIN
#define USE_REPLAYGAIN 0
#endif

#if USE_REPLAYGAIN
#include "Audio/replaygain/replaygain.h"
#endif

//=============================================================================
/** Config: USE_KEY_FINDER
 Enables the Key Finder library
 */
#ifndef USE_KEY_FINDER
#define USE_KEY_FINDER 0
#endif

//=============================================================================
/** Config: ENABLE_FFTW3
 Enables the FFTW3 library. This must be installed seperately, and linked seperately
 */
#ifndef ENABLE_FFTW3
#define ENABLE_FFTW3 0
#endif

#if ENABLE_FFTW3 && USE_KEY_FINDER
#include "Audio/IncludeKeyFinder.h"
#endif

//=============================================================================
/** Config: ENABLE_XWAX
 Enables the Xwax library.
 */
#ifndef ENABLE_XWAX
#define ENABLE_XWAX 0
#endif

#if ENABLE_XWAX
namespace xwax
{
    extern "C"
    {
        #include "debug.h"
        #include "lut.h"
        #include "pitch.h"
        #include "timecoder.h"
    }
}
#endif

//=============================================================================
/** Config: ENABLE_CEREAL
 Enables the Cereal library.
 */
#ifndef ENABLE_CEREAL
#define ENABLE_CEREAL 0
#endif

#if ENABLE_CEREAL
#undef TRANS
#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/array.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#endif

//=============================================================================
/** Disambiguate player data types
 */
using sample_t = double;
using second_t = double;
using pitch_t  = double;

using StereoPair = std::pair<unsigned, unsigned>;

using namespace juce;

//=============================================================================
/** Config: USE_DROW_AUDIO
 Enables the dRowAudio library.
 */
#ifndef USE_DROW_AUDIO
#define USE_DROW_AUDIO 0
#endif

#if USE_DROW_AUDIO
#include "drow/dRowAudio_BiquadFilter.h"
#include "drow/dRowAudio_ColouredAudioThumbnail.h"
#endif

//#include "ControlSurface/DeckDefinitions.h"

#include "Trackable/sammodule_trackable.h"
#include "Events/sammodule_events.h"
#include "Audio/sammodule_audio.h"
#include "Metadata/sammodule_metadata.h"
#include "Database/sammodule_database.h"
#include "EnginePrime/sammodule_engineprime.h"
#include "Deck/sammodule_deck.h"
#include "Network/sammodule_network.h"
#include "UI/sammodule_ui.h"

//=============================================================================
/** Config: ENABLE_TESTS
 Enables the Unit Tests.
 */
#ifndef ENABLE_TESTS
#define ENABLE_TESTS 0
#endif

#if ENABLE_TESTS
#include "IncludeTests.h"
#endif

//=============================================================================

#ifdef __clang__
#pragma clang diagnostic pop
#endif
