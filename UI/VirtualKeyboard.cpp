/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

VirtualKeyboard::VirtualKeyboard()
: Shift(true)
{
#define KEY(k) { String(k), nullptr }
#define SPECIAL(k) { SpecialKey::k, nullptr }
    
    layouts[Page::Alphabet] =
    {
          { KEY("q"), KEY("w"), KEY("e"), KEY("r"), KEY("t"), KEY("y"), KEY("u"), KEY("i"), KEY("o"), KEY("p"), SPECIAL(KeyDelete) }
        , { KEY("a"), KEY("s"), KEY("d"), KEY("f"), KEY("g"), KEY("h"), KEY("j"), KEY("k"), KEY("l"), SPECIAL(KeyEnter) }
        , { SPECIAL(KeyShiftLeft), KEY("z"), KEY("x"), KEY("c"), KEY("v"), KEY("b"), KEY("n"), KEY("m"), KEY(","), KEY("."), SPECIAL(KeyShiftRight) }
        , { SPECIAL(KeyFlip), SPECIAL(KeySpace), SPECIAL(KeyHide) }
    };
    
#undef KEY
#undef SPECIAL
    
    for (auto &[page, layout] : layouts)
    {
        for (auto &row : layout)
        {
            for (auto &[key, button] : row)
            {
                if (auto* string = std::get_if<String>(&key))
                {
                    button = buttons.add(new TextButton(*string));
                    
                    button->onClick = [=] { this->AppendText(*string); };
                    
                    keyButtons[*string] = button;
                }
                else if (auto* special = std::get_if<SpecialKey>(&key))
                {
                    String text;
                    
                    switch (*special)
                    {
                        case SpecialKey::KeyDelete:     text = "DEL";   break;
                        case SpecialKey::KeySpace:      text = "SPACE"; break;
                        case SpecialKey::KeyEnter:      text = "<-";    break;
                        case SpecialKey::KeyShiftLeft:
                        case SpecialKey::KeyShiftRight: text = "SHIFT"; break;
                        case SpecialKey::KeyHide:       text = "HIDE";  break;
                        case SpecialKey::KeyFlip:       text = "FLIP";  break;
                        default:
                            jassertfalse;
                            break;
                    };
                    
                    button = buttons.add(new TextButton(text));
                    
                    button->onClick = [=] { this->HandleSpecialKey(*special); };
                    
                    specialButtons[*special] = button;
                }
                
                addAndMakeVisible(button);
            }
        }
    }
    
    Shift.AddListenerAndCall(this, &VirtualKeyboard::OnShiftPressed);
}

void VirtualKeyboard::resized()
{
    const int  width        = getWidth() / 11;
    const int  height       = getHeight() / 4;
    const auto buttonBounds = juce::Rectangle<int>(0, 0, width, height);
    
    int rowIndex = 0;
    int colIndex = 0;
    
    for (auto &[page, layout] : layouts)
    {
        rowIndex = 0;
        
        switch (page)
        {
            case Page::Alphabet:
            {
                for (auto &row : layout)
                {
                    colIndex = 0;
                    
                    for (auto &[key, button] : row)
                    {
                        button->setBounds(buttonBounds.withPosition(width * colIndex, height * rowIndex));
                        
                        ++colIndex;
                    }
                    
                    ++rowIndex;
                }
                
                break;
            }
            case Page::Number:
            {
                break;
            }
            case Page::Symbol:
            {
                break;
            }
        };
    }
}

void VirtualKeyboard::AppendText(String character)
{
    Text = Text.Get() + (Shift ? character.toUpperCase()
                               : character.toLowerCase());
    Shift = false;
}

void VirtualKeyboard::HandleSpecialKey(SpecialKey special)
{
    switch (special)
    {
        case SpecialKey::KeyDelete:
        {
            Text = Text.Get().dropLastCharacters(1);
            break;
        }
        case SpecialKey::KeySpace:
        {
            Text = Text.Get() + " ";
            break;
        }
        case SpecialKey::KeyEnter:
        {
            break;
        }
        case SpecialKey::KeyShiftLeft:
        case SpecialKey::KeyShiftRight:
        {
            Shift = !Shift;
            break;
        }
        case SpecialKey::KeyHide:
        {
            break;
        }
        case SpecialKey::KeyFlip:
        {
            break;
        }
        default:
            jassertfalse;
            break;
    };
}

void VirtualKeyboard::OnShiftPressed(bool on)
{
    for (auto &[page, layout] : layouts)
    {
        for (auto &row : layout)
        {
            for (auto &[key, button] : row)
            {
                if (auto* string = std::get_if<String>(&key))
                {
                    button->setButtonText(on ? string->toUpperCase() : string->toLowerCase());
                }
            }
        }
    }
    
    specialButtons[SpecialKey::KeyShiftLeft] ->setToggleState(on, dontSendNotification);
    specialButtons[SpecialKey::KeyShiftRight]->setToggleState(on, dontSendNotification);
}
