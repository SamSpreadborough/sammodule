/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class VirtualKeyboard
: public  Component
, private Trackable
{
public:
    VirtualKeyboard();
    
    enum Page
    {
          Alphabet
        , Number
        , Symbol
    };
    
    Property<Page>   CurrentPage;
    Property<bool>   Shift;
    Property<String> Text;
    
    // Component
    void resized() override;
private:
    enum SpecialKey
    {
          KeyDelete
        , KeySpace
        , KeyEnter
        , KeyShiftLeft
        , KeyShiftRight
        , KeyHide
        , KeyFlip
    };
    
    void AppendText(String character);
    void HandleSpecialKey(SpecialKey special);
    void OnShiftPressed(bool on);
    
    using KeyVariant = std::variant<String, SpecialKey>;
    using KeyPair    = std::pair<KeyVariant, TextButton*>;
    using KeyLayout  = std::vector<std::vector<KeyPair>>;
    
    OwnedArray<TextButton> buttons;
    
    std::map<Page, KeyLayout> layouts;
    std::map<SpecialKey, TextButton*> specialButtons;
    std::map<String    , TextButton*> keyButtons;
};
