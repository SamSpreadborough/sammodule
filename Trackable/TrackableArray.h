/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    
 */
template <class Type>
class TrackableArray
: private Tracker
{
    using SelfType = TrackableArray<Type>;
public:
    //==============================================================================
    /** */
    TrackableArray()
    {
        ASSERT_TRACKABLE_BASE(Type)
    }

    /** */
    TrackableArray (const SelfType& other)
    {
        ASSERT_TRACKABLE_BASE(Type)

        for (auto &element : other)
        {
            Add(element);
        }
    }

    /** */
    TrackableArray (const SelfType&& other)
    {
        ASSERT_TRACKABLE_BASE(Type)

        for (auto &element : other)
        {
            Add(element);
        }
    }

    /** */
    ~TrackableArray() = default;

    //==============================================================================
    /**
     <#Description#>

     @param newElement <#newElement description#>
     */
    void Add (TrackablePointer<Type> newElement)
    {
        if (newElement)
        {
            newElement->AddTracker(this);
            array.add(newElement);
        }
    }

    /**
     <#Description#>

     @param newElement <#newElement description#>
     */
    void Add (Type* newElement)
    {
        if (newElement)
            Add(TrackablePointer<Type>(newElement));
    }

    /**
     <#Description#>

     @param indexToInsertAt <#indexToInsertAt description#>
     @param newElement <#newElement description#>
     */
    void Insert (int indexToInsertAt, Type *newElement)
    {
        if (newElement)
            Insert(indexToInsertAt, TrackablePointer<Type>(newElement));
    }

    /**
     <#Description#>

     @param indexToInsertAt <#indexToInsertAt description#>
     @param newElement <#newElement description#>
     */
    void Insert (int indexToInsertAt, TrackablePointer<Type> newElement)
    {
        if (newElement)
            Insert(indexToInsertAt, newElement.Get());
    }

    /**
     <#Description#>

     @param newElement <#newElement description#>
     @return <#return value description#>
     */
    bool AddIfNotAlreadyThere (Type *newElement)
    {
        if (Contains(newElement))
            return false;

        Add (newElement);
        return true;
    }

    /**
     <#Description#>

     @param newElement <#newElement description#>
     @return <#return value description#>
     */
    bool AddIfNotAlreadyThere (TrackablePointer<Type> newElement)
    {
        return newElement ? AddIfNotAlreadyThere(newElement.Get()) : false;
    }
    //==============================================================================
    /**
     <#Description#>
     */
    void Clear()
    {
        for (size_t i = 0; i < Size(); ++i)
        {
            Remove(i);
        }
    }

    /**
     <#Description#>

     @param index <#index description#>
     */
    void Remove(unsigned index)
    {
        assert(index < Size());

        if (index < Size())
        {
            array[index]->RemoveTracker(this);
            array.remove(index);
        }
    }

    //==============================================================================
    /**
     <#Description#>

     @return <#return value description#>
     */
    size_t Size() const
    {
        return size_t(array.size());
    }

    /**
     <#Description#>

     @return <#return value description#>
     */
    bool IsEmpty() const
    {
        return !Size();
    }

    /**
     <#Description#>

     @return <#return value description#>
     */
    bool IsNotEmpty() const
    {
        return Size();
    }

    /**
     <#Description#>

     @param trackable <#trackable description#>
     @return <#return value description#>
     */
    int IndexOf (Type *trackable) const
    {
        return IndexOf(static_cast<Trackable*>(trackable));
    }

    /**
     <#Description#>

     @param trackable <#trackable description#>
     @return <#return value description#>
     */
    int IndexOf (Trackable *trackable) const
    {
        int i = 0;
        for (auto &element : array)
        {
            if (element.Get() == trackable)
                return i;

            ++i;
        }

        return -1;
    }

    /**
     <#Description#>

     @param trackablePointer <#trackablePointer description#>
     @return <#return value description#>
     */
    int IndexOf (TrackablePointer<Type> trackablePointer) const
    {
        return array.indexOf(trackablePointer);
    }

    /**
     <#Description#>

     @param trackable <#trackable description#>
     @return <#return value description#>
     */
    bool Contains (Type *trackable) const
    {
        int i = 0;
        for (auto &element : array)
        {
            if (element.Get() == trackable)
                return true;

            ++i;
        }

        return false;
    }

    /**
     <#Description#>

     @param trackablePointer <#trackablePointer description#>
     @return <#return value description#>
     */
    int Contains (TrackablePointer<Type> trackablePointer) const
    {
        return array.contains(trackablePointer);
    }

    //==============================================================================
    /**
     <#Description#>

     @param index1 <#index1 description#>
     @param index2 <#index2 description#>
     */
    void Swap(int index1, int index2)
    {
        array.swap(index1, index2);
    }

    /**
     <#Description#>

     @param currentIndex <#currentIndex description#>
     @param newIndex <#newIndex description#>
     */
    void Move(int currentIndex, int newIndex)
    {
        array.move(currentIndex, newIndex);
    }

    //==============================================================================
    /**
     <#Description#>

     @param index <#index description#>
     @return <#return value description#>
     */
    TrackablePointer<Type> Get(int index) const
    {
        assert(index < Size());
        
        return index < Size() ? array[index] : TrackablePointer<Type>();
    }

    /**
     <#Description#>

     @param index <#index description#>
     @return <#return value description#>
     */
    TrackablePointer<Type> operator[] (const int index) const
    {
        return Get(index);
    }

    /**
     <#Description#>

     @return <#return value description#>
     */
    inline TrackablePointer<Type>* begin() const
    {
        return array.begin();
    }

    /**
     <#Description#>

     @return <#return value description#>
     */
    inline TrackablePointer<Type>* end() const
    {
        return array.end();
    }
private:
    //==============================================================================
    // Tracker
    void TrackableWillBeDeleted(Trackable *trackable) override
    {
        if (int index = IndexOf(trackable); index != -1)
            Remove(index);
    }

    //==============================================================================
    Array<TrackablePointer<Type>> array;
};
