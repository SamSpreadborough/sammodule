/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    A wrapper around a raw Trackable pointer that will provide access to it
    and automatically set it to nullptr upon deletion.
 */
template <class TrackableType>
class TrackablePointer
: private Tracker
{
public:
    //==============================================================================
    /** Constructs a null TrackablePointer */
    TrackablePointer() = default;

    /** Constructs an initialised TrackablePointer  */
    TrackablePointer (TrackableType *trackable)
    : object (trackable)
    {}

    /** Copy constructor */
    TrackablePointer (TrackablePointer &other)
    : object (other.object)
    {}

    /** Move constructor */
    TrackablePointer (TrackablePointer &&other)
    : object (other.object)
    {}

    /** Construct a TrackablePointer initialised by a shared pointer */
    TrackablePointer (std::shared_ptr<TrackableType> sharedPtr)
    : object (sharedPtr.get())
    {}

    /** Construct a TrackablePointer initialised by a unique pointer */
    TrackablePointer (std::unique_ptr<TrackableType> uniquePtr)
    : object (uniquePtr.get())
    {}

    /** Construct a TrackablePointer initialised by a weak pointer */
    TrackablePointer (std::weak_ptr<TrackableType> weakPtr)
    : object (weakPtr.lock().get())
    {}

    /** Construct a TrackablePointer initialised by a scoped pointer */
    TrackablePointer (ScopedPointer<TrackableType> scopedPtr)
    : object (scopedPtr.get())
    {}

    /** Destructor */
    ~TrackablePointer() { Reset(); }

    //==============================================================================
    /** Copy assignment */
    TrackablePointer &operator= (TrackablePointer &other)
    {
        Reset(other.object);
        return *this;
    }

    /** Assignment operator */
    TrackablePointer &operator= (TrackableType *object)
    {
        Reset (object);
        return *this;
    }

    /** Move assignment */
    TrackablePointer& operator= (TrackablePointer &&other)
    {
        Reset (other.object);
        return *this;
    }

    //==============================================================================
    /** Returns the object that this TrackablePointer refers to. */
    operator TrackableType*()   const { return object;  }

    /** Returns the object that this TrackablePointer refers to. */
    TrackableType* Get()        const { return object;  }

    /** Returns the object that this TrackablePointer refers to. */
    TrackableType& operator*()  const { return *object; }

    /** Lets you access methods and properties of the object that this TrackablePointer refers to. */
    TrackableType* operator->() const { return object;  }

    //==============================================================================

    /** Sets the Trackable object to a new pointer

        @param newObject the new object to wrap
     */
    void Reset (TrackableType* newObject = nullptr)
    {
        if (Trackable *trackable = dynamic_cast<Trackable*>(object))
            trackable->RemoveTracker(this);

        object = newObject;

        if (Trackable *trackable = dynamic_cast<Trackable*>(object))
            trackable->AddTracker(this);
    }

    /** Sets the Trackable object to the pointer of another TrackablePointer.

        @param other the TrackablePointer to reset this to.
     */
    void Reset (TrackablePointer& other)
    {
        Reset (other.object);
    }

private:
    //==============================================================================
    // Tracker
    void TrackableWillBeDeleted(Trackable *trackable) override
    {
        Reset();
    }

    TrackableType* object = nullptr;
};

//==============================================================================
template
< typename TrackableType1, typename TrackableType2>
bool operator== (TrackableType1* pointer1
                 , const TrackablePointer<TrackableType2>& pointer2)
{
    return pointer1 == pointer2.Get();
}

template
< typename TrackableType1, typename TrackableType2>
bool operator!= (TrackableType1* pointer1
                 , const TrackablePointer<TrackableType2>& pointer2)
{
    return pointer1 != pointer2.Get();
}

template
< typename TrackableType1, typename TrackableType2>
bool operator== (const TrackablePointer<TrackableType1>& pointer1
                 , TrackableType2* pointer2)
{
    return pointer1.Get() == pointer2;
}

template
< typename TrackableType1, typename TrackableType2>
bool operator!= (const TrackablePointer<TrackableType1>& pointer1
                 , TrackableType2* pointer2)
{
    return pointer1.Get() != pointer2;
}

template
< typename TrackableType1, typename TrackableType2>
bool operator== (  const TrackablePointer<TrackableType1>& pointer1
                 , const TrackablePointer<TrackableType2>& pointer2)
{
    return pointer1.Get() == pointer2.Get();
}

template
<typename TrackableType1, typename TrackableType2>
bool operator!= (  const TrackablePointer<TrackableType1>& pointer1
                 , const TrackablePointer<TrackableType2>& pointer2)
{
    return pointer1.Get() != pointer2.Get();
}

template <class TrackableType>
bool operator== (decltype (nullptr)
                 , const TrackablePointer<TrackableType>& pointer)
{
    return pointer.Get() == nullptr;
}

template <class TrackableType>
bool operator!= (decltype (nullptr)
                 , const TrackablePointer<TrackableType>& pointer)
{
    return pointer.Get() != nullptr;
}

template <class TrackableType>
bool operator== (const TrackablePointer<TrackableType>& pointer
                 , decltype (nullptr))
{
    return pointer.Get() == nullptr;
}

template <class TrackableType>
bool operator!= (const TrackablePointer<TrackableType>& pointer
                 , decltype (nullptr))
{
    return pointer.Get() != nullptr;
}

