/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#define ASSERT_TRACKABLE_BASE(base) static_assert(std::is_base_of<Trackable, base>::value \
                                                                , "Type must derive from Trackable");
//==============================================================================
/**
    A TokenHolder owns a shared pointer to a Token struct, that owns a pointer to
    an owning object. When the owning object is deleted, it will delete the
    shared pointer.

    Objects can get a weak pointer to the Token so they can find if the
    owning object has been deleted.

    Typically you would inherit from TokenHolder, and initialise it with
    'this'.
 */
template <class TokenClass>
class TokenHolder
{
public:
    //==============================================================================
    /**
        Owns a pointer to the owning TokenClass object.
        TokenHolder owns a std::shared_ptr<Token> object that tracks the lifetime
        of the TokenHolder object. The Token::Ptr is a std::weak_ptr that can be
        used to find out when the Token expires. The Token will expire when the
        owning TokenHolder object expires. Because of this, we can store a raw pointer
        to the TokenHolder pointer that can be accessed via the std::weak_ptr<Token>.
        Objects can store a Token::Ptr so can check that a TokenHolder object
        is alive.
     */
    struct Token
    {
        Token(TokenClass *token) : token(token) {}

        TokenClass *token;
    };

    using Ptr = std::weak_ptr<Token>;

    //==============================================================================
    /** Constructor
     */
    TokenHolder(TokenClass *owner)
    : token(new Token(owner))
    {}

    /** Destructor
     */
    virtual ~TokenHolder() { token.reset(); }

    //==============================================================================
    /** @return a std::weak_ptr<Token> to TokenHolder member std::shared_ptr<Token>.
     */
    Ptr GetToken() const { return token; }
private:
    //==============================================================================
    std::shared_ptr<Token> token;
};

class Trackable;

//==============================================================================
/**
    A listener class for Trackable items. Gets notified when a listened to
    Trackable item is about to be deleted.
    Can listen to multiple Trackable objects, and when a Trackable object
    is about to be deleted, will callback into TrackableWillBeDeleted.

    Implement TrackableWillBeDeleted to get notified when the Trackable object
    is about to be deleted.
 */
class Tracker
: public TokenHolder<Tracker>
{
public:
    //==============================================================================
    /** Constructor
     */
    Tracker()
    : TokenHolder<Tracker>(this)
    {}

    //==============================================================================
    /** Called when the Trackable destructor is called. Trackable is still alive
        when passed as an argument.

        @param trackable the Trackable object that will be deleted
     */
    virtual void TrackableWillBeDeleted(Trackable *trackable) = 0;
};

//==============================================================================
/**
    A base class for objects that wish to let Tracker objects know when they are
    about to be deleted. This is useful as it allows Tracker objects relying on
    Trackable's to do any clean up code they need to do before they are
    deleted. Crucially, the Trackable item is still alive when it raises the
    Tracker
 */
class Trackable
{
    using TrackerVec   = std::vector< TokenHolder<Tracker>::Ptr >;
    using TrackerVecIt = TrackerVec::iterator;
protected:
    //==============================================================================
    /** Constructs a Trackable object
     */
    Trackable() = default;

public:
    //==============================================================================
    /** Destructor.
     When called, will notify all Trackers that are listening to it.
     */
    virtual ~Trackable()
    {
        for (int i = 0; i < trackerTokenVec.size(); ++i)
        {
            TokenHolder<Tracker>::Ptr &token = trackerTokenVec[i];

            if (!token.expired())
                token.lock()->token->TrackableWillBeDeleted(this);
        }
    }

    /** Add a Tracker object listener.
        @param tracker Tracker object to get notified when Trackable is deleted.
     */
    void AddTracker(Tracker *tracker)
    {
        if (!IsTracking(tracker))
            trackerTokenVec.push_back(tracker->GetToken());
    }

    /** Remove a Tracker so that it won't get notified when a Trackable is deleted.
        @param tracker the Tracker object to remove.
     */
    void RemoveTracker(Tracker *tracker)
    {
        if (auto it = GetIterator(tracker); it != trackerTokenVec.end())
            trackerTokenVec.erase(it);
    }

    /** @param tracker the Tracker to check.
        @return true if listening to Trackable.
     */
    bool IsTracking(Tracker *tracker)
    {
        return GetIterator(tracker) != trackerTokenVec.end();
    }


    /** @return the number of Trackable's being tracked.
     */
    size_t Size() const { return trackerTokenVec.size(); }
private:
    //==============================================================================
    TrackerVecIt GetIterator(Tracker *tracker)
    {
        return std::find_if(  trackerTokenVec.begin()
                            , trackerTokenVec.end()
                            , [=](auto &token)
                            {
                                return tracker
                                && !token.expired()
                                && token.lock()->token == tracker;
                            });
    }

    //==============================================================================
    TrackerVec trackerTokenVec;
};
