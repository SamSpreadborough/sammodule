/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    A syncable application state that can be set via either a remote or local peer.
    The state is stored via a ValueTree and kept in sync using ValueTreeSynchroniser.
 */
class PeerState
{
protected:
    ValueTree state;
    
    PeerState();
    PeerState(const PeerState &other) = delete;
};

//==============================================================================
/**
    Sets up a TCP peer-to-peer network connection.
    Connection can send application state, ValueTrees, or Files.
    No need to set up server/client as this is decided automatically.
    Once connected, will send a ping/pong message every second to report that the
    two peers are connected; also used for latency.
 */
class NetworkPeer
: public  PeerState
, private InterprocessConnectionServer
, private InterprocessConnection
, private MultiTimer
, private ValueTreeSynchroniser
{
public:
    NetworkPeer();
    
    /** Connect to a remote peer. Provide a NetworkServiceDiscovery::Service object to
        connect to. The lower UID of the two will set up the server; the other will
        connect.
     
        @param remotePeer The NetworkServiceDiscovery::Service object for the remote
        peer to connect to
     */
    void Connect(const NetworkServiceDiscovery::Service &remotePeer);
    
    /** @return true if connected, else false.
     */
    bool IsConnected() const;
    
    /** Disconnects from the remote peer.
     */
    void Disconnect();
    
    /** Send a ValueTree object to the remote peer.

        @param tree The ValueTree to send.
        @return true if successful, else false.
     */
    bool SendValueTree(const ValueTree &tree);
    
    /** Send a file to the remote peer.

        @param file The file to send.
        @return true if successful, else false.
     */
    bool SendFile(const File &file);
    
    /** Send an application state request. If received, the remote peer will call
        ValueTreeSynchroniser::sendFullSyncCallback.
     
        @return true if successful, else false.
     */
    bool SendStateRequest();
    
    const String &GetLocalPeerUid() const;
    
    /** @return The NetworkServiceDiscovery::Service object representing the remote peer.
     */
    std::optional<const NetworkServiceDiscovery::Service> GetRemotePeer() const;
    
    double GetLocalSystemTime();
    double GetRemoteSystemTimeDelta();
    
    //==============================================================================
    class Listener
    {
    public:
        virtual ~Listener() = default;
        virtual void OnPeerConnected   (NetworkPeer *peer)                           = 0;
        virtual void OnPeerUnresponsive(NetworkPeer *peer)                           = 0;
        virtual void OnPeerPingReceived(NetworkPeer *peer, double latency)           = 0;
        virtual void OnPeerDisconnected(NetworkPeer *peer)                           = 0;
        virtual void OnMessageReceived (NetworkPeer *peer, const ValueTree &tree)    = 0;
        virtual void OnFileReceived    (NetworkPeer *peer, const File &message)      = 0;
    };
    
    void AddListener    (Listener *listener);
    void RemoveListener (Listener *listener);
private:
    //==============================================================================
    
    ValueTree CreatePingTree();
    bool SendPing();
    
    ValueTree CreateSystemTimeRequestTree();
    bool SendSystemTimeRequest();
    
    ValueTree CreateSystemTimeTree();
    bool SendSystemTime();
    
    ValueTree CreateStateRequest();
    
    // InterprocessConnectionServer
    juce::InterprocessConnection *createConnectionObject() override;
    
    // InterprocessConnection
    void connectionMade()                                  override;
    void connectionLost()                                  override;
    void messageReceived(const juce::MemoryBlock &message) override;
    
    enum TimerIdentifier : int
    {
          IdConnect
        , IdPing
    };
    
    // MultiTimer
    void timerCallback(int timerID) override;
    
    // ValueTreeSynchroniser
    void stateChanged(const void *encodedChange, size_t encodedChangeSize) override;
    
    void HandlePing(const ValueTree &tree);
    void HandleSystemTime(const ValueTree &tree);
    void HandleFile(const String &fileName, MemoryInputStream &memStream);
    
    //==============================================================================
    const String localPeerUid;
    std::optional<NetworkServiceDiscovery::Service> remotePeer;
    
    ListenerList<Listener> listenerList;
    Time lastPing;
    double systemTimeDelta;
};
