/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    A Property-led Ableton Link interface.
 
    @see ableton::Link
 */
class AbletonLinkControl
: public  Trackable
{
public:
    //==============================================================================
    /** Constructs a new AbletonLinkControl object. Default tempo is 120.0.
     */
    AbletonLinkControl(double defaultTempo = 120.0);
    
    Property<bool> Enabled;       //< Is Link currently enabled?
    Property<bool> StartStopSync; //< Is start/stop synchronization enabled?
    
    ConstProperty<AbletonLinkControl, double> Tempo;     //< The session tempo.
    ConstProperty<AbletonLinkControl, bool>   StartStop; //< The state of start/stop isPlaying changes
    ConstProperty<AbletonLinkControl, size_t> NumPeers;  //< How many peers are currently connected in a Link session?
private:
    //==============================================================================
    void OnEnabledChanged      (bool enabled);
    void OnStartStopSyncChanged(bool enabled);
    void OnTempoChanged        (double tempo);
    void OnIsPlayingChanged    (bool isPlaying);
    void OnNumPeersChanged     (size_t numPeers);
    
    std::unique_ptr<ableton::Link> link;
};
