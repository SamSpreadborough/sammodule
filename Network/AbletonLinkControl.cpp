/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

AbletonLinkControl::AbletonLinkControl(double defaultTempo)
: Enabled      (false)
, StartStopSync(false)
, Tempo        (defaultTempo)
, StartStop    (false)
, NumPeers     (0)
, link         (new ableton::Link(Tempo))
{
    Enabled      .AddListenerAndCall(this, [=](bool enabled) { OnEnabledChanged(enabled); });
    StartStopSync.AddListenerAndCall(this, [=](bool enabled) { OnStartStopSyncChanged(enabled); });
    
    link->setNumPeersCallback ([=](size_t num)     { OnNumPeersChanged(num); });
    link->setTempoCallback    ([=](double tempo)   { OnTempoChanged(tempo); });
    link->setStartStopCallback([=](bool isPlaying) { OnIsPlayingChanged(isPlaying); });
}

void AbletonLinkControl::OnEnabledChanged(bool enabled)
{
    link->enable(enabled);
}

void AbletonLinkControl::OnStartStopSyncChanged(bool enabled)
{
    link->enableStartStopSync(enabled);
}

void AbletonLinkControl::OnTempoChanged(double tempo)
{
    Tempo = tempo;
}

void AbletonLinkControl::OnIsPlayingChanged(bool isPlaying)
{
    StartStop = isPlaying;
}

void AbletonLinkControl::OnNumPeersChanged(size_t numPeers)
{
    NumPeers = numPeers;
}
