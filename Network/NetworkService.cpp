/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

NetworkService::NetworkService(  const String& serviceName
                               , int broadcastPort
                               , int connectionPort)
: uid        (Uuid().toString())
, serviceName(serviceName)
, broadcastPort(broadcastPort)
, connectionPort(connectionPort)
, advertiser (  serviceName
              , uid
              , broadcastPort
              , connectionPort)
, serviceList (  serviceName
               , broadcastPort)
, knownServices(serviceList.getServices())
{
    serviceList.onChange = [=]{ this->OnServiceListChanged(); };
}

const String &NetworkService::GetUid() const
{
    return uid;
}

const String &NetworkService::GetServiceName() const
{
    return serviceName;
}

const int NetworkService::GetBroadcastPort() const
{
    return broadcastPort;
}

const int NetworkService::GetConnectionPort() const
{
    return connectionPort;
}

void NetworkService::OnServiceListChanged()
{
    const auto newServices = serviceList.getServices();
    
    for (const auto &s : newServices)
    {
        auto it = std::find_if(  knownServices.begin()
                               , knownServices.end()
                               , [=, &s](const NetworkServiceDiscovery::Service &it)
                               {
                                   return it.description == s.description;
                               });
        
        if (it == knownServices.end())
        {
            InstanceFound.Notify(s);
        }
    }
    
    for (const auto &s : knownServices)
    {
        auto it = std::find_if(  newServices.begin()
                               , newServices.end()
                               , [=, &s](const NetworkServiceDiscovery::Service &it)
                               {
                                   return it.description == s.description;
                               });
        
        if (it == newServices.end())
        {
            InstanceLost.Notify(s);
        }
    }
    
    knownServices = newServices;
}
