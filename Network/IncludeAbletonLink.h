/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#if JUCE_MAC
#define LINK_PLATFORM_MACOSX 1
#elif JUCE_WINDOWS
#define LINK_PLATFORM_WINDOWS 1
#elif JUCE_LINUX
#define LINK_PLATFORM_LINUX 1
#endif

#include <ableton/Link.hpp>
