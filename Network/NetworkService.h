/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/**
    TODO: Comments
 */
class NetworkService
{
public:
    NetworkService(  const String& serviceName
                   , int broadcastPort
                   , int connectionPort);
    
    ConstSignal<NetworkService, NetworkServiceDiscovery::Service> InstanceFound;
    ConstSignal<NetworkService, NetworkServiceDiscovery::Service> InstanceLost;
    
    const String &GetUid()            const;
    const String &GetServiceName()    const;
    const int     GetBroadcastPort()  const;
    const int     GetConnectionPort() const;
private:
    void OnServiceListChanged();
    
    const String uid;
    const String serviceName;
    const int    broadcastPort;
    const int    connectionPort;
    
    NetworkServiceDiscovery::Advertiser           advertiser;
    NetworkServiceDiscovery::AvailableServiceList serviceList;
    
    std::vector<NetworkServiceDiscovery::Service> knownServices;
};
