/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace
{
    const String stateMapId  ("state=");
    const String xmlTypeId   ("xml=");
    const String fileTypeId  ("file=");
    const String hostId      ("host");
    const String timeId      ("time");
    const String pingType    ("ping");
    const String systemTime  ("systemtime");
}

PeerState::PeerState()
: state ("state")
{
}

NetworkPeer::NetworkPeer()
: ValueTreeSynchroniser (state)
, localPeerUid          (Uuid().toString())
, remotePeer            (std::nullopt)
{
    
}

void NetworkPeer::Connect(const NetworkServiceDiscovery::Service &remotePeer)
{
    this->remotePeer = remotePeer;
    
    if (remotePeer.instanceID.compareNatural(localPeerUid) > 0)
    {
        beginWaitingForSocket(remotePeer.port);
    }
    else
    {
        startTimer(IdConnect, 500);
    }
}

bool NetworkPeer::IsConnected() const
{
    return isConnected();
}

void NetworkPeer::Disconnect()
{
    remotePeer = std::nullopt;
    systemTimeDelta = 0.0;
    disconnect();
    stopTimer(TimerIdentifier::IdPing);
    stopTimer(TimerIdentifier::IdConnect);
}

bool NetworkPeer::SendFile(const File &file)
{
    MemoryBlock block;
    MemoryOutputStream memStream(block, false);
    memStream.writeString(fileTypeId + file.getFileName());
    
    FileInputStream fileStream(file);
    
    if (fileStream.openedOk())
    {
        memStream.writeFromInputStream(fileStream, fileStream.getTotalLength());
        
        return sendMessage(block);
    }
    
    return false;
}

bool NetworkPeer::SendValueTree(const ValueTree &tree)
{
    MemoryBlock block;
    MemoryOutputStream stream(block, false);
    stream.writeString(xmlTypeId);
    
    tree.writeToStream(stream);
    
    return sendMessage(block);
}

const String &NetworkPeer::GetLocalPeerUid() const
{
    return localPeerUid;
}

std::optional<const NetworkServiceDiscovery::Service> NetworkPeer::GetRemotePeer() const
{
    if (isConnected() && remotePeer)
    {
        return *remotePeer;
    }
    else
    {
        return std::nullopt;
    }
}

double NetworkPeer::GetLocalSystemTime()
{
    return Time::getCurrentTime().getMillisecondCounterHiRes();
}

double NetworkPeer::GetRemoteSystemTimeDelta()
{
    return systemTimeDelta;
}

void NetworkPeer::AddListener(Listener *listener)
{
    listenerList.add(listener);
}

void NetworkPeer::RemoveListener(Listener *listener)
{
    listenerList.remove(listener);
}

ValueTree NetworkPeer::CreatePingTree()
{
    ValueTree tree(pingType);
    tree.setProperty(hostId, localPeerUid        , nullptr);
    tree.setProperty(timeId, GetLocalSystemTime(), nullptr);
    
    return tree;
}

bool NetworkPeer::SendPing()
{
    return SendValueTree(CreatePingTree());
}

ValueTree NetworkPeer::CreateSystemTimeRequestTree()
{
    ValueTree tree(systemTime);
    
    return tree;
}

bool NetworkPeer::SendSystemTimeRequest()
{
    return SendValueTree(CreateSystemTimeRequestTree());
}

ValueTree NetworkPeer::CreateSystemTimeTree()
{
    ValueTree tree(systemTime);
    tree.setProperty(hostId, localPeerUid        , nullptr);
    tree.setProperty(timeId, GetLocalSystemTime(), nullptr);
    
    return tree;
}

bool NetworkPeer::SendSystemTime()
{
    return SendValueTree(CreateSystemTimeTree());
}

ValueTree NetworkPeer::CreateStateRequest()
{
    ValueTree tree(stateMapId);
    return tree;
}

bool NetworkPeer::SendStateRequest()
{
    return SendValueTree(CreateStateRequest());
}

juce::InterprocessConnection *NetworkPeer::createConnectionObject()
{
    return this;
}

void NetworkPeer::connectionMade()
{
    lastPing = Time::getCurrentTime();
    SendSystemTimeRequest();
    startTimer(TimerIdentifier::IdPing, 1000);
    
    listenerList.call(&Listener::OnPeerConnected, this);
    
    DBG(String::formatted("[%s] -> [%s]", localPeerUid.toRawUTF8(), remotePeer->instanceID.toRawUTF8()));
}

void NetworkPeer::connectionLost()
{
    stopTimer(TimerIdentifier::IdPing);
    
    listenerList.call(&Listener::OnPeerDisconnected, this);
}

void NetworkPeer::messageReceived(const juce::MemoryBlock &message)
{
    MemoryInputStream memStream(message, false);
    const String header = memStream.readString();
    const String type   = header.upToFirstOccurrenceOf("=", true, true);
    
    if (type == xmlTypeId)
    {
        ValueTree tree = ValueTree::readFromStream(memStream);
        
        if (tree.isValid())
        {
            if (tree.hasType(pingType))
            {
                HandlePing(tree);
            }
            else if (tree.hasType(systemTime))
            {
                HandleSystemTime(tree);
            }
            else if (tree.hasType(stateMapId))
            {
                ValueTreeSynchroniser::sendFullSyncCallback();
            }
            else
            {
                listenerList.call(&Listener::OnMessageReceived, this, tree);
            }
        }
    }
    else if (type == fileTypeId)
    {
        const String fileName = header.fromFirstOccurrenceOf("=", false, true);
        HandleFile(fileName, memStream);
    }
    else if (type == stateMapId)
    {
        MemoryBlock stateBlock;
        stateBlock.setSize(memStream.getNumBytesRemaining());
        memStream.read(stateBlock.getData(), int(stateBlock.getSize()));;
        
        ValueTreeSynchroniser::applyChange(  state
                                           , stateBlock.getData()
                                           , stateBlock.getSize()
                                           , nullptr);
    }
}

void NetworkPeer::timerCallback(int timerID)
{
    switch (TimerIdentifier(timerID))
    {
        case IdConnect:
        {
            if (IsConnected())
            {
                stopTimer(TimerIdentifier::IdConnect);
            }
            else
            {
                if (remotePeer)
                {
                    connectToSocket(  remotePeer->address.toString()
                                    , remotePeer->port
                                    , 1500);
                }
            }
            break;
        }
        case IdPing:
        {
            const RelativeTime diff = Time::getCurrentTime() - lastPing;
            
            if (diff.inSeconds() > 3)
            {
                listenerList.call(&Listener::OnPeerUnresponsive, this);
            }
            
            SendPing();
            break;
        }
        default:
            break;
    }
}

void NetworkPeer::stateChanged(const void *encodedChange, size_t encodedChangeSize)
{
    MemoryBlock block;
    MemoryOutputStream stream(block, false);
    stream.writeString(stateMapId);
    stream.write(encodedChange, encodedChangeSize);
    
    sendMessage(block);
}

void NetworkPeer::HandlePing(const ValueTree &tree)
{
    if (tree.getProperty(hostId) == localPeerUid)
    {
        lastPing = Time::getCurrentTime();
        
        const double latency = GetLocalSystemTime() - double(tree.getProperty(timeId));
        listenerList.call(&Listener::OnPeerPingReceived, this, latency);
    }
    else
    {
        SendValueTree(tree);
    }
}

void NetworkPeer::HandleSystemTime(const ValueTree &tree)
{
    if (tree.hasProperty(hostId) && tree.hasProperty(timeId))
    {
        systemTimeDelta = GetLocalSystemTime() - double(tree.getProperty(timeId));
    }
    else
    {
        SendSystemTime();
    }
}

void NetworkPeer::HandleFile(const String &fileName, MemoryInputStream &memStream)
{
    const File tempDir  = File::getSpecialLocation(File::SpecialLocationType::tempDirectory);
    const File file     = tempDir.getSiblingFile(fileName);
    
    FileOutputStream fileStream(file);
    
    if (fileStream.openedOk())
    {
        fileStream.writeFromInputStream(memStream, memStream.getTotalLength());
        
        listenerList.call(&Listener::OnFileReceived, this, file);
    }
}
