/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */


//==============================================================================
/** A ConstSignal wrapper around a variable. Use it like you would do the variable
    type, but get notified when the value changes.

    A ConstProperty is only settable by the SetterClass, but anything can
    add a listener to it.

    @code
    ConstProperty<SetterClass, String> property;

    property.AddListener(this, [=](String value)
    {
        // Do something
    });

    property = "new string";
    // Listener function is now called
 
    @endcode

    When adding listeners, you can optionally use AddListenerAndCall to immediately
    get notified with the current value, or you could optionally use
    AddListenerOldNew to get notified when the value changes with the old value
    and the new value in the callback.

    ConstProperty's can also be constructed to take a filtering function, that
    can be used to perform a check on a value before it is set.
    There is a constructor overload that takes a Min and Max value that uses
    this filtering function.

    @see ConstSignal, Trackable, Property, BlockableConnection, ConnectionBlocker
 */
template <class Setter
        , class PropertyType>
class ConstProperty
    : public ConstSignal<Setter, PropertyType>
{
    friend Setter;
    friend class ValueAdaptor;
public:
    //==============================================================================
    using SelfType       = ConstProperty<Setter, PropertyType>;
    using BaseType       = ConstSignal  <Setter, PropertyType>;
    using FilterFunction = std::function<PropertyType(PropertyType)>;
    using CallbackOldNew = std::function<void(PropertyType, PropertyType)>;

    //==============================================================================
    /** Constructs a ConstProperty with a default value.
     */
    ConstProperty(PropertyType value = PropertyType())
        : value (value)
    {}

    /** Constructs a ConstProperty with an initial value, and a min and max
        range that will be used when setting future values.
     */
    ConstProperty(PropertyType value, PropertyType min, PropertyType max)
        : filterFunction ([=](PropertyType value){ return jlimit(min, max, value); })
        , value          (value)
    {}

    /** Constructs a ConstProperty with an initial value and a custom
        filtering function.
     */
    ConstProperty(PropertyType value, FilterFunction filterFunction)
        : filterFunction (filterFunction)
        , value          (value)
    {}

    /** Constructs a ConstProperty with the same value and filtering function.
        Does not copy the listeners.
     */
    ConstProperty(const SelfType &other)
        : BaseType       (other)
        , filterFunction (other.filterFunction)
        , value          (other.value)
    {}

    /** Move constructor
     */
    ConstProperty(SelfType &&other)
        : value                 (std::move(other.value))
        , oldValue              (std::move(other.oldValue))
        , filterFunction        (std::move(other.filterFunction))
        , BaseType::connections (std::move(other.connections))
    {}

    
    //==============================================================================
    template <class ObjectClass, class ... Args>
    /** Adds a listener to this Signal and immediately calls the callback.

        @param listener The Trackable listener to add. This should be a pointer to the
                        object whose member function you'd like to call.
        @param call The member function to call
        @return The blockable for this connection.
     */
    Blockable::Ptr AddListenerAndCall(  Trackable  *listener
                                      , void (ObjectClass::*call)(PropertyType value, Args ... args)
                                      , Args ... args)
    {
        return AddListenerAndCall(listener, [=](PropertyType value)
        {
            ((ObjectClass*)listener->*call)(value, args...);
        });
    }
    
    //==============================================================================
    /** Adds a listener to this Signal and immediately calls the callback.
     
        @param callback The functor to be called
        @param listener optional listener for use with Trackable
        @return The blockable connection object for use with ConnectionBlocker
     */
    Blockable::Ptr AddListenerAndCall(  Trackable                           *listener
                                      , typename BaseType::CallbackVariant &&callback)
    {
        auto connection = BaseType::AddListener(listener, std::move(callback));
        BaseType::Call(callback, value);
        return connection;
    }
    
    /** Add a Signal as a listener and immediately calls the callback. When this Signal changes, it will notify the
        Signal.
     
        @param other The Signal that will listen
        @return The blockable connection object for use with ConnectionBlocker
     */
    Blockable::Ptr AddListenerAndCall(SelfType &other)
    {
        return AddListenerAndCall(&other, [=, &other](PropertyType newValue)
        {
            other.HowToSet(newValue);
        });
    }
    
    //==============================================================================
    template <class ObjectClass, class ... Args>
    /** Adds a listener to this Property that, when raised, will callback with
        the current and old value.
     
        @param listener The Trackable listener to add. This should be a pointer to the
        object whose member function you'd like to call.
        @param call The member function to call
        @return The blockable for this connection.
     */
    Blockable::Ptr AddListenerOldNew(  Trackable  *listener
                                     , void (ObjectClass::*call)(  PropertyType oldValue
                                                                 , PropertyType newValue
                                                                 , Args ... args)
                                     , Args ... args)
    {
        return AddListenerOldNew(listener, [=](  PropertyType oldValue
                                               , PropertyType newValue)
        {
            ((ObjectClass*)listener->*call)(  oldValue
                                            , newValue
                                            , args ...);
        });
    }
    
    /** Adds a listener to this Property that, when raised, will callback with
        the current and old value.
     
        @param callback The functor to be called
        @param listener optional listener for use with Trackable
        @return The blockable connection object for use with ConnectionBlocker
     */
    Blockable::Ptr AddListenerOldNew(  Trackable       *listener
                                     , CallbackOldNew &&callbackOldNew)
    {
        auto callback = [=](PropertyType value){ callbackOldNew(std::ref(oldValue), value); };
        return BaseType::AddListener(listener, callback);
    }
    
    //==============================================================================
    template <class ObjectClass, class ... Args>
    /** Adds a listener to this Property that, when raised, will callback with
        the current and old value. This will immediately call the callback.
     
        @param listener The Trackable listener to add. This should be a pointer to the
        object whose member function you'd like to call.
        @param call The member function to call
        @return The blockable for this connection.
     */
    Blockable::Ptr AddListenerOldNewAndCall(  Trackable *listener
                                            , void (ObjectClass::*call)(  PropertyType oldValue
                                                                        , PropertyType newValue
                                                                        , Args ... args)
                                            , Args ... args)
    {
        return AddListenerOldNewAndCall(listener, [=](  PropertyType oldValue
                                                      , PropertyType newValue)
        {
            ((ObjectClass*)listener->*call)(  oldValue
                                            , newValue
                                            , args ...);
        });
    }
    
    /** Adds a listener to this Property that, when raised, will callback with
        the current and old value. This will immediately call the callback.
     
        @param callback The functor to be called
        @param listener optional listener for use with Trackable
        @return The blockable connection object for use with ConnectionBlocker
     */
    Blockable::Ptr AddListenerOldNewAndCall(  Trackable       *listener
                                            , CallbackOldNew &&callbackOldNew)
    {
        auto callback = [=](PropertyType value){ callbackOldNew(std::ref(oldValue), value); };
        return AddListenerAndCall(listener, callback);
    }

    //==============================================================================
    /** Get the value of the Property.

        @return The property value
     */
    const          PropertyType &Get()        const { return value; }
    operator const PropertyType &()           const { return Get(); }
    const          PropertyType *operator->() const { return &value; }
    
    /** ConstProperty is capable to adding filters to it to only allow values to
        be set under a custom situation. If the ConstProperty has a filter
        constructed, you can perform a check on the value before electing to
        set it. If there is no filter set, then this will always return true.

        @param check The value to check
        @return True if the value fulfils the filter and is able to be set, else false
     */
    bool IsValid(const PropertyType& check) const { return filterFunction(check); }

    /** Useful for interfacing with juce::PropertyComponent classes.
        Add converting functions to/from juce::var.
        Returns the internal Value object.
     */
    const Value &GetPropertyAsValue(  std::function<var(PropertyType)> toVar
                                    , std::function<PropertyType(var)> fromVar)
    {        
        if (!juceValue.get())
            juceValue.reset(new Value(new ValueAdaptor(*this, toVar, fromVar)));
        
        return *juceValue.get();
    }
    
    /** Useful for interfacing with juce::PropertyComponent classes.
        Returns the internal Value object.
     */
    const Value &GetPropertyAsValue()
    {
        return GetPropertyAsValue(  [=](PropertyType v){ return v; }
                                  , [=](var v)         { return v; });
    }
protected:
    //==============================================================================
    SelfType &operator=(const SelfType     &rhs)   { Set(rhs.value); return *this; }
    SelfType &operator=(const PropertyType &value) { Set(value);     return *this; }
    
    void HowToSet(PropertyType newValue) override
    {
        Set(newValue);
    }
    
    /** Sets the ConstProperty with this value. If there is a filter function set
        for values, it will perform the check before setting the internal value.
        Once set, will emit to all Listeners

        @param newValue The new value to be set in the ConstProperty
     */
    void Set(PropertyType newValue)
    {
        newValue = filterFunction(newValue);

        if (value != newValue)
        {
            oldValue = value;
            value    = newValue;
            
            BaseType::Notify(value);
        }
    }

    FilterFunction filterFunction = [=](PropertyType value){ return value; };
    
    PropertyType value;
    PropertyType oldValue;
    
private:
    class ValueAdaptor
    : public  Value::ValueSource
    , private Trackable
    {
    public:
        ValueAdaptor(  SelfType &property
                     , std::function<var(PropertyType)> toVar
                     , std::function<PropertyType(var)> fromVar)
        : property (property)
        , toVar    (toVar)
        , fromVar  (fromVar)
        {
            property.AddListener(this, [=]{ sendChangeMessage(true); });
        }
        
        juce::var getValue() const               override { return toVar(property.Get());     }
        void setValue(const juce::var &newValue) override { property.Set(fromVar(newValue)); }
    private:
        SelfType &property;
        
        std::function<var(PropertyType)> toVar;
        std::function<PropertyType(var)> fromVar;
    };
    
    std::shared_ptr<Value> juceValue;
};

#define EQUALITY_OPERATOR_OVERLOAD(OPERATOR) \
template <class Setter, class Type> \
inline bool operator OPERATOR (const ConstProperty<Setter, Type> &lhs \
                             , const Type &rhs) \
{ return lhs.Get() OPERATOR rhs; } \
\
template <class Setter, class Type> \
inline bool operator OPERATOR (const Type &lhs \
                             , const ConstProperty<Setter, Type> &rhs) \
{ return lhs OPERATOR rhs.Get(); } \
\
template <class Setter, class Type> \
inline bool operator OPERATOR (const ConstProperty<Setter, Type> &lhs \
                             , const ConstProperty<Setter, Type> &rhs) \
{ return lhs.Get() OPERATOR rhs.Get(); }

EQUALITY_OPERATOR_OVERLOAD(==)
EQUALITY_OPERATOR_OVERLOAD(!=)
EQUALITY_OPERATOR_OVERLOAD(< )
EQUALITY_OPERATOR_OVERLOAD(> )
EQUALITY_OPERATOR_OVERLOAD(<=)
EQUALITY_OPERATOR_OVERLOAD(>=)

#undef CONSTPROPERTY_OPERATOR_OVERLOAD

//==============================================================================
/**
    A variable wrapper that provides a callback to a settable function
    when the value is changed. This Property is publically settable by all.
 */
template <class PropertyType>
class Property
: public ConstProperty<void, PropertyType>
{
    using SelfType = Property     <PropertyType>;
    using BaseType = ConstProperty<void, PropertyType>;
public:
    //==============================================================================
    /** Constructs a Property with a default value.
     */
    Property(const PropertyType &value = PropertyType())
    : BaseType(value)
    {}

    /** Constructs a Property with an initial value and a custom
        filtering function.
     */
    Property(const PropertyType &value, typename BaseType::FilterFunction filterFunction)
    : BaseType(value, filterFunction)
    {}

    /** Constructs a Property with an initial value, and a min and max
        range that will be used when setting future values.
     */
    Property(const PropertyType &value, PropertyType min, PropertyType max)
    : BaseType(value, [=](PropertyType value){ return jlimit(min, max, value); })
    {}

    /** Constructs a Property with the same value and filtering function.
        Does not copy the listeners.
     */
    Property(const SelfType &other)
    : BaseType(other)
    {}

    /** Move constructor
     */
    Property(SelfType &&other)
    : BaseType(other)
    {}
    
    //==============================================================================
    using BaseType::Set;
    
    SelfType &operator=(const SelfType     &rhs)   { Set(rhs.value); return *this; }
    SelfType &operator=(const PropertyType &value) { Set(value);     return *this; }
};

/// A Property whose value represents a selection.
/// A selection is a value which is one of an available number of items that can be
/// selected. Add the selectable items with AddSelectionItem. Only items that are
/// added to selection can be selected.
template <class Type>
class SelectionProperty
: public ConstProperty<SelectionProperty<Type>, Type>
{
    using BaseType = ConstProperty<SelectionProperty, Type>;
public:
    SelectionProperty(  Type initialSelection = Type()
                      , std::map<Type, std::string> options = {})
    : BaseType (initialSelection)
    , options  (options)
    {
        
    }
        
    /** Return the current selectable options.
     */
    std::map<Type, std::string> GetOptions() const
    {
        return options;
    }
    
    /** Return the number of options.
     */
    size_t NumOptions() const
    {
        return options.size();
    }
    
    /** Return whether this Property contains an item
     */
    bool ContainsItem(Type value)
    {
        return options.find(value) != options.end();
    }
    
    /** Set the selection by key.
     */
    void SetSelectionItem(Type value)
    {
        if (auto it = options.find(value); it != options.end())
        {
            BaseType::Set(it->first);
        }
    }
    
    /** Set the selection by index rather than key.
     */
    void SetSelectionIndex(size_t index)
    {
        if (index < options.size())
        {
            auto it = options.begin();
            std::advance(it, index);
            
            BaseType::Set(it->first);
        }
    }
    
    /** Return the index of the current selection.
     */
    size_t GetSelectionIndex() const
    {
        auto it = options.find(BaseType::Get());
        return std::distance(options.begin(), it);
    }
    
    /** Get the string of the current selection.
     */
    std::string GetSelectionString() const
    {
        return options.at(BaseType::Get());
    }
    
    /** Get all the option strings.
     */
    juce::StringArray GetStringArray() const
    {
        juce::StringArray array;
        
        for (auto &[key, value] : options)
        {
            array.add(value);
        }
        
        return array;
    }
    
    /** Get all the option keys.
     */
    juce::Array<var> GetKeyArray() const
    {
        Array<var> keyArray;
        
        for (auto &[key, value] : options)
        {
            keyArray.add(int(key));
        }
        
        return keyArray;
    }
private:
    // ConstProperty
    void HowToSet(Type newValue) override
    {
        if (ContainsItem(newValue))
        {
            BaseType::Set(newValue);
        }
    }
    
    std::map<Type, std::string> options;
};
