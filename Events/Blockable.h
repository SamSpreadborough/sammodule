/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/** A base class for classes that can be blocked
 */
class Blockable
    : public Trackable
{
public:
    using Ptr = std::weak_ptr<Blockable>;

    //==============================================================================
    /** Creates a Blockable object. Default blocked state is false.
     */
    Blockable()
    : isBlocked(false)
    {}

    /** Sets the blocking status

        @param blocked blocking status
     */
    void SetBlocked(bool blocked) { isBlocked = blocked; }
    
    /** @return the blocking status
     */
    bool IsBlocked() const { return isBlocked; }
    
    operator bool() { return !isBlocked; }
private:
    //==============================================================================
    bool isBlocked;
};
