/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */


//==============================================================================
/**
 An RAII object for blocking Connections in a scoped fashion.
 Keep a copy of the BlockableConnection returned by ConstSignal or Notifier
 and use it as the argument for this blocker.
 */
class ConnectionBlocker
{
public:
    //==============================================================================
    /** Creates a ConnectionBlocker. Sets the connection blocked state to true
     */
    ConnectionBlocker(Blockable::Ptr blockable)
        : blockable(blockable)
    {        
        if (!blockable.expired())
            blockable.lock()->SetBlocked(true);
    }

    /** Destructor.
     */
    ~ConnectionBlocker()
    {
        if (!blockable.expired())
            blockable.lock()->SetBlocked(false);
    }

private:
    //==============================================================================
    Blockable::Ptr blockable;
};
