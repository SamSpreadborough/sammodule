/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

//==============================================================================
/** An object that can be listened to, and notifies listeners with updates.

    ConstSignal presents a publically listenable interface, but only the
    'Setter' class can emit signals. Signals emitted with be the type
    templated with the SignalType type. SignalType is a variadic type, so
    the variable type could be a single type or multiple types.

    For a publically settable ConstSignal, see Signal.

    @code
    ConstSignal<SetterClass, int>                 singleArgSignal;
    ConstSignal<SetterClass, int, double, String> multipleArgSignal;
    @endcode

    Listeners can add themselves to a ConstSignal via AddListener.
    If you pass a Trackable* as an argument, then the listener will be
    automatically removed when the Trackable is deleted. Typically this would
    look like:

    @code
    class MyClass
        : public Trackable
    {
    public:
        MyClass()
        {
            signal.AddListener(this, [=](String value)
            {
                // Do Thing
            }})
        }

        ConstSignal<SetterClass, String> signal;
    };
    @endcode

    Create a ConstSignal when you want to have multiple objects react to changes.
    To notify a signal, call Notify() with the value type of the Signal.

    @code
    ConstSignal<SetterClass, int> signal;
    @endcode

    @code
    class MyClass
    {
    public:
        ConstSignal<MyClass, void> signal;

        void NotifySignal()
        {
            signal.Notify();
        }
    };

    class MyListenerClass
        : public Trackable
    {
    public:
        MyListenerClass(MyClass &object)
        {
            object.signal.AddListener(this, [=]
            {
                // Do thing
            });

            object.NotifySignal();
        }
    };
    @endcode

    You can optionally add a listener with a function signature with the same
    arguments as the SignalType, which will mean that you get the value emitted
    as well as the event, or you can add a callback function with no arguments,
    and just get notified about the event.

    @code
    ConstSignal<SetterClass, unsigned> signal;

    signal.AddListener(this, [=](unsigned value)
    {
        // Do something with the emitted value
    });

    signal.AddListener(this, [=]
    {
        // Do something with the emitted event
    });
    @endcode

    AddListener returns a Blockable::Ptr object which can used to
    temporarily block a callback. You can manually set the blocked state in
    Blockable::Ptr, but prefer to use a ConnectionBlocker object as the
    blocked state is then tied to the lifetime of the ConnectionBlocker object.

    Listeners can be removed by either passing it the Trackable* used when
    adding the listener, or passing it the Blockable::Ptr object.

    @see Blockable, ConnectionBlocker, Signal, Trackable, Tracker
 */
template <class Setter
        , class ... SignalType>
class ConstSignal
    : private Tracker
    , public  Trackable
{
    friend Setter;
protected:
    using Callback0          = std::function<void()>;
    using Callback           = std::function<void(SignalType ...)>;
    using CallbackVariant    = std::variant<Callback0
                                          , Callback>;

    using Connection         = std::tuple< Trackable*
                                         , CallbackVariant
                                         , std::shared_ptr<Blockable>>;
    using ConnectionIterator = typename std::list<Connection>::iterator;

    using SelfType           = ConstSignal<Setter, SignalType ...>;
public:
    
    //==============================================================================
    /** Add a function to be called when the Signal is emited. This can take an
        optional SignalListener for use as the Trackable.
     
        @param callbackVariant The functor to be called
        @param listener optional listener for use with Trackable
        @return The blockable connection object for use with ConnectionBlocker
     */
    Blockable::Ptr AddListener(  Trackable        *listener
                               , CallbackVariant &&callbackVariant)
    {
        if (listener)
            listener->AddTracker(this);
        
        auto connection = connections.emplace_back(std::make_tuple(  listener
                                                                   , callbackVariant
                                                                   , new Blockable));
        
        return Blockable::Ptr(GetBlockable(connection));
    }
    
    template <class ObjectClass, class ... Args>
    /** Add a function to be called when the Signal is emited
     
        @code
        Signal<int> mySignal;
     
        mySignal.AddListener(this, &MyCallbackClass::CallbackMethod, optionalArgs);
        @endcode
     
        @param listener The Trackable listener to add. This should be a pointer to the
        object whose member function you'd like to call.
        @param call The member function to call
        @return The blockable for this connection.
     */
    Blockable::Ptr AddListener(  Trackable  *listener
                               , void (ObjectClass::*call)(SignalType ... value, Args ... args)
                               , Args ... args)
    {
        return AddListener(listener, [=](SignalType ... value)
                           {
                               ((ObjectClass*)listener->*call)(value ..., args ...);
                           });
    }
    
    template <class ObjectClass, class ... Args>
    /** Add a function to be called when the Signal is emited
     
        @code
        Signal<int> mySignal;
        
        mySignal.AddListener(this, &MyCallbackClass::CallbackMethod, optionalArgs);
        @endcode
        
        @param listener The Trackable listener to add. This should be a pointer to the
        object whose member function you'd like to call.
        @param call The member function to call
        @return The blockable for this connection.
     */
    Blockable::Ptr AddListener(  Trackable  *listener
                               , void (ObjectClass::*call)(Args ... args)
                               , Args ... args)
    {
        return AddListener(listener, [=]
        {
            ((ObjectClass*)listener->*call)(args ...);
        });
    }
    
    /** Add a Signal as a listener. When this Signal changes, it will notify the
        Signal.

        @param other The Signal that will listen
        @return The blockable connection object for use with ConnectionBlocker
     */
    Blockable::Ptr AddListener(SelfType &other)
    {
        return AddListener(&other, [=, &other](SignalType ... value)
        {
            other.HowToSet(value ...);
        });
    }
    
    //==============================================================================
    /** Removes a listener from the Signal so that it will not be called

        @param listener A Trackable pointer or Blockable::Ptr reference to be removed
     */
    template <class TrackingObject>
    void RemoveListener(const TrackingObject object)
    {
        if (auto it = FindConnection(object); it != connections.end())
            connections.erase(it);
    }

    /** Removes all attached listeners to this ConstSignal
     */
    void RemoveAllListeners() { connections.clear(); }

    //==============================================================================
    /** Perform a check to see if a given SignalListener is attached to this
        ConstSignal
     
        @param listener The listener to check
        @return true if is attached, otherwise false
     */
    template <class TrackingObject>
    bool IsListening(TrackingObject listener)
    {
        return FindConnection(listener) != connections.end();
    }
    
    /** Returns the number of listening connections on this ConstSignal.
     
        @return This number of listening connections
     */
    size_t Size() const { return connections.size(); }

protected:
    //==============================================================================
    /** Used by this and derived classes to define behaviour when we add
        a listener from a SelfType signal.

        @param value The value to set.
     */
    virtual void HowToSet(SignalType ... value)
    {
        Notify(value ...);
    }
    
    /** Will call all listeners with the value that is passed.
     
        @param value The value that will be passed to all listeners
     */
    void Notify(SignalType ... value)
    {
        for (auto &[listener, callback, blockable] : connections)
        {
            if (!blockable->IsBlocked())
                Call(callback, value ...);
        }
    }

    /** Handles the logic needed for calling the correct callback variant

        @param callback The callback variant to call
        @param value The value to call with (if the callback variant type supports it)
     */
    void Call(const CallbackVariant &callback, SignalType ... value)
    {
        if (auto result = std::get_if<Callback0>(&callback))
        {
            (*result)();
        }
        else if (auto result = std::get_if<Callback>(&callback))
        {
            (*result)(value ... );
        }
    }

private:
    //==============================================================================
    ConnectionIterator FindConnection(const Trackable *listener)
    {
        return listener
        ? std::find_if(  connections.begin()
                       , connections.end()
                       , [=, &listener](auto &connection)
                       {
                           return GetTrackable(connection) == listener;
                       })
        : connections.end();
    }
    
    ConnectionIterator FindConnection(const Blockable::Ptr &blockable)
    {
        return !blockable.expired()
        ? std::find_if(  connections.begin()
                       , connections.end()
                       , [=, &blockable](auto &connection)
                       {
                           return GetBlockable(connection).get() == blockable.lock().get();
                       })
        : connections.end();
    }
    
    // Tracker
    void TrackableWillBeDeleted(Trackable *trackable) override
    {
        if (auto it = FindConnection(trackable); it != connections.end())
            connections.erase(it);
    }

    Trackable                  *GetTrackable(Connection &connection) { return std::get<0>(connection); }
    CallbackVariant            &GetCallback (Connection &connection) { return std::get<1>(connection); }
    std::shared_ptr<Blockable> &GetBlockable(Connection &connection) { return std::get<2>(connection); }

    std::list<Connection> connections;
};

//==============================================================================
/** A publically emittable version of ConstSignal.

    Does not require that you provide a Setter class.
 */
template <class ... SignalType>
class Signal
    : public ConstSignal<void, SignalType...>
{
    using BaseType = ConstSignal<void, SignalType...>;
public:
    //==============================================================================
    using BaseType::Notify;
};
