/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */


//==============================================================================
/** Chains A->B. This will update A to have the same value as B.
 */
template <class Type>
inline void Chain(Property<Type>& A, Property<Type>& B)
{
    A.AddListenerAndCall(&B, [=, &B](Type value) { B = value; });
    B.AddListener       (&A, [=, &A](Type value) { A = value; });
}

/** Remove listeners to properties
 */
template <class TypeA
        , class TypeB>
inline void Unchain(Property<TypeA>& A, Property<TypeB>& B)
{
    A.RemoveListener(&B);
    B.RemoveListener(&A);
}

//==============================================================================
/** Chain two Properties together using a converter functor.
    If no custom functor is provided, it will default to static_cast.
 */
template <class TypeA
        , class TypeB>
inline void ChainWithConversion(  Property<TypeA> &A
                                , Property<TypeB> &B
                                , std::function<TypeB(TypeA)> convertAtoB = nullptr
                                , std::function<TypeA(TypeB)> convertBtoA = nullptr)
{
    auto callbackA = [=, &B](TypeA a) { B = convertAtoB ? convertAtoB(a) : static_cast<TypeB>(a); };
    auto callbackB = [=, &A](TypeB b) { A = convertBtoA ? convertBtoA(b) : static_cast<TypeA>(b); };
    
    A.AddListenerAndCall(&B, callbackA);
    B.AddListener       (&A, callbackB);
}

