# README #

SamModule

### (Optional) Dependencies ###

Some classes require external dependencies. Install the dependencies and enable them in the module preferences in Projucer

Install the dependencies via Homebrew:

install Homebrew (https://brew.sh)
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
install Taglib (https://taglib.org) (Enables 'AudioFile')
```
brew install taglib
```
install fftw3 (http://www.fftw.org) (Enables 'AudioAnalysisJob')
```
brew install fftw
```
