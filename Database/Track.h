/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

using track_t = unsigned;

struct Track
{
    using Ptr = std::shared_ptr<Track>;
    using Vec = std::vector<Track>;
    
    track_t     id = 0;
    std::string filePath;
    std::string dateAdded;
    std::string title;
    std::string artist;
    std::string album;
    std::string albumArtist;
    std::string genre;
    std::string comment;
    std::string composer;
    std::string remixer;
    std::string label;
    unsigned    trackNumber   = 0;
    unsigned    year          = 0;
    unsigned    bitRate       = 0;
    unsigned    numChannels   = 0;
    unsigned    key           = 0;
    unsigned    sampleRate    = 0;
    double      lengthSeconds = 0.0;
    double      beatGridStart = 0.0;
    double      bpm           = 0.0;
    double      gain          = 0.0;
    
    std::vector<char> smallArtwork;
    std::vector<char> largeArtwork;
};

inline bool operator ==(const Track &lhs, const Track &rhs)
{
    return lhs.id            == rhs.id
        && lhs.filePath      == rhs.filePath
        && lhs.dateAdded     == lhs.dateAdded
        && lhs.title         == lhs.title
        && lhs.artist        == lhs.artist
        && lhs.album         == lhs.album
        && lhs.albumArtist   == lhs.albumArtist
        && lhs.genre         == lhs.genre
        && lhs.comment       == lhs.comment
        && lhs.composer      == lhs.composer
        && lhs.remixer       == lhs.remixer
        && lhs.label         == lhs.label
        && lhs.trackNumber   == lhs.trackNumber
        && lhs.year          == lhs.year
        && lhs.bitRate       == lhs.bitRate
        && lhs.numChannels   == lhs.numChannels
        && lhs.key           == lhs.key
        && lhs.sampleRate    == lhs.sampleRate
        && lhs.lengthSeconds == lhs.lengthSeconds
        && lhs.beatGridStart == lhs.beatGridStart
        && lhs.bpm           == lhs.bpm
        && lhs.gain          == lhs.gain
        && lhs.smallArtwork  == lhs.smallArtwork
        && lhs.largeArtwork  == lhs.largeArtwork;
}

inline bool operator !=(const Track &lhs, const Track &rhs)
{
    return !(lhs == rhs);
}
