/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

TrackConnector::TrackConnector(TrackablePointer<Database> db)
: db      (db)
, trackId (0)
{
    db->TrackUpdated.AddListener(this, [=]{ this->FullPropertyRefresh(); });
    db->TrackRemoved.AddListener(this, [=]{ this->FullPropertyRefresh(); });
    
    AddListener(filePath);
    AddListener(dateAdded);
    AddListener(title);
    AddListener(artist);
    AddListener(album);
    AddListener(albumArtist);
    AddListener(genre);
    AddListener(comment);
    AddListener(composer);
    AddListener(remixer);
    AddListener(label);
    AddListener(trackNumber);
    AddListener(year);
    AddListener(bitRate);
    AddListener(numChannels);
    AddListener(key);
    AddListener(sampleRate);
    AddListener(lengthSeconds);
    AddListener(beatGridStart);
    AddListener(bpm);
    AddListener(gain);
    AddListener(smallArtwork);
    AddListener(largeArtwork);
}

Track TrackConnector::ToTrack() const
{
    Track track;
    
    track.id              = trackId;
    track.filePath        = filePath;
    track.dateAdded       = dateAdded;
    track.title           = title;
    track.artist          = artist;
    track.album           = album;
    track.albumArtist     = albumArtist;
    track.genre           = genre;
    track.comment         = comment;
    track.composer        = composer;
    track.remixer         = remixer;
    track.label           = label;
    track.trackNumber     = trackNumber;
    track.year            = year;
    track.bitRate         = bitRate;
    track.numChannels     = numChannels;
    track.key             = key;
    track.sampleRate      = sampleRate;
    track.lengthSeconds   = lengthSeconds;
    track.beatGridStart   = beatGridStart;
    track.bpm             = bpm;
    track.gain            = gain;
    track.smallArtwork    = smallArtwork;
    track.largeArtwork    = largeArtwork;
    
    return track;
}

void TrackConnector::ReferTo(track_t newTrack)
{
    auto blockers = GetConnectionBlockers();
    
    trackId = newTrack;
    FullPropertyRefresh();
}

track_t TrackConnector::GetTrackId() const
{
    return trackId;
}

bool TrackConnector::IsConnected() const
{
    return trackId != 0 && db;
}

void TrackConnector::OnTrackUpdated(track_t updatedId)
{
    FullPropertyRefresh();
}

void TrackConnector::OnTrackRemoved(track_t removedId)
{
    FullPropertyRefresh();
}

template <class Type>
void TrackConnector::AddListener(Property<Type> &property)
{
    auto blockable = property.AddListener(this, [=]
    {
        Track track = ToTrack();
        db->UpdateTrack(track);
    });
    
    blockables.emplace_back(blockable);
}

void TrackConnector::FullPropertyRefresh()
{
    auto blockers = GetConnectionBlockers();
    
    Track::Ptr track = db->GetTrack(trackId);
    
    filePath        = track ? track->filePath       : "";
    dateAdded       = track ? track->dateAdded      : "";
    title           = track ? track->title          : "";
    artist          = track ? track->artist         : "";
    album           = track ? track->album          : "";
    albumArtist     = track ? track->albumArtist    : "";
    genre           = track ? track->genre          : "";
    comment         = track ? track->comment        : "";
    composer        = track ? track->composer       : "";
    remixer         = track ? track->remixer        : "";
    label           = track ? track->label          : "";
    trackNumber     = track ? track->trackNumber    : 0;
    year            = track ? track->year           : 0;
    bitRate         = track ? track->bitRate        : 0;
    numChannels     = track ? track->numChannels    : 0;
    key             = track ? track->key            : 0;
    sampleRate      = track ? track->sampleRate     : 0;
    lengthSeconds   = track ? track->lengthSeconds  : 0.0;
    beatGridStart   = track ? track->beatGridStart  : 0.0;
    bpm             = track ? track->bpm            : 0.0;
    gain            = track ? track->gain           : 0.0;
    smallArtwork    = track ? track->smallArtwork   : std::vector<char>();
    largeArtwork    = track ? track->largeArtwork   : std::vector<char>();
}

std::vector<ConnectionBlocker> TrackConnector::GetConnectionBlockers() const
{
    std::vector<ConnectionBlocker> blockers;
    
    for (auto &b : blockables)
    {
        blockers.emplace_back(b);
    }
    
    return blockers;
}
