/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace
{
    const String hotCueIdentifier   = "hotcue";
    const String loopIdentifier     = "loop";
    const String enabledIdentifier  = "enabled";
    const String positionIdentifier = "position";
    const String startIdentifier    = "start";
    const String endIdentifier      = "end";
    const String colourIdentifier   = "colour";
    const String nameIdentifier     = "name";
    
    ValueTree DataToTree(const std::vector<char> &data)
    {
        return ValueTree::readFromData(data.data(), data.size());
    }
    
    std::vector<char> TreeToData(ValueTree tree)
    {
        MemoryBlock block;
        MemoryOutputStream stream(block, false);
        
        tree.writeToStream(stream);
        
        std::vector<char> data;
        data.reserve(block.getSize());
        
        memcpy(data.data(), block.getData(), block.getSize());
        
        return data;
    }
}

HotCues::HotCues()
: tree (hotCueIdentifier)
{
    Init();
}

HotCues::HotCues(std::vector<char> data)
: tree (DataToTree(data))
{
    jassert(tree.hasType(hotCueIdentifier));
    
    Init();
}

std::vector<char> HotCues::GetData() const
{
    return TreeToData(tree);
}

void HotCues::Init()
{
    jassert(tree.getNumChildren() == 8);
    
    for (int i = 0; i < tree.getNumChildren(); ++i)
    {
        auto child = tree.getChild(i);
        
        enabled  [i].referTo(child, enabledIdentifier , nullptr);
        position [i].referTo(child, positionIdentifier, nullptr);
        colour   [i].referTo(child, colourIdentifier  , nullptr);
        name     [i].referTo(child, nameIdentifier    , nullptr);
    }
}

Loops::Loops()
: tree (loopIdentifier)
{
    Init();
}

Loops::Loops(std::vector<char> data)
: tree (DataToTree(data))
{
    jassert(tree.hasType(loopIdentifier));
    
    Init();
}

std::vector<char> Loops::GetData() const
{
    return TreeToData(tree);
}

void Loops::Init()
{
    jassert(tree.getNumChildren() == 8);
    
    for (int i = 0; i < tree.getNumChildren(); ++i)
    {
        auto child = tree.getChild(i);
        
        enabled [i].referTo(child, enabledIdentifier, nullptr);
        start   [i].referTo(child, startIdentifier  , nullptr);
        end     [i].referTo(child, endIdentifier    , nullptr);
        colour  [i].referTo(child, colourIdentifier , nullptr);
        name    [i].referTo(child, nameIdentifier   , nullptr);
    }
}
