/* Copyright (C) Sam Spreadborough - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
*/

class TrackConnector
: private Trackable
{
public:
    TrackConnector(TrackablePointer<Database> db);
    
    Track ToTrack() const;
    
    track_t GetTrackId() const;
    
    Property<std::string>       filePath;
    Property<std::string>       dateAdded;
    Property<std::string>       title;
    Property<std::string>       artist;
    Property<std::string>       album;
    Property<std::string>       albumArtist;
    Property<std::string>       genre;
    Property<std::string>       comment;
    Property<std::string>       composer;
    Property<std::string>       remixer;
    Property<std::string>       label;
    Property<unsigned>          trackNumber;
    Property<unsigned>          year;
    Property<unsigned>          bitRate;
    Property<unsigned>          numChannels;
    Property<unsigned>          key;
    Property<unsigned>          sampleRate;
    Property<double>            lengthSeconds;
    Property<double>            beatGridStart;
    Property<double>            bpm;
    Property<double>            gain;
    Property<std::vector<char>> smallArtwork;
    Property<std::vector<char>> largeArtwork;

    void ReferTo(track_t newTrack);
    
    bool IsConnected() const;
private:
    void OnTrackUpdated(track_t updatedId);
    void OnTrackRemoved(track_t removedId);
    
    template <class Type>
    void AddListener(Property<Type> &property);
    
    void FullPropertyRefresh();
    
    std::vector<ConnectionBlocker> GetConnectionBlockers() const;
    
    TrackablePointer<Database> db;
    
    std::vector<Blockable::Ptr> blockables;
    
    track_t trackId;
};
