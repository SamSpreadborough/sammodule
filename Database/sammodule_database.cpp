/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#if USE_SQLITE_ORM
#include "../Database/PerformanceData.cpp"
#include "../Database/Playlist.cpp"
#include "../Database/Database.cpp"
#include "../Database/TrackConnector.cpp"
#endif
