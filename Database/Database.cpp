/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#define DB_CHECK(default) if (!IsOpen()) return default

namespace
{
    const Identifier rootIdentifier("root");
}

using namespace sqlite_orm;

template <typename Function>
static void applyFunctionRecursively (const ValueTree& tree, const Function& function)
{
    function (tree);
    
    for (auto&& child : tree)
    {
        applyFunctionRecursively (child, function);
    }
}

Database::Database()
{

}

Database::~Database()
{
    Close();
}

File Database::CreateNewDatabase(File directory)
{
    if (directory.isDirectory())
    {
        const String fileName = String("database") + String(SQLITE_EXT);
        File childFile = directory.getChildFile(fileName);

        if (childFile.existsAsFile())
            childFile.deleteFile();

        if (childFile.create())
        {
            SQLiteStorage storage = InitDatabaseStorage(childFile.getFullPathName().toStdString());
            storage.sync_schema();

            ValueTree root(rootIdentifier);
            MemoryBlock treeBlock;
            MemoryOutputStream stream(treeBlock, false);
            root.writeToStream(stream);

            DatabaseInfo info;
            info.dateCreated = Time::getCurrentTime().toISO8601(true).toStdString();
            info.playlistData.resize(treeBlock.getSize());
            std::memcpy(info.playlistData.data(), treeBlock.getData(), treeBlock.getSize());

            storage.insert<DatabaseInfo>(info);

            return childFile;
        }
    }

    return File();
}

bool Database::CanOpen(File file)
{
    return file.existsAsFile() && file.hasFileExtension(SQLITE_EXT);
}

bool Database::Open(File file)
{
    Close();

    if (CanOpen(file))
    {
        storage.reset(new SQLiteStorage(InitDatabaseStorage(file.getFullPathName().toStdString())));

        DatabaseOpened.Notify(this);
        
        auto info = storage->get_no_throw<DatabaseInfo>(1);
        
        root = ValueTree::readFromData(info->playlistData.data(), info->playlistData.size());
        
        return IsOpen();
    }

    return false;
}

void Database::Close()
{
    if (IsOpen())
    {
        try
        {
            MemoryBlock treeBlock;
            MemoryOutputStream stream(treeBlock, false);
            root.writeToStream(stream);
            
            auto info = storage->get_no_throw<DatabaseInfo>(1);
            jassert(info);
            info->playlistData.clear();
            
            info->playlistData.resize(treeBlock.getSize());
            std::memcpy(info->playlistData.data(), treeBlock.getData(), treeBlock.getSize());
            
            storage->update(*info);
        }
        catch (const std::exception &e)
        {
            DBG(e.what());
            jassertfalse;
        }
    }

    DatabaseWillClose.Notify(this);
    storage.reset();
    DatabaseClosed.Notify(this);
}

Time Database::GetDateCreated() const
{
    DB_CHECK(Time());
    
    auto info = storage->get_no_throw<DatabaseInfo>(1);
    
    return Time::fromISO8601(info->dateCreated);
}

bool Database::IsOpen() const
{
    return storage.get() != nullptr && root.isValid();
}

size_t Database::CountTracks() const
{
    DB_CHECK(0);

    return storage->count<Track>();
}

track_t Database::InsertTrack(Track &track)
{
    DB_CHECK(0);

    auto id = storage->insert(track);
    track.id = id;

    PerformanceData perfData;
    perfData.trackId.reset(new track_t (id));
    
    storage->insert(perfData);
    
    TrackAdded.Notify(this, id);
    
    return id;
}

bool Database::ContainsTrack(track_t trackId) const
{
    DB_CHECK(false);

    return GetTrack(trackId) != nullptr;
}

Track::Ptr Database::GetTrack(track_t trackId) const
{
    DB_CHECK(nullptr);

    return storage->get_no_throw<Track>(trackId);
}

Track::Vec Database::GetAllTracks() const
{
    DB_CHECK(Track::Vec());

    return storage->get_all<Track>();
}

void Database::UpdateTrack(Track &track)
{
    DB_CHECK();

    storage->update(track);
    
    TrackUpdated.Notify(this, track.id);
}

void Database::RemoveTrack(Track &track)
{
    DB_CHECK();

    RemoveTrack(track.id);
}

void Database::RemoveTrack(track_t trackId)
{
    DB_CHECK();

    storage->remove<Track>(trackId);
    
    TrackRemoved.Notify(this, trackId);
}

PerformanceData::Ptr Database::GetPerformanceData() const
{
    DB_CHECK(nullptr);
    
    return nullptr;
}

void Database::UpdatePerformanceData(const PerformanceData &data)
{
    DB_CHECK();
    
    storage->update(data);
}

Playlist Database::CreatePlaylist()
{
    DB_CHECK(Playlist());
    
    try
    {
        Playlist playlist(this);
        
        playlist.GetTree().setProperty("uid", Uuid().toString(), nullptr);
        root.appendChild(playlist.GetTree(), nullptr);
        
        jassert(playlist);
        
        PlaylistAdded.Notify(this, playlist.GetUid());
        
        return playlist;
    }
    catch (const std::exception &e)
    {
        DBG(e.what());
        jassertfalse;
        return Playlist();
    }
}

std::vector<Playlist> Database::GetRootPlaylists()
{
    std::vector<Playlist> playlists;
    
    for (auto vt : root)
    {
        Playlist pl(vt, this);
        playlists.push_back(pl);
    }
    
    return playlists;
}

std::vector<Playlist> Database::GetAllPlaylists()
{
    std::vector<Playlist> vec;
    
    applyFunctionRecursively (root, [=, &vec] (const ValueTree& v)
    {
        if (v.hasType("playlist"))
        {
            Playlist playlist(v, this);
            vec.push_back(playlist);
        }
    });
    
    return vec;
}

Playlist Database::GetPlaylist(Uuid playlistUid)
{
    ValueTree result;
    
    applyFunctionRecursively (root, [=, &result] (const ValueTree& v)
    {
        if (v.getProperty("uid").toString() == playlistUid.toString())
        {
            result = v;
        }
    });
    
    return Playlist(result, this);
}

void Database::DeletePlaylist(const Playlist &playlist)
{
    DB_CHECK();
    
    Uuid uid = playlist.GetUid();
    
    root.removeChild(playlist.GetTree(), nullptr);
    
    PlaylistRemoved.Notify(this, uid);
}

void Database::DumpIntoConsole()
{
    DBG(root.toXmlString());
    
    if (IsOpen())
    {
        for(auto &t : storage->iterate<Track>())
        {
            DBG(storage->dump(t));
        }
    }
}

#undef DB_CHECK
