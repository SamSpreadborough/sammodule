/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class Database;

class Playlist
: private Trackable
, private Tracker
{
public:
    Playlist();
    Playlist(Database *db);
    Playlist(ValueTree tree, Database *db);
    Playlist(const Playlist &other);
    
    struct Item
    {
    private:
        ValueTree tree;
    public:
        
        Item();
        Item(ValueTree tree);
        
        track_t   trackId;
        Time      dateAdded;
        
        const ValueTree &GetTree() const;
    };
    
    Uuid GetUid() const;
    
    String GetName() const;
    void SetName(String newName);
    
    void InsertTrack(track_t trackId);
    void InsertPlaylist(const Playlist &playlist);
    
    void RemoveTrack(const Item item);
    void RemovePlaylist(Playlist playlist);

    size_t NumTracks() const;
    size_t NumPlaylists() const;
    
    Item     GetTrack(size_t index) const;
    std::vector<Item> GetAllTracks() const;
    Playlist GetPlaylist(size_t index) const;
    
    Playlist GetParentPlaylist() const;
    bool ParentIsRoot() const;
    
    operator bool() const;
    bool IsValid() const;

    ValueTree GetTree() const;
private:
    
    // Tracker
    void TrackableWillBeDeleted(Trackable *trackable) override;
    
    void Close();
    
    ValueTree tree;
    ValueTree playlistTree;
    ValueTree trackTree;
    
    CachedValue<String> name;

    TrackablePointer<Database> db;
};

inline bool operator ==(const Playlist &lhs, const Playlist &rhs)
{
    return lhs.GetTree() == rhs.GetTree();
}
