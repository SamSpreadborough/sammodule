/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#define SQLITE_EXT ".db"

class Database
: public Trackable
{
public:
     Database();
    ~Database();
    
    ConstSignal<Database, Database*>          DatabaseOpened;
    ConstSignal<Database, Database*>          DatabaseWillClose;
    ConstSignal<Database, Database*>          DatabaseClosed;
    ConstSignal<Database, Database*, track_t> TrackAdded;
    ConstSignal<Database, Database*, track_t> TrackUpdated;
    ConstSignal<Database, Database*, track_t> TrackRemoved;
    ConstSignal<Database, Database*, Uuid>    PlaylistAdded;
    ConstSignal<Database, Database*, Uuid>    PlaylistRemoved;
    
    static File CreateNewDatabase(File directory);
    static bool CanOpen(File file);
    bool Open(File file);
    bool IsOpen() const;
    void Close();

    Time       GetDateCreated() const;
    
    size_t     CountTracks() const;
    track_t    InsertTrack(Track &track);
    bool       ContainsTrack(track_t trackId) const;
    Track::Ptr GetTrack(track_t trackId) const;
    Track::Vec GetAllTracks() const;
    void       UpdateTrack(Track &track);
    void       RemoveTrack(Track &track);
    void       RemoveTrack(track_t trackId);
    
    PerformanceData::Ptr GetPerformanceData() const;
    void UpdatePerformanceData(const PerformanceData &data);
    
    Playlist   CreatePlaylist();
    std::vector<Playlist> GetRootPlaylists();
    std::vector<Playlist> GetAllPlaylists();
    Playlist   GetPlaylist(Uuid playlistUid);
    void       DeletePlaylist(const Playlist &playlist);
    
    template <class SortBy>
    void GetSorted(Track::Vec &vec, SortBy sortBy, bool sortForward);
    
    template <class SortBy>
    void GetSorted(std::vector<Playlist::Item> &vec, SortBy sortBy, bool sortForward);
    
    template <class SortBy>
    void GetSorted(std::vector<track_t> &vec, SortBy sortBy, bool sortForward);
    
    template <class SortBy>
    Track::Vec Search(const String &term, SortBy sortBy, bool sortForward);
    
    void DumpIntoConsole();
private:
    std::unique_ptr<SQLiteStorage> storage;
    ValueTree root;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Database)
};

template <class SortBy>
Track::Vec Database::Search(const String &term, SortBy sortBy, bool sortForward)
{
    using namespace sqlite_orm;
    
    if (!IsOpen())
        return Track::Vec();
    
    const std::string searchClause = "%" + term.toStdString() + "%";
    
    return storage->get_all<Track>(where(   like(&Track::title      , searchClause)
                                         or like(&Track::artist     , searchClause)
                                         or like(&Track::album      , searchClause)
                                         or like(&Track::albumArtist, searchClause)
                                         or like(&Track::composer   , searchClause)
                                         or like(&Track::label      , searchClause)
                                         )
                                   , sortForward ? order_by(sortBy).asc()
                                                 : order_by(sortBy).desc()
                                   );
}

template <class SortBy>
void Database::GetSorted(Track::Vec &vec, SortBy sortBy, bool sortForward)
{
    using namespace sqlite_orm;
    
    std::vector<track_t> trackIds;
    for (auto &it : vec)
    {
        trackIds.push_back(it.id);
    }
    
    auto sorted = storage->get_all<Track>(where(in(&Track::id, trackIds))
                                          , sortForward ? order_by(sortBy).asc()
                                                        : order_by(sortBy).desc());
    
    vec = sorted;
}

template <class SortBy>
void Database::GetSorted(std::vector<Playlist::Item> &vec, SortBy sortBy, bool sortForward)
{
    using namespace sqlite_orm;
    
    std::vector<track_t> trackIds;
    for (auto &it : vec)
    {
        trackIds.push_back(it.trackId);
    }
    
    GetSorted(trackIds, sortBy, sortForward);
    
    std::vector<Playlist::Item> newVec;
    
    for (auto &id : trackIds)
    {
        auto it = std::find_if(vec.begin(), vec.end()
                               , [=](const Playlist::Item &item)
        {
            return item.trackId == id;
        });
        
        jassert(it != vec.end());
        
        Playlist::Item item = *it;
        
        newVec.emplace_back(item);
        vec.erase(it);
    }
    
    vec = newVec;
}

template <class SortBy>
void Database::GetSorted(std::vector<track_t> &vec, SortBy sortBy, bool sortForward)
{
    using namespace sqlite_orm;
    
    auto sorted = storage->select(columns(&Track::id)
                                  , sortForward ? order_by(sortBy).asc()
                                                : order_by(sortBy).desc());
    
    vec.clear();
    
    for (size_t i = 0; i < sorted.size(); ++i)
    {
        vec.push_back(std::get<0>(sorted[i]));
    }
}

template <>
struct VariantConverter<unsigned>
{
    static unsigned fromVar (const var& v)      { return unsigned(v.operator int()); }
    static var      toVar   (const unsigned& s) { return int(s); }
};
