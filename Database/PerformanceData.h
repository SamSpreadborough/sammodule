/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

using perf_data_t = int;

class HotCues
{
public:
    HotCues();
    HotCues(std::vector<char> data);
    
    CachedValue<bool>     enabled [8];
    CachedValue<second_t> position[8];
    CachedValue<unsigned> colour  [8];
    CachedValue<String>   name    [8];
    
    std::vector<char> GetData() const;
private:
    void Init();
    
    ValueTree tree;
};

class Loops
{
public:
    Loops();
    Loops(std::vector<char> data);
    
    CachedValue<bool>     enabled [8];
    CachedValue<second_t> start   [8];
    CachedValue<second_t> end     [8];
    CachedValue<unsigned> colour  [8];
    CachedValue<String>   name    [8];
    
    std::vector<char> GetData() const;
private:
    void Init();
    
    ValueTree tree;
};

struct PerformanceData
{
    using Ptr = std::shared_ptr<PerformanceData>;
    
    perf_data_t id = 0;
    std::shared_ptr<track_t> trackId;
    
    second_t cuePosition;
    second_t beatGridStart;
    
    std::vector<char> loopData;
    std::vector<char> hotCueData;
};
