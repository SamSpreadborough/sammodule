/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

inline
auto InitDatabaseStorage(const std::string &path)
{
    using namespace sqlite_orm;
    
    return make_storage(path
                        , make_table("Info"
                                     , make_column("Info ID"           , &DatabaseInfo::id, primary_key())
                                     , make_column("Info Date Created" , &DatabaseInfo::dateCreated)
                                     , make_column("Info Playlist Tree", &DatabaseInfo::playlistData)
                                     )
                        , make_table("Tracks"
                                     , make_column("ID"               , &Track::id, primary_key())
                                     , make_column("File Path"        , &Track::filePath)
                                     , make_column("Title"            , &Track::title)
                                     , make_column("Artist"           , &Track::artist)
                                     , make_column("Date Added"       , &Track::dateAdded)
                                     , make_column("BPM"              , &Track::bpm)
                                     , make_column("Sample Rate"      , &Track::sampleRate)
                                     , make_column("Gain"             , &Track::gain)
                                     , make_column("Album"            , &Track::album)
                                     , make_column("Album Artist"     , &Track::albumArtist)
                                     , make_column("Track Number"     , &Track::trackNumber)
                                     , make_column("Year"             , &Track::year)
                                     , make_column("Genre"            , &Track::genre)
                                     , make_column("Comment"          , &Track::comment)
                                     , make_column("Composer"         , &Track::composer)
                                     , make_column("Remixer"          , &Track::remixer)
                                     , make_column("Label"            , &Track::label)
                                     , make_column("Bit Rate"         , &Track::bitRate)
                                     , make_column("Num Channels"     , &Track::numChannels)
                                     , make_column("Length (Seconds)" , &Track::lengthSeconds)
                                     , make_column("Key"              , &Track::key)
                                     , make_column("Beat Grid Start"  , &Track::beatGridStart)
                                     , make_column("Small Art"        , &Track::smallArtwork)
                                     , make_column("Large Art"        , &Track::largeArtwork)
                                     )
                        , make_table("Performance"
                                     , make_column("ID"            , &PerformanceData::id)
                                     , make_column("Track"         , &PerformanceData::trackId)
                                     , make_column("Cue"           , &PerformanceData::cuePosition)
                                     , make_column("Beatgrid Start", &PerformanceData::beatGridStart)
                                     , make_column("Loop Data"     , &PerformanceData::loopData)
                                     , make_column("Hotcue Data"   , &PerformanceData::hotCueData)
                                     , foreign_key(&PerformanceData::trackId).references(&Track::id)
                                     )
                        );
}

using SQLiteStorage = decltype(InitDatabaseStorage(""));
