/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace
{
    const String playlistType("playlist");
    const String childrenType("children");
    const String trackType("track");
    const String tracksType("tracks");
    const String nameType("name");
    const String trackIdIdentifier("id");
    const String dateIdentifier("date");
}

Playlist::Item::Item()
: tree      (ValueTree())
, trackId   (0)
, dateAdded (Time())
{
    
}

Playlist::Item::Item(ValueTree tree)
: tree      (tree)
, trackId   (track_t(int(tree.getProperty(trackIdIdentifier))))
, dateAdded (Time::fromISO8601(tree.getProperty(dateIdentifier).toString()))
{
    
}

const ValueTree &Playlist::Item::GetTree() const
{
    return tree;
}

Playlist::Playlist()
: tree         ()
, name         ()
, db           (nullptr)
{
    
}

Playlist::Playlist(Database *db)
: tree             (playlistType)
, playlistTree     (childrenType)
, trackTree        (tracksType)
, name             (tree, nameType, nullptr)
, db               (db)
{
    name = "New Playlist";
    
    tree.appendChild(playlistTree, nullptr);
    tree.appendChild(trackTree   , nullptr);
    
    db->DatabaseWillClose.AddListener(this, [=]{ this->Close(); });
    db->AddTracker(this);
}

Playlist::Playlist(const Playlist &other)
: tree             (other.tree)
, playlistTree     (tree.getChildWithName(childrenType))
, trackTree        (tree.getChildWithName(tracksType))
, name             (tree, nameType, nullptr)
, db               (other.db)
{
    db->DatabaseWillClose.AddListener(this, [=]{ this->Close(); });
    db->AddTracker(this);
}

Playlist::Playlist(ValueTree tree, Database *db)
: tree             (tree)
, playlistTree     (tree.getChildWithName(childrenType))
, trackTree        (tree.getChildWithName(tracksType))
, name             (tree, nameType, nullptr)
, db               (db)
{        
    db->DatabaseWillClose.AddListener(this, [=]{ this->Close(); });
    db->AddTracker(this);
}

Uuid Playlist::GetUid() const
{
    return Uuid(tree.getProperty("uid"));
}

String Playlist::GetName() const
{
    return IsValid() ? name : String();
}

void Playlist::SetName(String newName)
{
    jassert(IsValid());
    
    if (IsValid() && newName.isNotEmpty())
    {
        name = newName;
    }
}

void Playlist::InsertTrack(track_t trackId)
{
    if (IsValid())
    {
        ValueTree vt(trackType);
        vt.setProperty(trackIdIdentifier, int(trackId)                          , nullptr);
        vt.setProperty(dateIdentifier   , Time::getCurrentTime().toISO8601(true), nullptr);
        
        trackTree.appendChild(vt, nullptr);
    }
}

void Playlist::InsertPlaylist(const Playlist &playlist)
{
    if (IsValid())
    {
        ValueTree parent = playlist.tree.getParent();
        parent.removeChild(playlist.tree, nullptr);
        
        playlistTree.appendChild(playlist.tree, nullptr);
    }
}

void Playlist::RemoveTrack(const Item item)
{
    if (IsValid() && item.GetTree().isValid())
    {
        trackTree.removeChild(item.GetTree(), nullptr);
    }
}

void Playlist::RemovePlaylist(Playlist playlist)
{
    if (IsValid() && playlist)
    {
        playlistTree.removeChild(playlist.tree, nullptr);
        
        auto root = tree.getRoot();
        jassert(root.isValid());
        root.appendChild(playlist.tree, nullptr);
    }
}

size_t Playlist::NumTracks() const
{
    return IsValid() ? size_t(trackTree.getNumChildren()) : 0;
}

size_t Playlist::NumPlaylists() const
{
    return IsValid() ? size_t(playlistTree.getNumChildren()) : 0;
}

Playlist::Item Playlist::GetTrack(size_t index) const
{
    if (IsValid() && index < NumTracks())
    {
        return Item(trackTree.getChild(int(index)));
    }
    else
    {
        return Item();
    }
}

std::vector<Playlist::Item> Playlist::GetAllTracks() const
{
    std::vector<Item> items;
 
    if (IsValid())
    {
        const auto size = NumTracks();
        
        for (size_t i = 0; i < size; ++i)
        {
            items.emplace_back(GetTrack(i));
        }
    }
    
    return items;
}

Playlist Playlist::GetPlaylist(size_t index) const
{
    if (IsValid() && index < NumPlaylists())
    {
        return { playlistTree.getChild(int(index)), db };
    }
    else
    {
        return Playlist();
    }
}

Playlist Playlist::GetParentPlaylist() const
{
    // The parent tree is the parent't children tree, so we get that parent.
    auto parentTree = tree.getParent().getParent();
    
    return IsValid() && !ParentIsRoot() ? Playlist(parentTree, db)
                                        : Playlist();
}

bool Playlist::ParentIsRoot() const
{
    return IsValid() && tree.getParent() == tree.getRoot();
}

Playlist::operator bool() const
{
    return IsValid();
}

bool Playlist::IsValid() const
{
    return tree.isValid()
        && playlistTree.isValid()
        && playlistTree.getParent() == tree
        && trackTree.isValid()
        && trackTree.getParent() == tree
        && tree.getParent().isValid()
        && GetUid() != Uuid::null()
        && db != nullptr;
}

ValueTree Playlist::GetTree() const
{
    return tree;
}

void Playlist::TrackableWillBeDeleted(Trackable *trackable)
{
    Close();
}

void Playlist::Close()
{
    db           = nullptr;
    tree         = ValueTree();
    playlistTree = ValueTree();
    trackTree    = ValueTree();
}
