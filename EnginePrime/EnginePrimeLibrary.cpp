/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#define CHECK(returnType) if (!IsOpen()) return returnType

using namespace sqlite_orm;

Library::Library()
{
    
}

void Library::Open(const EnginePrime::Drive &drive)
{
    Close();
    
    const auto musicDbPath       = drive.GetMusicDb()      .getFullPathName().toStdString();
    const auto performanceDbPath = drive.GetPerformanceDb().getFullPathName().toStdString();
    
    storage.reset(new StorageAdaptor(  new MetadataStorage(InitMetadataStorage(musicDbPath))
                                     , new PerformanceStorage(InitPerformanceStorage(performanceDbPath)))
                  );
    Drive.reset(new EnginePrime::Drive(drive));
    
    playlistTree = ContainerParser::ToValueTree(*storage);
}

void Library::Close()
{
    storage         .reset();
    Drive.reset();
}

bool Library::IsOpen() const
{
    return storage         .get()
        && Drive.get()
        && playlistTree.isValid();
}

juce::String Library::GetVersion(  unsigned *major
                                            , unsigned *minor
                                            , unsigned *patch) const
{
    CHECK("");
    
    auto information = storage->GetMetadataInfo();
    
    jassert(information);
    
    if (major && minor && patch)
    {
        *major = information->schemaVersionMajor;
        *minor = information->schemaVersionMinor;
        *patch = information->schemaVersionPatch;
    }
    
    return juce::String::formatted(  "%d.%d.%d"
                                   , information->schemaVersionMajor
                                   , information->schemaVersionMinor
                                   , information->schemaVersionPatch);
}

size_t Library::CountTracks() const
{
    CHECK(0);
    return storage->GetMetadataStorage()->count<TrackTable>();
}

bool Library::ContainsTrack(track_t trackId) const
{
    return !storage->GetMetadataStorage()->select(&TrackTable::id, where(is_equal(&TrackTable::id, trackId))).empty();
}

std::vector<track_t> Library::GetAllTrackIds() const
{
    CHECK(std::vector<track_t>());
    
    try
    {
        auto metadata = storage->GetMetadataStorage();
        return metadata->select(&TrackTable::id);
    }
    catch (std::exception &e)
    {
        DBG(e.what());
        jassertfalse;
        
        return std::vector<track_t>();
    }
}

Track Library::GetTrack(track_t trackId) const
{
    CHECK(Track());
    
    auto artworkRow          = storage->GetAlbumArtRow        (trackId);
    auto trackRow            = storage->GetTrackRow           (trackId);
    auto metadataRows        = storage->GetMetaDataRow        (trackId);
    auto metadataIntegerRows = storage->GetMetaDataIntegerRow (trackId);
    
    return Track(  Drive
                       , artworkRow
                       , trackRow
                       , metadataRows
                       , metadataIntegerRows);
}

Container Library::GetRootPlaylist() const
{
    CHECK(Container());
    
    return Container(playlistTree, storage);
}

void Library::DumpContainers() const
{
    DBG(playlistTree.toXmlString());
}
