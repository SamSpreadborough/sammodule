/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

UserProfile::UserProfile(const Drive &drive)
: json  (JSON::parse(drive.GetUserProfile()))
, drive (drive)
{
    
}

std::shared_ptr<UserProfile> UserProfile::Create(const Drive &drive)
{
    if (IsLoadable(drive))
    {
        return std::shared_ptr<UserProfile>(new UserProfile(drive));
    }
    else
    {
        return nullptr;
    }
}

bool UserProfile::IsLoadable(const Drive &drive)
{
    return drive.Exists() && drive.GetUserProfile().existsAsFile();
}

BpmRangeOption UserProfile::GetBpmRangeOption() const
{
    return ToBpmRangeOption(GetStringOption(UserProfileChild::BPMRange));
}

bool UserProfile::GetNeedleLock() const
{
    return GetStateOption(UserProfileChild::NeedleLock);
}

bool UserProfile::GetColorizeKey() const
{
    return GetStateOption(UserProfileChild::ColorizeKey);
}

bool UserProfile::GetDisplayFileName() const
{
    return GetStateOption(UserProfileChild::DisplayFileName);
}

bool UserProfile::GetOnAirMode() const
{
    return GetStateOption(UserProfileChild::OnAirMode);
}

bool UserProfile::GetPadLock() const
{
    return GetStateOption(UserProfileChild::PadLock);
}

bool UserProfile::GetPlatterMode() const
{
    return GetStateOption(UserProfileChild::PlatterMode);
}

bool UserProfile::GetRemoteStartStop() const
{
    return GetStateOption(UserProfileChild::RemoteStartStop);
}

bool UserProfile::GetTimeDisplayMode() const
{
    return GetStateOption(UserProfileChild::TimeDisplayMode);
}

bool UserProfile::GetPausedHotCueBehaviour() const
{
    return GetStateOption(UserProfileChild::PausedHotCueBehaviour);
}

int UserProfile::GetTrackEndWarningTime() const
{
    return GetValueOption(UserProfileChild::TrackEndWarningTime);
}

juce::String UserProfile::GetName() const
{
    return GetStringOption(UserProfileChild::Name);
}

juce::String UserProfile::GetVersion() const
{
    return GetInfo()["version"];
}

const char *UserProfile::ToString(UserProfileChild child) const
{
    switch (child)
    {
        case UserProfileChild::Name:                    return "Name";
        case UserProfileChild::DefaultPlayBackPosition: return "DefaultPlayBackPosition";
        case UserProfileChild::TrackEndWarningTime:     return "TrackEndWarningTime";
        case UserProfileChild::OnAirMode:               return "OnAirMode";
        case UserProfileChild::LockPlayingDeck:         return "LockPlayingDeck";
        case UserProfileChild::NeedleLock:              return "NeedleLock";
        case UserProfileChild::KeyDisplayMode:          return "KeyDisplayMode";
        case UserProfileChild::ColorizeKey:             return "ColorizeKey";
        case UserProfileChild::QuantizeToGrid:          return "QuantizeToGrid";
        case UserProfileChild::LoopOutMode:             return "LoopOutMode";
        case UserProfileChild::SyncMode:                return "SyncMode";
        case UserProfileChild::SyncButtonAction:        return "SyncButtonAction";
        case UserProfileChild::BpmTolerance:            return "BpmTolerance";
        case UserProfileChild::KeyCompatibility:        return "KeyCompatibility";
        case UserProfileChild::DefaultSpeedRange:       return "DefaultSpeedRange";
        case UserProfileChild::DefaultLoopSize:         return "DefaultLoopSize";
        case UserProfileChild::BPMRange:                return "BPMRange";
        case UserProfileChild::PlayerColor1A:           return "PlayerColor1A";
        case UserProfileChild::PlayerColor1B:           return "PlayerColor1B";
        case UserProfileChild::PlayerColor1:            return "PlayerColor1";
        case UserProfileChild::PlayerColor2A:           return "PlayerColor2A";
        case UserProfileChild::PlayerColor2B:           return "PlayerColor2B";
        case UserProfileChild::PlayerColor2:            return "PlayerColor2";
        case UserProfileChild::PlayerColor3A:           return "PlayerColor3A";
        case UserProfileChild::PlayerColor3B:           return "PlayerColor3B";
        case UserProfileChild::PlayerColor3:            return "PlayerColor3";
        case UserProfileChild::PlayerColor4A:           return "PlayerColor4A";
        case UserProfileChild::PlayerColor4B:           return "PlayerColor4B";
        case UserProfileChild::PlayerColor4:            return "PlayerColor4";
        case UserProfileChild::TimeDisplayMode:         return "TimeDisplayMode";
        case UserProfileChild::PadLock:                 return "PadLock";
        case UserProfileChild::DisplayFileName:         return "DisplayFileName";
        case UserProfileChild::RemoteStartStop:         return "RemoteStartStop";
        case UserProfileChild::PausedHotCueBehaviour:   return "PausedHotCueBehaviour";
        case UserProfileChild::PlatterMode:             return "PlatterMode";
        case UserProfileChild::Product:                 return "Product";
    }
}

juce::var UserProfile::GetData() const
{
    return json["data"];
}

juce::var UserProfile::GetInfo() const
{
    return json["info"];
}

juce::var UserProfile::GetUser() const
{
    return GetData()["User"];
}

juce::var UserProfile::GetChildOrder() const
{
    return GetUser()["childorder"];
}

juce::var UserProfile::GetChildren() const
{
    return GetUser()["children"];
}

void UserProfile::DumpIntoConsole()
{
    DBG(JSON::toString(json));
}

bool UserProfile::GetStateOption (UserProfileChild child) const
{
    auto option = GetChildren()[ToString(child)];
    return option["state"];
}

juce::String UserProfile::GetStringOption (UserProfileChild child) const
{
    auto option = GetChildren()[ToString(child)];
    return option["string"];
}

juce::Colour UserProfile::GetColourOption(UserProfileChild child) const
{
    auto option = GetChildren()[ToString(child)];
    juce::String colourString = option["color"];
    
    return juce::Colour();
}

int UserProfile::GetValueOption (UserProfileChild child) const
{
    auto option = GetChildren()[ToString(child)];
    return option["value"];
}
