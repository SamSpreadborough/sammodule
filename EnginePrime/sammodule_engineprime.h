/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#if USE_SQLITE_ORM
namespace EnginePrime
{
#include "../EnginePrime/UserProfile/UserProfileUtils.h"
#include "../EnginePrime/EnginePrimeDrive.h"
#include "../EnginePrime/Database/Schema/MetadataTable.h"
#include "../EnginePrime/Database/Schema/PerformanceTable.h"
#include "../EnginePrime/Database/Schema/EnginePrimeSchema.h"
#include "../EnginePrime/Database/Containers/ContainerTreeIdentifiers.h"
#include "../EnginePrime/Database/Schema/StorageAdaptor.h"
#include "../EnginePrime/Database/Containers/ContainerParser.h"
#include "../EnginePrime/Database/Containers/ContainerHandle.h"
#include "../EnginePrime/Database/Track/TrackHandle.h"
#include "../EnginePrime/EnginePrimeLibrary.h"
#include "../EnginePrime/EnginePrimeUserProfile.h"
}
#endif
