/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#if USE_SQLITE_ORM
namespace EnginePrime
{
#include "../EnginePrime/EnginePrimeDrive.cpp"
#include "../EnginePrime/Database/Schema/StorageAdaptor.cpp"
#include "../EnginePrime/Database/Containers/ContainerParser.cpp"
#include "../EnginePrime/Database/Containers/ContainerHandle.cpp"
#include "../EnginePrime/Database/Track/TrackHandle.cpp"
#include "../EnginePrime/EnginePrimeLibrary.cpp"
#include "../EnginePrime/EnginePrimeUserProfile.cpp"
}
#endif
