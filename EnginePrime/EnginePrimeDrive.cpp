/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace
{
    const juce::String engineLibraryDir         = "Engine Library";
    const juce::String musicLibraryDir          = "Music";
    const juce::String musicDbFile              = "m.db";
    const juce::String musicDbJournalFile       = "m.db-journal";
    const juce::String performanceDbFile        = "p.db";
    const juce::String performanceDbJournalFile = "p.db-journal";
    const juce::String userProfileFile          = "user.profile";
    const juce::String versionFile              = "version";
}

std::shared_ptr<Drive> Drive::Create(const juce::File &drive)
{
    std::shared_ptr<Drive> ptr;
    
    if (IsLoadable(drive))
    {
        ptr.reset(new Drive(drive));
    }
    
    return ptr;
}

bool Drive::IsLoadable(const juce::File &drive)
{
    const auto libraryDir           = drive     .getChildFile(engineLibraryDir);
    const auto musicDir             = libraryDir.getChildFile(musicLibraryDir);
    const auto musicDb              = libraryDir.getChildFile(musicDbFile);
    const auto musicDbJournal       = libraryDir.getChildFile(musicDbJournalFile);
    const auto performanceDb        = libraryDir.getChildFile(performanceDbFile);
    const auto performanceDbJournal = libraryDir.getChildFile(performanceDbJournalFile);
    
    const bool exists                       = drive.exists();
    const bool isDir                        = drive.isDirectory();
    const bool containsLibraryDir           = libraryDir.exists() && libraryDir.isDirectory();
    const bool containsMusicDir             = musicDir  .exists() && musicDir  .isDirectory();
    const bool containsMusicDb              = musicDb             .existsAsFile();
    const bool containsMusicDbJournal       = musicDbJournal      .existsAsFile();
    const bool containsPerformanceDb        = performanceDb       .existsAsFile();
    const bool containsPerformanceDbJournal = performanceDbJournal.existsAsFile();
    
    return exists
        && isDir
        && containsLibraryDir
        && containsMusicDir
        && containsMusicDb
        && containsMusicDbJournal
        && containsPerformanceDb
        && containsPerformanceDbJournal;
}

juce::File Drive::GetDriveDir() const
{
    return drive;
}

juce::File Drive::GetEngineLibraryDir() const
{
    return drive.getChildFile(engineLibraryDir);
}

juce::File Drive::GetMusicDir() const
{
    return GetEngineLibraryDir().getChildFile(musicLibraryDir);
}

juce::File Drive::GetMusicDb() const
{
    return GetEngineLibraryDir().getChildFile(musicDbFile);
}

juce::File Drive::GetMusicDbJournal() const
{
    return GetEngineLibraryDir().getChildFile(musicDbJournalFile);
}

juce::File Drive::GetPerformanceDb() const
{
    return GetEngineLibraryDir().getChildFile(performanceDbFile);
}

juce::File Drive::GetPerformanceDbJournal() const
{
    return GetEngineLibraryDir().getChildFile(performanceDbJournalFile);
}

juce::File Drive::GetUserProfile() const
{
    return GetEngineLibraryDir().getChildFile(userProfileFile);
}

juce::File Drive::GetVersion() const
{
    return GetEngineLibraryDir().getChildFile(versionFile);
}

bool Drive::Exists() const
{
    return drive.exists();
}

Drive::Drive(const juce::File &drive)
: drive (drive)
{
    
}
