/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class UserProfile
{
public:
    static std::shared_ptr<UserProfile> Create(const Drive &drive);
    static bool IsLoadable(const Drive &drive);
    
    BpmRangeOption GetBpmRangeOption()        const;
    bool           GetNeedleLock()            const;
    bool           GetColorizeKey()           const;
    bool           GetDisplayFileName()       const;
    bool           GetOnAirMode()             const;
    bool           GetPadLock()               const;
    bool           GetPlatterMode()           const;
    bool           GetRemoteStartStop()       const;
    bool           GetTimeDisplayMode()       const;
    bool           GetPausedHotCueBehaviour() const;
    int            GetTrackEndWarningTime()   const;
    juce::String   GetVersion()               const;
    juce::String   GetName()                  const;
    
    void DumpIntoConsole();
private:
    UserProfile(const Drive &drive);
    
    enum class UserProfileChild
    {
          Name
        , DefaultPlayBackPosition
        , TrackEndWarningTime
        , OnAirMode
        , LockPlayingDeck
        , NeedleLock
        , KeyDisplayMode
        , ColorizeKey
        , QuantizeToGrid
        , LoopOutMode
        , SyncMode
        , SyncButtonAction
        , BpmTolerance
        , KeyCompatibility
        , DefaultSpeedRange
        , DefaultLoopSize
        , BPMRange
        , PlayerColor1A
        , PlayerColor1B
        , PlayerColor1
        , PlayerColor2A
        , PlayerColor2B
        , PlayerColor2
        , PlayerColor3A
        , PlayerColor3B
        , PlayerColor3
        , PlayerColor4A
        , PlayerColor4B
        , PlayerColor4
        , TimeDisplayMode
        , PadLock
        , DisplayFileName
        , RemoteStartStop
        , PausedHotCueBehaviour
        , PlatterMode
        , Product
    };
    
    const char *ToString(UserProfileChild child) const;

    juce::var GetData()       const;
    juce::var GetInfo()       const;
    juce::var GetUser()       const;
    juce::var GetChildOrder() const;
    juce::var GetChildren()   const;
    
    bool         GetStateOption (UserProfileChild child) const;
    juce::String GetStringOption(UserProfileChild child) const;
    juce::Colour GetColourOption(UserProfileChild child) const;
    int          GetValueOption (UserProfileChild child) const;
    
    juce::var json;
    
    const Drive drive;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (UserProfile)
};
