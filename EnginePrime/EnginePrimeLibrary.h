/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class Library
{
public:
    Library();

    void Open(const Drive &drive);
    void Close();
    bool IsOpen() const;
    
    juce::String GetVersion(  unsigned *major = nullptr
                            , unsigned *minor = nullptr
                            , unsigned *patch = nullptr) const;
    
    size_t               CountTracks()                   const;
    bool                 ContainsTrack(track_t trackId)  const;
    std::vector<track_t> GetAllTrackIds()                const;
    Track                GetTrack(track_t trackId)       const;
    
    Container GetRootPlaylist() const;
    
    void DumpContainers() const;
private:
    std::shared_ptr<StorageAdaptor>   storage;
    std::shared_ptr<Drive> Drive;
    juce::ValueTree                   playlistTree;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Library)
};
