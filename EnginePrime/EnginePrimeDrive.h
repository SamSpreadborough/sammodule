/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

/**
    A class for easy access of engine prime file organisation.
 */
class Drive
{
public:
    /// If the drive is loadable, returns a new Drive object, else
    /// returns nullptr.
    static std::shared_ptr<Drive> Create(const juce::File &drive);
    
    /// Checks if the drive is loadable as an Engine Prime drive
    static bool IsLoadable(const juce::File &drive);
    
    juce::File GetDriveDir()             const;
    juce::File GetEngineLibraryDir()     const;
    juce::File GetMusicDir()             const;
    juce::File GetMusicDb()              const;
    juce::File GetMusicDbJournal()       const;
    juce::File GetPerformanceDb()        const;
    juce::File GetPerformanceDbJournal() const;
    juce::File GetUserProfile()          const;
    juce::File GetVersion()              const;
    
    /// Checks that the drive exists.
    bool Exists() const;
protected:
    Drive(const juce::File &drive);
    
private:
    const juce::File drive;
    
    JUCE_LEAK_DETECTOR (Drive)
};
