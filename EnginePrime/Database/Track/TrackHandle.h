/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

enum MusicalKey : int
{
      KeyNone = 0
    , Key8A   = 1  // A minor
    , Key9B   = 2  // G major
    , Key9A   = 3  // E minor
    , Key10B  = 4  // D major
    , Key10A  = 5  // B minor
    , Key11B  = 6  // A major
    , Key11A  = 7  // F# minor
    , Key12B  = 8  // E major
    , Key12A  = 9  // Db minor
    , Key1B   = 10 // B major
    , Key1A   = 11 // Ab minor
    , Key2B   = 12 // F# major
    , Key2A   = 13 // Eb minor
    , Key3B   = 14 // Db major
    , Key3A   = 15 // Bb minor
    , Key4B   = 16 // Ab major
    , Key4A   = 17 // F minor
    , Key5B   = 18 // Eb major
    , Key5A   = 19 // C minor
    , Key6B   = 20 // Bb major
    , Key6A   = 21 // G minor
    , Key7B   = 22 // F major
    , Key7A   = 23 // D minor
    , Key8B   = 24 // C major (TBC confirm this is 24, as opposed to 0)
};

class Track
{
public:
    Track();
    Track(  std::shared_ptr<Drive>  drive
          , std::shared_ptr<AlbumArtTable>     artworkRow
          , std::shared_ptr<TrackTable>        trackRow
          , std::vector<MetaDataTable>         metadataRows
          , std::vector<MetaDataIntegerTable>  metadataIntegerRows);
    
    track_t      GetId()               const;
    unsigned     GetLength()           const;
    unsigned     GetLengthCalculated() const;
    unsigned     GetBpm()              const;
    double       GetBpmCalculated()    const;
    unsigned     GetYear()             const;
    unsigned     GetBitrate()          const;
    juce::String GetTitle()            const;
    juce::String GetArtist()           const;
    juce::String GetAlbum()            const;
    juce::String GetGenre()            const;
    juce::String GetComment()          const;
    juce::String GetPublisher()        const;
    juce::String GetComposer()         const;
    juce::String GetDuration()         const;
    bool         IsPlayed()            const;
    juce::String GetFileExt()          const;
    juce::File   GetFile()             const;
    unsigned     GetTrackNumber()      const;
    MusicalKey   GetKey()              const;
    juce::Image  GetArtwork()          const;
        
    bool IsValid() const;
    
    void Dump() const;
private:
    enum MetaDataType
    {
          TypeTitle     = 0
        , TypeArtist    = 1
        , TypeAlbum     = 2
        , TypeGenre     = 3
        , TypeComment   = 4
        , TypePublisher = 5
        , TypeComposer  = 6
        , TypeDuration  = 9
        , TypePlayed    = 11
        , TypeFileExt   = 12
    };
    
    enum MetaDataIntegerType
    {
          TypeDateLastPlayed   = 0
        , TypeDateLastChanged  = 1
        , TypeDateFileLastLoad = 2
        , TypeMusicalKey       = 3
    };
    
    std::shared_ptr<Drive> drive;
    std::shared_ptr<AlbumArtTable>    artworkRow;
    std::shared_ptr<TrackTable>       trackRow;
    std::vector<MetaDataTable>        metadataRows;
    std::vector<MetaDataIntegerTable> metadataIntegerRows;
    
    JUCE_LEAK_DETECTOR (Track)
};
