/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

#define CHECK(returnType) if (!IsValid()) return returnType;

Track::Track()
{
    
}

Track::Track(  std::shared_ptr<Drive>  drive
                         , std::shared_ptr<AlbumArtTable>     artworkRow
                         , std::shared_ptr<TrackTable>        trackRow
                         , std::vector<MetaDataTable>         metadataRows
                         , std::vector<MetaDataIntegerTable>  metadataIntegerRows)
: drive               (drive)
, artworkRow          (artworkRow)
, trackRow            (trackRow)
, metadataRows        (metadataRows)
, metadataIntegerRows (metadataIntegerRows)
{
    
}

track_t Track::GetId() const
{
    CHECK(0);
    return track_t(trackRow->id);
}

unsigned Track::GetLength() const
{
    CHECK(0);
    return trackRow->length;
}

unsigned Track::GetLengthCalculated() const
{
    CHECK(0);
    return trackRow->lengthCalculated;
}

unsigned Track::GetBpm() const
{
    CHECK(0);
    return trackRow->bpm;
}

double Track::GetBpmCalculated() const
{
    CHECK(0.0);
    return trackRow->bpmAnalyzed;
}

unsigned Track::GetYear() const
{
    CHECK(0);
    return trackRow->year;
}

unsigned Track::GetBitrate() const
{
    CHECK(0);
    return trackRow->bitrate;
}

juce::String Track::GetTitle() const
{
    CHECK("");
    return metadataRows[TypeTitle].text;
}

juce::String Track::GetArtist() const
{
    CHECK("");
    return metadataRows[TypeArtist].text;
}

juce::String Track::GetAlbum() const
{
    CHECK("");
    return metadataRows[TypeAlbum].text;
}

juce::String Track::GetGenre() const
{
    CHECK("");
    return metadataRows[TypeGenre].text;
}

juce::String Track::GetComment() const
{
    CHECK("");
    return metadataRows[TypeComment].text;
}

juce::String Track::GetPublisher() const
{
    CHECK("");
    return metadataRows[TypePublisher].text;
}

juce::String Track::GetComposer() const
{
    CHECK("");
    return metadataRows[TypeComposer].text;
}

juce::String Track::GetDuration() const
{
    CHECK("");
    return metadataRows[TypeDuration].text;
}

bool Track::IsPlayed() const
{
    CHECK(false);
    return juce::String(metadataRows[TypePlayed].text).getIntValue();
}

juce::String Track::GetFileExt() const
{
    CHECK("");
    return metadataRows[TypeFileExt].text;
}

juce::File Track::GetFile() const
{
    CHECK(juce::File());
    return drive->GetMusicDir().getFullPathName() + "/" + trackRow->filename;
}

unsigned Track::GetTrackNumber() const
{
    CHECK(0);
    return trackRow->playOrder;
}

juce::Image Track::GetArtwork() const
{
    CHECK(juce::Image());
    return artworkRow ? ImageCache::getFromMemory(artworkRow->albumArt.data(), int(artworkRow->albumArt.size()))
                      : juce::Image();
}

MusicalKey Track::GetKey() const
{
    CHECK(MusicalKey::KeyNone);
    return MusicalKey(metadataIntegerRows[TypeMusicalKey].value);
}

bool Track::IsValid() const
{
    return drive.get()
    && artworkRow.get()
    && trackRow.get()
    && metadataRows.size()
    && metadataIntegerRows.size();
}

void Track::Dump() const
{
    DBG("============================================");
    
    DBG("ID         |" << int(GetId())                );
    DBG("Length     |" << int(GetLength())            );
    DBG("Length     |" << int(GetLengthCalculated())  );
    DBG("BPM        |" << int(GetBpm())               );
    DBG("BPM        |" << GetBpmCalculated()          );
    DBG("Year       |" << int(GetYear())              );
    DBG("Bitrate    |" << int(GetBitrate())           );
    DBG("Title      |" << GetTitle()                  );
    DBG("Artist     |" << GetArtist()                 );
    DBG("Album      |" << GetAlbum()                  );
    DBG("Genre      |" << GetGenre()                  );
    DBG("Comment    |" << GetComment()                );
    DBG("Publisher  |" << GetPublisher()              );
    DBG("Composer   |" << GetComposer()               );
    DBG("Duration   |" << GetDuration()               );
    DBG("Is Played  |" << int(IsPlayed())             );
    DBG("File Ext   |" << GetFileExt()                );
    DBG("File path  |" << GetFile().getFullPathName() );
    DBG("Track #    |" << int(GetTrackNumber())       );
    DBG("Key        |" << int(GetKey())               );
    
    DBG("============================================");
}

#undef CHECK
