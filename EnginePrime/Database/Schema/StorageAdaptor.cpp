/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

StorageAdaptor::StorageAdaptor(  MetadataStorage    *metadataStorage
                               , PerformanceStorage *performanceStorage)
: metadataStorage    (metadataStorage)
, performanceStorage (performanceStorage)
{
    
}

std::shared_ptr<MetadataInformationTable> StorageAdaptor::GetMetadataInfo() const
{
    return metadataStorage->get_no_throw<MetadataInformationTable>(1);
}

std::shared_ptr<PerformanceInformationTable> StorageAdaptor::GetPerformanceInfo() const
{
    return performanceStorage->get_no_throw<PerformanceInformationTable>(1);
}

std::shared_ptr<AlbumArtTable> StorageAdaptor::GetAlbumArtRow(track_t trackId) const
{
    if (auto track = GetTrackRow(trackId))
    {
        return metadataStorage->get_no_throw<AlbumArtTable>(track->idAlbumArt);
    }
    else
    {
        return nullptr;
    }
}

std::shared_ptr<CopiedTrackTable> StorageAdaptor::GetCopiedTrackRow(track_t trackId) const
{
    return metadataStorage->get_no_throw<CopiedTrackTable>(trackId);
}

std::shared_ptr<TrackTable> StorageAdaptor::GetTrackRow(track_t trackId) const
{
    return metadataStorage->get_no_throw<TrackTable>(trackId);
}

std::vector<MetaDataTable> StorageAdaptor::GetMetaDataRow(track_t trackId) const
{
    using namespace sqlite_orm;
    return metadataStorage->get_all<MetaDataTable>(where(is_equal(&MetaDataTable::id, trackId)));
}

std::vector<MetaDataIntegerTable> StorageAdaptor::GetMetaDataIntegerRow(track_t trackId) const
{
    using namespace sqlite_orm;
    return metadataStorage->get_all<MetaDataIntegerTable>(where(is_equal(&MetaDataIntegerTable::id, trackId)));
}

std::shared_ptr<PerformanceDataTable> StorageAdaptor::GetPerformanceDataRow(track_t trackId) const
{
    return performanceStorage->get_no_throw<PerformanceDataTable>(trackId);
}

std::shared_ptr<ListTable> StorageAdaptor::GetListRow(list_t listId) const
{
    return metadataStorage->get_no_throw<ListTable>(listId);
}

std::shared_ptr<ListHierarchyTable> StorageAdaptor::GetListHierarchyRow (list_t listId) const
{
    return metadataStorage->get_no_throw<ListHierarchyTable>(listId);
}

std::shared_ptr<ListParentListTable> StorageAdaptor::GetListParentListRow (list_t listId) const
{
    return metadataStorage->get_no_throw<ListParentListTable>(listId);
}

std::shared_ptr<ListTrackListTable> StorageAdaptor::GetListTrackListRow (list_t listId) const
{
    return metadataStorage->get_no_throw<ListTrackListTable>(listId);
}

std::shared_ptr<MetadataStorage> StorageAdaptor::GetMetadataStorage() const
{
    return metadataStorage;
}

std::shared_ptr<PerformanceStorage> StorageAdaptor::GetPerformanceStorage() const
{
    return performanceStorage;
}
