/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

inline
auto InitMetadataStorage(const std::string &path)
{
    using namespace sqlite_orm;
    
    return make_storage(  path
                        
                        , make_index("index_AlbumArt_hash", &AlbumArtTable::hash)
                        , make_index("index_AlbumArt_id"  , &AlbumArtTable::id)
                        , make_table(  "AlbumArt"
                                     , make_column("id"      , &AlbumArtTable::id, primary_key())
                                     , make_column("hash"    , &AlbumArtTable::hash)
                                     , make_column("albumArt", &AlbumArtTable::albumArt)
                                     )
                        
                        , make_index("index_CopiedTrack_trackId", &CopiedTrackTable::trackId)
                        , make_table(  "CopiedTrack"
                                     , make_column("trackId"                  , &CopiedTrackTable::trackId, primary_key())
                                     , make_column("uuidOfSourceDatabase"     , &CopiedTrackTable::uuidOfSourceDatabase)
                                     , make_column("idOfTrackInSourceDatabase", &CopiedTrackTable::idOfTrackInSourceDatabase)
                                     , foreign_key(&CopiedTrackTable::trackId).references(&TrackTable::id)
                                     )
                        
                        , make_index("index_Information_id", &MetadataInformationTable::id)
                        , make_table(  "Information"
                                     , make_column("id"                                   , &MetadataInformationTable::id, primary_key())
                                     , make_column("uuid"                                 , &MetadataInformationTable::uuid)
                                     , make_column("schemaVersionMajor"                   , &MetadataInformationTable::schemaVersionMajor)
                                     , make_column("schemaVersionMinor"                   , &MetadataInformationTable::schemaVersionMinor)
                                     , make_column("schemaVersionPatch"                   , &MetadataInformationTable::schemaVersionPatch)
                                     , make_column("currentPlayedIndicator"               , &MetadataInformationTable::currentPlayedIndicator)
                                     , make_column("lastRekordBoxLibraryImportReadCounter", &MetadataInformationTable::lastRekordBoxLibraryImportReadCounter)
                                     )
                        
                        , make_index("index_InternalDatabase_uuid", &InternalDatabaseTable::uuid)
                        , make_table(  "InternalDatabase"
                                     , make_column("uuid", &InternalDatabaseTable::uuid, primary_key())
                                     , make_column("name", &InternalDatabaseTable::name)
                                     )
                        
                        , make_index("index_ListId_id"      , &ListTable::id)
                        , make_index("index_ListId_ordering", &ListTable::ordering)
                        , make_index("index_ListId_path"    , &ListTable::path)
                        , make_index("index_ListId_type"    , &ListTable::type)
                        , make_table(  "List"
                                     , make_column("id"        , &ListTable::id, primary_key())
                                     , make_column("type"      , &ListTable::type, primary_key())
                                     , make_column("title"     , &ListTable::title)
                                     , make_column("path"      , &ListTable::path)
                                     , make_column("isFolder"  , &ListTable::isFolder)
                                     , make_column("trackCount", &ListTable::trackCount)
                                     , make_column("ordering"  , &ListTable::ordering)
                                     )
                        
                        , make_index("index_ListHierarchy_listId"       , &ListHierarchyTable::listId)
                        , make_index("index_ListHierarchy_listIdChild"  , &ListHierarchyTable::listIdChild)
                        , make_index("index_ListHierarchy_listType"     , &ListHierarchyTable::listType)
                        , make_index("index_ListHierarchy_listTypeChild", &ListHierarchyTable::listTypeChild)
                        , make_table(  "ListHierarchy"
                                     , make_column("listId"       , &ListHierarchyTable::listId)
                                     , make_column("listType"     , &ListHierarchyTable::listType)
                                     , make_column("listIdChild"  , &ListHierarchyTable::listIdChild)
                                     , make_column("listTypeChild", &ListHierarchyTable::listTypeChild)
                                     , foreign_key(&ListHierarchyTable::listId)     .references(&ListTable::id)
                                     , foreign_key(&ListHierarchyTable::listIdChild).references(&ListTable::id)
                                     )
                        
                        , make_index("index_ListParentList_listOriginId"  , &ListParentListTable::listOriginId)
                        , make_index("index_ListParentList_listOriginType", &ListParentListTable::listOriginType)
                        , make_index("index_ListParentList_listParentId"  , &ListParentListTable::listParentId)
                        , make_index("index_ListParentList_listParentType", &ListParentListTable::listParentType)
                        , make_table(  "ListParentList"
                                     , make_column("listOriginId"  , &ListParentListTable::listOriginId)
                                     , make_column("listOriginType", &ListParentListTable::listOriginType)
                                     , make_column("listParentId"  , &ListParentListTable::listParentId)
                                     , make_column("listParentType", &ListParentListTable::listParentType)
                                     , foreign_key(&ListParentListTable::listOriginId).references(&ListTable::id)
                                     , foreign_key(&ListParentListTable::listParentId).references(&ListTable::id)
                                     )
                        
                        , make_index("index_ListParentList_listId"  , &ListTrackListTable::listId)
                        , make_index("index_ListParentList_listType", &ListTrackListTable::listType)
                        , make_index("index_ListParentList_trackId" , &ListTrackListTable::trackId)
                        , make_table(  "ListTrackList"
                                     , make_column("id"                     , &ListTrackListTable::id, primary_key())
                                     , make_column("listId"                 , &ListTrackListTable::listId)
                                     , make_column("listType"               , &ListTrackListTable::listType)
                                     , make_column("trackId"                , &ListTrackListTable::trackId)
                                     , make_column("trackIdInOriginDatabase", &ListTrackListTable::trackIdInOriginDatabase)
                                     , make_column("databaseUuid"           , &ListTrackListTable::databaseUuid)
                                     , make_column("trackNumber"            , &ListTrackListTable::trackNumber)
                                     , foreign_key(&ListTrackListTable::listId) .references(&ListTable::id)
                                     , foreign_key(&ListTrackListTable::trackId).references(&TrackTable::id)
                                     )
                        
                        , make_index("index_MetaData_id"  , &MetaDataTable::id)
                        , make_index("index_MetaData_text", &MetaDataTable::text)
                        , make_index("index_MetaData_type", &MetaDataTable::type)
                        , make_table("MetaData"
                                     , make_column("id"  , &MetaDataTable::id, primary_key())
                                     , make_column("type", &MetaDataTable::type, primary_key())
                                     , make_column("text", &MetaDataTable::text)
                                     , foreign_key(&MetaDataTable::id).references(&TrackTable::id)
                                     )
                        
                        , make_index("index_MetaDataInteger_id"   , &MetaDataIntegerTable::id)
                        , make_index("index_MetaDataInteger_type" , &MetaDataIntegerTable::type)
                        , make_index("index_MetaDataInteger_value", &MetaDataIntegerTable::value)
                        , make_table("MetaDataInteger"
                                     , make_column("id"   , &MetaDataIntegerTable::id, primary_key())
                                     , make_column("type" , &MetaDataIntegerTable::type, primary_key())
                                     , make_column("value", &MetaDataIntegerTable::value)
                                     , foreign_key(&MetaDataIntegerTable::id).references(&TrackTable::id)
                                     )
                        
                        , make_index("index_Track_filename"                 , &TrackTable::filename)
                        , make_index("index_Track_id"                       , &TrackTable::id)
                        , make_index("index_Track_idAlbumArt"               , &TrackTable::idAlbumArt)
                        , make_index("index_Track_idTrackInExternalDatabase", &TrackTable::idTrackInExternalDatabase)
                        , make_index("index_Track_isExternalTrack"          , &TrackTable::isExternalTrack)
                        , make_index("index_Track_path"                     , &TrackTable::path)
                        , make_index("index_Track_uuidOfExternalDatabase"   , &TrackTable::uuidOfExternalDatabase)
                        , make_table("Track"
                                     , make_column("id"                       , &TrackTable::id, primary_key())
                                     , make_column("playOrder"                , &TrackTable::playOrder)
                                     , make_column("length"                   , &TrackTable::length)
                                     , make_column("lengthCalculated"         , &TrackTable::lengthCalculated)
                                     , make_column("bpm"                      , &TrackTable::bpm)
                                     , make_column("year"                     , &TrackTable::year)
                                     , make_column("path"                     , &TrackTable::path)
                                     , make_column("filename"                 , &TrackTable::filename)
                                     , make_column("bitrate"                  , &TrackTable::bitrate)
                                     , make_column("bpmAnalyzed"              , &TrackTable::bpmAnalyzed)
                                     , make_column("trackType"                , &TrackTable::trackType)
                                     , make_column("isExternalTrack"          , &TrackTable::isExternalTrack)
                                     , make_column("uuidOfExternalDatabase"   , &TrackTable::uuidOfExternalDatabase)
                                     , make_column("idTrackInExternalDatabase", &TrackTable::idTrackInExternalDatabase)
                                     , make_column("idAlbumArt"               , &TrackTable::idAlbumArt)
                                     , make_column("pdbImportKey"             , &TrackTable::pdbImportKey)
                                     )
                        );
}

inline
auto InitPerformanceStorage(const std::string &path)
{
    using namespace sqlite_orm;
    
    return make_storage(  path
                        , make_index("index_Information_id", &PerformanceInformationTable::id)
                        , make_table(  "Information"
                                     , make_column("id"                                   , &PerformanceInformationTable::id, primary_key())
                                     , make_column("uuid"                                 , &PerformanceInformationTable::uuid)
                                     , make_column("schemaVersionMajor"                   , &PerformanceInformationTable::schemaVersionMajor)
                                     , make_column("schemaVersionMinor"                   , &PerformanceInformationTable::schemaVersionMinor)
                                     , make_column("schemaVersionPatch"                   , &PerformanceInformationTable::schemaVersionPatch)
                                     , make_column("currentPlayedIndciator"               , &PerformanceInformationTable::currentPlayedIndicator)
                                     , make_column("lastRekordBoxLibraryImportReadCounter", &PerformanceInformationTable::lastRekordBoxLibraryImportReadCounter)
                                     )
                        
                        , make_index("index_PerformanceData_id", &PerformanceDataTable::id)
                        , make_table(  "PerformanceData"
                                     , make_column("id"                        , &PerformanceDataTable::id, primary_key())
                                     , make_column("isAnalyzed"                , &PerformanceDataTable::isAnalyzed)
                                     , make_column("isRendered"                , &PerformanceDataTable::isRendered)
                                     , make_column("trackData"                 , &PerformanceDataTable::trackData)
                                     , make_column("highResolutionWaveFormData", &PerformanceDataTable::highResolutionWaveFormData)
                                     , make_column("overviewWaveFormData"      , &PerformanceDataTable::overviewWaveFormData)
                                     , make_column("beatData"                  , &PerformanceDataTable::beatData)
                                     , make_column("quickCues"                 , &PerformanceDataTable::quickCues)
                                     , make_column("loops"                     , &PerformanceDataTable::loops)
                                     , make_column("hasSeratoValue"            , &PerformanceDataTable::hasSeratoValue)
                                     , make_column("hasRekordboxValues"        , &PerformanceDataTable::hasRekordboxValues)
                                     , make_column("hasTraktorValues"          , &PerformanceDataTable::hasTraktorValues)
                                     )
                        );
}

using MetadataStorage    = decltype(InitMetadataStorage(""));
using PerformanceStorage = decltype(InitPerformanceStorage(""));
