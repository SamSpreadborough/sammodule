/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

using track_t = unsigned;
using list_t  = unsigned;

struct AlbumArtTable
{
    unsigned          id;
    std::string       hash;
    std::vector<char> albumArt;
};

struct CopiedTrackTable
{
    std::shared_ptr<track_t> trackId;
    std::string              uuidOfSourceDatabase;
    unsigned                 idOfTrackInSourceDatabase;
};

struct MetadataInformationTable
{
    unsigned    id;
    std::string uuid;
    unsigned    schemaVersionMajor;
    unsigned    schemaVersionMinor;
    unsigned    schemaVersionPatch;
    unsigned    currentPlayedIndicator;
    unsigned    lastRekordBoxLibraryImportReadCounter;
};

struct InternalDatabaseTable
{
    std::string uuid;
    std::string name;
};

struct ListTable
{
    list_t      id;
    unsigned    type;
    std::string title;
    std::string path;
    bool        isFolder;
    unsigned    trackCount;
    unsigned    ordering;
};

struct ListHierarchyTable
{
    std::shared_ptr<list_t> listId;
    unsigned                listType;
    std::shared_ptr<list_t> listIdChild;
    unsigned                listTypeChild;
};

struct ListParentListTable
{
    std::shared_ptr<list_t> listOriginId;
    unsigned                listOriginType;
    std::shared_ptr<list_t> listParentId;
    unsigned                listParentType;
};

struct ListTrackListTable
{
    unsigned                 id;
    std::shared_ptr<list_t>  listId;
    unsigned                 listType;
    std::shared_ptr<track_t> trackId;
    unsigned                 trackIdInOriginDatabase;
    std::string              databaseUuid;
    unsigned                 trackNumber;
};

struct MetaDataTable
{
    std::shared_ptr<track_t> id;
    unsigned                 type;
    std::string              text;
};

struct MetaDataIntegerTable
{
    std::shared_ptr<track_t> id;
    unsigned                 type;
    unsigned                 value;
};

struct TrackTable
{
    track_t     id;
    unsigned    playOrder;
    unsigned    length;
    unsigned    lengthCalculated;
    unsigned    bpm;
    unsigned    year;
    std::string path;
    std::string filename;
    unsigned    bitrate;
    double      bpmAnalyzed;
    unsigned    trackType;
    bool        isExternalTrack;
    std::string uuidOfExternalDatabase;
    unsigned    idTrackInExternalDatabase;
    unsigned    idAlbumArt;
    unsigned    pdbImportKey;
};
