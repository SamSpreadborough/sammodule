/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class StorageAdaptor
{
public:
    StorageAdaptor(  MetadataStorage    *metadataStorage
                   , PerformanceStorage *performanceStorage);
    
    std::shared_ptr<MetadataInformationTable>    GetMetadataInfo()    const;
    std::shared_ptr<PerformanceInformationTable> GetPerformanceInfo() const;
    
    std::shared_ptr<AlbumArtTable>        GetAlbumArtRow        (track_t trackId) const;
    std::shared_ptr<CopiedTrackTable>     GetCopiedTrackRow     (track_t trackId) const;
    std::shared_ptr<TrackTable>           GetTrackRow           (track_t trackId) const;
    std::vector<MetaDataTable>            GetMetaDataRow        (track_t trackId) const;
    std::vector<MetaDataIntegerTable>     GetMetaDataIntegerRow (track_t trackId) const;
    std::shared_ptr<PerformanceDataTable> GetPerformanceDataRow (track_t trackId) const;
    std::shared_ptr<ListTable>            GetListRow            (list_t listId)   const;
    std::shared_ptr<ListHierarchyTable>   GetListHierarchyRow   (list_t listId)   const;
    std::shared_ptr<ListParentListTable>  GetListParentListRow  (list_t listId)   const;
    std::shared_ptr<ListTrackListTable>   GetListTrackListRow   (list_t listId)   const;
        
    std::shared_ptr<MetadataStorage>    GetMetadataStorage()    const;
    std::shared_ptr<PerformanceStorage> GetPerformanceStorage() const;
private:
    std::shared_ptr<MetadataStorage>    metadataStorage;
    std::shared_ptr<PerformanceStorage> performanceStorage;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (StorageAdaptor)
};
