/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

struct PerformanceInformationTable
{
    unsigned    id;
    std::string uuid;
    unsigned    schemaVersionMajor;
    unsigned    schemaVersionMinor;
    unsigned    schemaVersionPatch;
    unsigned    currentPlayedIndicator;
    unsigned    lastRekordBoxLibraryImportReadCounter;
};

struct PerformanceDataTable
{
    unsigned          id;
    bool              isAnalyzed;
    bool              isRendered;
    std::vector<char> trackData;
    std::vector<char> highResolutionWaveFormData;
    std::vector<char> overviewWaveFormData;
    std::vector<char> beatData;
    std::vector<char> quickCues;
    std::vector<char> loops;
    bool              hasSeratoValue;
    bool              hasRekordboxValues;
    bool              hasTraktorValues;
};

struct TrackData
{
    double sampleRate;
    uint64 sampleLength;
    double loudness;
    uint32 analysedKey;
};
