/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

juce::ValueTree ContainerParser::ToValueTree(const StorageAdaptor &storage)
{
    auto tree = juce::ValueTree(ContainerTreeIdentifiers::rootId);
    
    auto metadata = storage.GetMetadataStorage();
    
    jassert(metadata);
    
    std::map<list_t, juce::ValueTree> treeMap;
    
    for (auto &list : metadata->get_all<ListTable>())
    {
        juce::ValueTree child(ContainerTreeIdentifiers::crateId);
        child.setProperty(ContainerTreeIdentifiers::crateIdId   , int(list.id)            , nullptr);
        child.setProperty(ContainerTreeIdentifiers::listTypeId  , int(list.type)          , nullptr);
        child.setProperty(ContainerTreeIdentifiers::titleId     , juce::String(list.title), nullptr);
        child.setProperty(ContainerTreeIdentifiers::pathId      , juce::String(list.path) , nullptr);
        child.setProperty(ContainerTreeIdentifiers::isFolderId  , list.isFolder           , nullptr);
        child.setProperty(ContainerTreeIdentifiers::trackCountId, int(list.trackCount)    , nullptr);
        child.setProperty(ContainerTreeIdentifiers::orderingId  , int(list.ordering)      , nullptr);
        
        treeMap[list.id] = child;
        tree.addChild(child, -1, nullptr);
    }
    
    for (auto &list : metadata->get_all<ListHierarchyTable>())
    {
        auto parent = treeMap[*list.listId];
        auto child  = treeMap[*list.listIdChild];
        
        child.getParent().removeChild(child, nullptr);
        parent.addChild(child, -1, nullptr);
    }
    
    return tree;
}
