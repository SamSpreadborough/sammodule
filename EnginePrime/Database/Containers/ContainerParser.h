/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class StorageAdaptor;

class ContainerParser
{
public:
    static juce::ValueTree ToValueTree(const StorageAdaptor &storage);
};
