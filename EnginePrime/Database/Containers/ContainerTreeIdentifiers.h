/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

namespace ContainerTreeIdentifiers
{
    const Identifier rootId       = "root";
    const Identifier crateId      = "crate";
    const Identifier crateIdId    = "id";
    const Identifier listTypeId   = "type";
    const Identifier titleId      = "title";
    const Identifier pathId       = "path";
    const Identifier isFolderId   = "isFolder";
    const Identifier trackCountId = "trackCount";
    const Identifier orderingId   = "ordering";
};
