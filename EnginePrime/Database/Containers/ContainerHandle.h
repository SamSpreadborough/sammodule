/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

class StorageAdaptor;

enum ContainerType
{
      TypeError    = 0
    , TypePlaylist = 1
    , TypeHistory  = 2
    , TypePrepare  = 3
    , TypeCrate    = 4
};

class Container
{
public:
    Container();
    Container(  juce::ValueTree                 tree
                    , std::shared_ptr<StorageAdaptor> storage);
    Container(const Container &other);
    
    list_t        GetId()         const;
    ContainerType GetType()       const;
    juce::String  GetName()       const;
    juce::String  GetPath()       const;
    bool          GetIsFolder()   const;
    size_t        GetTrackCount() const;
    unsigned      GetOrdering()   const;
    
    std::vector<track_t> GetTracks() const;
    
    size_t          GetNumChildren()       const;
    Container GetParent()            const;
    Container GetChild(size_t index) const;
    Container GetRoot()              const;
    
    bool IsValid() const;
    bool IsRoot()  const;
    
    void Dump() const;
private:
    void Init();
    
    CachedValue<int>          ID;
    CachedValue<int>          Type;
    CachedValue<juce::String> Title;
    CachedValue<juce::String> Path;
    CachedValue<bool>         IsFolder;
    CachedValue<int>          TrackCount;
    CachedValue<int>          Ordering;
    
    juce::ValueTree                 tree;
    std::shared_ptr<StorageAdaptor> storage;
};
