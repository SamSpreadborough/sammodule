/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

Container::Container()
{
    
}

Container::Container(  juce::ValueTree                 tree
                               , std::shared_ptr<StorageAdaptor> storage)
: tree    (tree)
, storage (storage)
{
    Init();
}

Container::Container(const Container &other)
: tree    (other.tree)
, storage (other.storage)
{
    Init();
}

list_t Container::GetId() const
{
    return list_t(ID);
}

ContainerType Container::GetType() const
{
    return ContainerType(int(Type));
}

juce::String Container::GetName() const
{
    return Title;
}

juce::String Container::GetPath() const
{
    return Path;
}

bool Container::GetIsFolder() const
{
    return IsFolder;
}

size_t Container::GetTrackCount() const
{
    return size_t(TrackCount);
}

unsigned Container::GetOrdering() const
{
    return unsigned(Ordering);
}

std::vector<track_t> Container::GetTracks() const
{
    using namespace sqlite_orm;
    
    if (IsValid())
    {
        auto metadata = storage->GetMetadataStorage();
        
        auto results = metadata->select(&ListTrackListTable::trackId, where(is_equal(&ListTrackListTable::listId, GetId())));

        std::vector<track_t> tracks;
        
        for (auto &row : results)
        {
            tracks.push_back(*row);
        }
        
        return tracks;
    }
    else
    {
        return std::vector<track_t>();
    }
}

Container Container::GetParent() const
{
    return Container (tree.getParent(), storage);
}

size_t Container::GetNumChildren() const
{
    return size_t(tree.getNumChildren());
}

Container Container::GetChild(size_t index) const
{
    return Container (tree.getChild(int(index)), storage);
}

Container Container::GetRoot() const
{
    return Container(tree.getRoot(), storage);
}

bool Container::IsValid() const
{
    return tree.isValid() && storage.get();
}

bool Container::IsRoot() const
{
    return tree.isValid() && tree.hasType(ContainerTreeIdentifiers::rootId);
}

void Container::Dump() const
{
    DBG(tree.toXmlString());
}

void Container::Init()
{
    ID        .referTo(tree, ContainerTreeIdentifiers::crateIdId   , nullptr);
    Type      .referTo(tree, ContainerTreeIdentifiers::listTypeId  , nullptr);
    Title     .referTo(tree, ContainerTreeIdentifiers::titleId     , nullptr);
    Path      .referTo(tree, ContainerTreeIdentifiers::pathId      , nullptr);
    IsFolder  .referTo(tree, ContainerTreeIdentifiers::isFolderId  , nullptr);
    TrackCount.referTo(tree, ContainerTreeIdentifiers::trackCountId, nullptr);
    Ordering  .referTo(tree, ContainerTreeIdentifiers::orderingId  , nullptr);
}
