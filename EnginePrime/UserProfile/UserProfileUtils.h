/* Copyright (C) Sam Spreadborough - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sam Spreadborough <spreads.sam@gmail.com>, 2018
 */

// ==============================================================================

enum class BpmRangeOption
{
      Option58_115
    , Option68_135
    , Option78_155
    , Option88_175
    , Option98_195
};

inline
const char *ToString(BpmRangeOption child)
{
    switch (child)
    {
        case BpmRangeOption::Option58_115: return "58-115";
        case BpmRangeOption::Option68_135: return "68-135";
        case BpmRangeOption::Option78_155: return "78-155";
        case BpmRangeOption::Option88_175: return "88-175";
        case BpmRangeOption::Option98_195: return "98_195";
    }
}

inline
BpmRangeOption ToBpmRangeOption(const juce::String &string)
{
         if (string == ToString(BpmRangeOption::Option58_115)) return BpmRangeOption::Option58_115;
    else if (string == ToString(BpmRangeOption::Option68_135)) return BpmRangeOption::Option68_135;
    else if (string == ToString(BpmRangeOption::Option78_155)) return BpmRangeOption::Option78_155;
    else if (string == ToString(BpmRangeOption::Option88_175)) return BpmRangeOption::Option88_175;
    else if (string == ToString(BpmRangeOption::Option98_195)) return BpmRangeOption::Option98_195;
    else
        jassertfalse;                                          return BpmRangeOption::Option58_115;
}

// ==============================================================================
